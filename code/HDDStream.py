import time
import sys
import numpy as np

# This module applies the HDDStream algorithm to incrementally perform clustering. See the following papers for more
# details:
# [1] Ntoutsi, Irene, et al. "Density-based Projected Clustering over High Dimensional Data Streams." SDM. 2012.
# [2] Bohm, Christian, et al. "Density connected clustering with local subspace preferences." Data Mining, 2004.
# ICDM'04. Fourth IEEE International Conference on. IEEE, 2004.
# Author: Deeksha singh, modified by Tao Tang,Mark Read

max_preferred_dimensionality = None


class Cluster(object):
    """ Represents a set of points, there are three types of clusters in hddstream:outlier,potential,core. """
    def __init__(self, cluster_id=None, CF1=(), CF2=(), cumulative_size=0, size=0, preferred_dimension=[],
                 neighbourhood_clusters=[]):
        # single int for potential and outlier, multiple ints for core(consists of potential)
        self.cluster_id = cluster_id
        # CF1 CF2 and cumulative size are weighted sum(weight function is f(t-ti), t is current time, ti is the time
        # point i added)
        # cluster feature 1, defined as weighted linear sum of all points in this cluster
        self.CF1 = CF1
        # cluster feature 2, defined as weighted linear sum of square of all points in this cluster
        self.CF2 = CF2
        # number of clusters added during all time
        self.cumulative_size = cumulative_size
        # number of clusters added in current time point
        self.size = size
        # preferred_dimension of a cluster
        self.preferred_dimension = preferred_dimension
        # set of clusters whose core within a given weighted distance to centroid of this cluster
        self.neighbourhood_clusters = neighbourhood_clusters

    def calculate_preferred_dimension(self, pref_dim_variance_threshold_squared, p_k):
        """ If variance of a dimension smaller than threshold, set its weights as p_k, else 1 """
        self.preferred_dimension = np.ones(self.CF1.shape[0])
        for index in range(self.CF1.shape[0]):
            variance_squared = (self.CF2[index] / self.cumulative_size) - ((self.CF1[index] / self.cumulative_size) ** 2)
            if variance_squared <= pref_dim_variance_threshold_squared:
                self.preferred_dimension[index] = p_k

    def get_copy(self):
        """ Return a new cluster with the same data of current cluster """
        return Cluster(self.cluster_id, self.CF1, self.CF2, self.cumulative_size, self.size,
                       self.preferred_dimension, self.neighbourhood_clusters)

    # return the centroid of a clusters
    @staticmethod
    def get_centre(self):
        # TODO, Tao, can you make this a method on the Cluster class. Clusters should calculate their own centres.
        return np.array([(x / self.cumulative_size) for x in self.CF1])

class PreDeCon(object):
    def __init__(self, datapoints, dataset_dimensionality, radius_threshold, pref_dim_variance_threshold,
                 max_subspace_dimensionality, density_threshold, p_k, radius_threshold_decay_factor = 1,count_time=False):
        # list of data points to be classified
        self.datapoints = datapoints
        #in full dataset with 16 dimesions, all pcore clusters merge into one core cluster,this is to modify it by set another smaller radiu threshold for Predecon
        self.radius_threshold_decay_factor = radius_threshold_decay_factor
        self.radius_threshold = radius_threshold # * self.radius_threshold_decay_factor
        self.radius_threshold_squared = radius_threshold ** 2
        self.dataset_dimensionality = dataset_dimensionality
        self.pref_dim_variance_threshold = pref_dim_variance_threshold
        self.pref_dim_variance_threshold_squared = pref_dim_variance_threshold ** 2
        self.max_subspace_dimensionality = max_subspace_dimensionality
        self.density_threshold = density_threshold
        self.p_k = p_k
        # list of cluster object, represents the result of clustering
        self.higher_level_clusters = []
        self.count_time = count_time
        


        """
        Run through each datapoint. If they are core points, we examine their neighbourhoods recursively, building up
        the cluster as we go.
        """
    def run(self):
#        print('radius threshold is decayed by:', self.radius_threshold_decay_factor)
#        print('radius threshold is:',self.radius_threshold)
        sw = Stopwatch()

        self.determine_datapoint_preference_neighbourhoods()
        self.calculate_preference_weighted_neighbourhoods()

        #set bool member which shows if this point is a higher level core in each datapoint
        for datapoint in self.datapoints:
            datapoint.check_if_higher_level_core(self.max_subspace_dimensionality, self.density_threshold)

        for i in range(len(self.datapoints)):
            new_higher_level_cluster_id = len(self.higher_level_clusters)
            datapoint = self.datapoints[i]
            # that depends on type for datapoint here, for datapoint , higher level is pcore cluster,for datapoint_cluster,higher level is core cluster
            if datapoint.is_higher_level_core() and datapoint.classification != 't':
                # create a higher level cluster with datapoint as core
                new_cluster = datapoint.create_higher_level_cluster([new_higher_level_cluster_id])
                # ids of weight neighbourhoods of this datapoint
                # try to add its neighbourhoods to this new cluster
                self.expand(new_cluster, datapoint)
                new_cluster.calculate_preferred_dimension(self.pref_dim_variance_threshold_squared, self.p_k)
                if new_cluster.cumulative_size >= 1.0:
                    self.higher_level_clusters.append(new_cluster)
            else: #if failed, mark this datapoint as noise
                self.datapoints[i].classification = 'n'

            if self.count_time:
                if i % 5 == 0:
                    print('initialisation complete: ' + str((i / len(self.datapoints)) * 100) + ' %')
                if sw.split() > 3600:
                    print('warning: initialisation takes more than 1 hour')
                    sys.exit()
        
        print('preferred dimensions of clusters:')            
        for c in self.higher_level_clusters:
            print(c.preferred_dimension)            
              
  #return the list of clusters after running 
    def get_result(self):
        return self.higher_level_clusters

    #after a new cluster created, try to add its neighbourhood into it
    def expand(self, new_cluster, datapoint):
        """
        params: new_cluster: new cluster to expand
        params: datapoint, the core datapoint which used to create new cluster
        """
        # ids of weight neighbourhoods of this data point
        n_ids = np.copy(datapoint.preference_weighted_neighbourhoods_id)
        # try to add its neighbourhoods to this new cluster

        while len(n_ids) > 0:
            first_dp = self.datapoints[n_ids[0]]
            r_ids = []
            for other_id in n_ids:  # in Bohm fig 4, this loop computes R.
                if first_dp.is_direct_reach(self.datapoints[other_id], other_id,
                                            self.max_subspace_dimensionality):
                    r_ids.append(other_id)
                 
            for r_id in r_ids:
                r = self.datapoints[r_id].classification

                if r == 'f':
                    n_ids = np.append(n_ids, r_id)
                if r == 'f' or r == 'n':  # unclassified or noise
                    self.datapoints[r_id].classification = 't'
                    self.datapoints[r_id].add_to_higher_cluster(new_cluster)		
                        # pop item from top
            n_ids = np.delete(n_ids, 0, axis=0)

    def determine_datapoint_preference_neighbourhoods(self):
        """
        Determines the unweighted neighbourhood of each data point in X. The neighbourhood is all points lying
        within a threshold Euclidean distance of the given point, including the given point itself.
        """
        for point in self.datapoints:
            for index, other_point in enumerate(self.datapoints):
                e_dist = get_euclidean_dist(other_point.value, point.value)
                if e_dist <= self.radius_threshold:
                    point.neighbourhoods_id.append(index)
                    # compute the preferred dimension for this neighbourhood set
            for dimension in range(self.dataset_dimensionality):
                variance = self.get_variance(point, dimension)
                if variance <= self.pref_dim_variance_threshold:
                    point.neighbourhoods_preferred_dimensions[dimension] = self.p_k
                else:
                    point.neighbourhoods_preferred_dimensions[dimension] = 1

    def calculate_preference_weighted_neighbourhoods(self):
        """ From PreDeCon Definition 5. """
        """
        TODO can we not set self.datapoint_weighted_neighbourhoods and datapoint_weighted_neighbourhood_ids to []
        here, rather than at the end of _initialise() and perform_offline_clustering()?
        """
        sw = Stopwatch()
        count = 0
        for point in self.datapoints:
            for i in point.neighbourhoods_id:
                # i and index are used to extract preferred dimension of X[i] and point
                dist = self.get_general_preference_dist_squared(point, self.datapoints[i])  # squared to avoid sqrt ops
                if dist <= self.radius_threshold_squared:
                    point.preference_weighted_neighbourhoods_id.append(i)
        print("Calculating pref weighted neighbourhood took %s seconds " % (sw.split()))
        print('--------------------------')

    # return the average distance from this point to its neighbourhood in given dimension
    def get_variance(self, point, dimension):
        result = 0.0
        for i in point.neighbourhoods_id:
            result += (point.value[dimension] - self.datapoints[i].value[dimension]) ** 2
        return result / len(point.neighbourhoods_id)

    def get_general_preference_dist_squared(self, point, other):
        """ From PreDeCon Definition 4. Note this is squared distance. """
        return max(self.get_dist_p_squared(point, other), self.get_dist_p_squared(other, point))
    
    #get the wieghted distance between p and q,using preferred dimension of p
    def get_dist_p_squared(self, p, q):
        weight = p.neighbourhoods_preferred_dimensions
        sum = 0.0
        for i in range(self.dataset_dimensionality):
            sum += weight[i] * (p.value[i] - q.value[i]) ** 2
        return sum


# represents a point which need to be classified in PreDeCon
class Datapoint(object):
    def __init__(self, point, cluster_id=[]):
        # np array represent this point
        self.value = point
        # list of index represents points withn a given distance
        self.neighbourhoods_id = []
        self.preference_weighted_neighbourhoods_id = []
        # list of intgers with lenght = dimensionality of point, which variance in the dimension smaller than variance threshlod
        self.neighbourhoods_preferred_dimensions = np.ones(len(point))
        # maintaining a status for datapoint,'t' means classified,'f' means unclassfied,'n' means noise
        self.classification = 'f'
        self.cluster_id = cluster_id
        # if this point is centroid of higher level cluster
        self.is_core = None

    # if thie point can be centroid of a pcore cluster
    def get_pref_dimension_number(self):
        return (np.array(self.neighbourhoods_preferred_dimensions) > 1).sum()
    
    #create a higher level cluster based on this point
    def create_higher_level_cluster(self, new_id):
        return Cluster(cluster_id=new_id, CF1=np.zeros(len(self.value)), CF2=np.zeros(len(self.value)),
                       preferred_dimension=np.ones(len(self.value)))

    # check if other direct reach to self,self corresponding point 'q' in Predecon Definition 7
    def is_direct_reach(self, point_p, point_p_id, max_preferred_dimensionality):
        return self.is_core \
               and point_p.get_pref_dimension_number() <= max_preferred_dimensionality \
               and point_p_id in self.preference_weighted_neighbourhoods_id

    #add this datapoint to higher cluster
    def add_to_higher_cluster(self, new_cluster):
        new_cluster.CF1 += self.value
        new_cluster.CF2 += self.value ** 2
        new_cluster.size += 1
        new_cluster.cumulative_size += 1

    def check_if_higher_level_core(self, max_subspace_dimensionality, density_threshold):
        self.is_core = self.get_pref_dimension_number() <= max_subspace_dimensionality and len(
            self.preference_weighted_neighbourhoods_id) >= density_threshold

    def is_higher_level_core(self):
        return self.is_core


class Datapoint_Cluster(Datapoint):
    def __init__(self, point, cluster_id=[], cumulative_size=0, size=0, is_core=None, CF1=0, CF2=0):
        super().__init__(point=point, cluster_id=cluster_id)
        self.is_core = is_core
        self.size = size
        self.cumulative_size = cumulative_size
        self.CF1 = CF1
        self.CF2 = CF2

    def create_higher_level_cluster(self, new_id):
        return Cluster(cluster_id=[], CF1=np.zeros(len(self.value)), CF2=np.zeros(len(self.value)),
                       preferred_dimension=np.ones(len(self.value)))
        # return an empty cluster(represents core cluster)

    # is_core for datapoint_cluster is generated by hddstream as input for __init__
    def check_if_higher_level_core(self, max_subspace_dimensionality, density_threshold):
        return

    def add_to_higher_cluster(self, new_cluster):
        new_cluster.CF1 += self.CF1
        new_cluster.CF2 += self.CF2
        new_cluster.size += self.size
        new_cluster.cumulative_size += self.cumulative_size
        new_cluster.cluster_id.append(self.cluster_id[0])



class HDDStream(object):
    def __init__(self,
                 max_subspace_dimensionality=None,  # if None, will instead use dimensionality of dataset.
                 density_threshold_proportion_events=1,
                 radius_threshold=1.5,
                 k=100, beta=0.5,
                 decay=1.,  # rate at which old data's influence on the cluster diminishes with time
                 initialisation_dataset_proportion=0.005,
                 log_clustering_data=False,  # used for logging
                 pref_dim_variance_threshold_proportion=0.125):
 
 
        
        self.init = False  # initialisation phase, where PreDeCon is used to seed clusters on a portion of input data
        # max dimensionality of a subspace
        self.max_subspace_dimensionality = max_subspace_dimensionality  # when None, take dimensionality of dataset
        # mu is density threshold for pcore and core microclusters, mu_boundary defines percentage of data to set mu to

        # clusters must capture a certain number of data points (events) to be core or pcore. This is determined
        # as a proportion of the input dataset size, to accommodate different numbers of events in cytometry runs.
        self.density_threshold = None
        self.density_threshold_proportion_events = density_threshold_proportion_events  # see above comment.
            
        # radius threshold, used in defining the maximum radius (volume) of a cluster
        self.radius_threshold = radius_threshold
        # used to avoid having to square root values for comparison with threshold
        self.radius_threshold_squared = self.radius_threshold * self.radius_threshold

        # delta is variance threshold, set later
        # if a cluster's variance along a dimension is less than this threshold, then that dimension is 'preferred'
        self.pref_dim_variance_threshold = None  # set relative to incoming normalised data, once clustering starts
        # to save computational power (squaring and square rooting), save the squared dimensionality variance threshold
        # for use in comparisons.
        self.pref_dim_variance_threshold_squared = None  # set relative to normalised data, once clustering starts
        # proportion of range of normalised data under which a dim is preferred
        self.pref_dim_variance_threshold_proportion = pref_dim_variance_threshold_proportion

        # k is used to adjust influence of preferred dimensions, should be k >> 1
        self.p_k = k
        # decay is factor to reduce influence of old data with
        self.decay_rate = decay
        # beta is the level of relaxation allowed to potential microclusters, it is within (0,1)
        self.p_beta = beta

        # now initialisation_stage_density_threshold_proportion deleted, both use self.density_threshold_proportion_events

        # section is the part of data to use for initialisation using PreDeCon
        self.initialisation_dataset_proportion = initialisation_dataset_proportion

        # list of clusters, see details in class cluster, representation *potential* core microclusters, each cluster has the following values
        # preferred dimension now keep in cluster itself
        self.pcore_clusters = []
        self.outlier_clusters = []
        # collection of all core clusters, each consisting of one or more potential core clusters
        self.core_clusters = []

        # time of last and current data
        self.last_data_timestamp = 0
        self.current_data_timestamp = 0

        self.dataset_dimensionality = 0  # number of features in the data set.
        self.dataset_num_events = 0  # number of events (rows) in the data set.Tt149162
        self.standard_dimensionality = 3
        # PreDeCon variables
        # list of np.array objects, one per data point (i). Each array contains the data point ids (points x 1) that are
        # in the weighted neighbourhood of point i.
        # TODO, Tao, why do we need "datapoint_weighted_neighbourhoods" above? They are a complete copy of information
        # in the input data file (X), and we are storing their ids here. So why copy all that data?
        self.current_ids = []

        # these variables log the progress of the algorithm, and can be output to the user.
        self.cluster_data = []
        self.pcore_data = []
        self.outlier_data = []
        self.log_clustering_data = log_clustering_data  # records cluster data for downstream evaluation

        #probably need these to check range of radius threshold later
        self.radius_squared_hddstream = [0,0]
        self.radius_squared_predecon = [0,0]
         
    def is_started(self):
        return self.init

    def get_core_cluster_ids(self):
        if (len(self.core_clusters) == 1):
            return [self.core_clusters[0].cluster_id] 
        return [cluster.cluster_id for cluster in self.core_clusters]

    def get_cluster_data(self):
        return np.array(self.cluster_data)

    def get_cumulative_size(self):
        return np.array([cluster.cumulative_size for cluster in self.core_clusters])

    def get_size(self):
        return np.array([cluster.size for cluster in self.core_clusters])

    def get_clusters_centres(self):
        return np.array([cluster.get_centre(cluster) for cluster in self.core_clusters])


    def get_core_clusters_pref_dimensions(self):
        return [cluster.preferred_dimension for cluster in self.core_clusters]

    def print_params(self):
        p_str = 'running HDDStream with params mu=' + str(
            self.density_threshold) + ' maximal projected dimensionality = ' + str(
            self.max_subspace_dimensionality) + ' radius threshold = ' + str(
            self.radius_threshold) + ' variance_threshold=' + str(
            self.pref_dim_variance_threshold) + ' decay_rate=' + str(self.decay_rate) \
                + ' beta=' + str(self.p_beta) + ' density_threshold_proportion= ' + str(
            self.density_threshold_proportion_events) + ' initialisation_percentage ' + str(
            self.initialisation_dataset_proportion)
        print(p_str)

    # not sure should variance set as fixed or according to value range per day,
    def set_dataset_dependent_parameters(self, X):
        # X is the input dataset for a given point in time.
        def determine_variance_threshold(data):
            # Determines variance threshold used for determining if a cluster 'prefers' a given dimension. This is set
            # anew for each input data time point. All data across all time points are normalised to [0,1] based on
            # their collective values prior to clustering. This means data at t=0 can lie in range [0,1], but t=1 might
            # lie in range [0.1,0.88], just because of experimental error in cytometry. We do not want these artifacts
            # to fall through to clustering, so we allow for them by setting the variance threshold anew for every time
            # point.
            maxes = np.nanmax(data, axis=0)  # max values found in each dimension
            mins = np.nanmin(data, axis=0)  # same, but minimum
            ranges = maxes - mins  # find the range, the variance threshold is some proportion of this range.
            average = np.nanmean(ranges)
            threshold = average * self.pref_dim_variance_threshold_proportion
            self.pref_dim_variance_threshold = threshold
            self.pref_dim_variance_threshold_squared = self.pref_dim_variance_threshold ** 2
            return threshold

        self.dataset_dimensionality = X.shape[1]
        self.dataset_num_events = X.shape[0]
        
#        self.radius_threshold = self.radius_threshold * np.sqrt(self.dataset_dimensionality/self.standard_dimensionality)
#        self.radius_threshold_squared = self.radius_threshold ** 2
        
        # if set to nonsensical value, set to dimensionality of dataset. This is performed once only, at clustering
        # initialisation; data set dimensionality should not change over time points.
        if self.max_subspace_dimensionality is None:
            self.max_subspace_dimensionality = self.dataset_dimensionality

        # set parameters
        self.density_threshold = self.dataset_num_events * self.density_threshold_proportion_events
        self.pref_dim_variance_threshold = determine_variance_threshold(X)
        # used in comparisons to save sqrt operations.
        self.pref_dim_variance_threshold_squared = self.pref_dim_variance_threshold ** 2
        self.print_params()

    def start(self, X, start_time):
        # X:whole data set
        #radius increased by sqaurt root of dimension, so radius threshold needed to be modifed by it
        self.radius_threshold = self.radius_threshold * np.sqrt(X.shape[1]/self.standard_dimensionality)
        self.radius_threshold_squared = self.radius_threshold ** 2
                
        print('starting HDDStream')
        self.set_dataset_dependent_parameters(X)
        

        # update time
        self.current_data_timestamp = start_time
        initialisation_data = int(np.floor(self.dataset_num_events * self.initialisation_dataset_proportion))
        # Initialise with section of data
        self._initialise(X[0:initialisation_data])
        # Send rest of data from first day to online clustering
        self.newdata(X[initialisation_data:], start_time, reset_parameters=False)

    def newdata(self, X, data_timestamp, reset_parameters=True):
        """ Load in and cluster a new datafile, representing a different point in time. """
        if reset_parameters:
            self.set_dataset_dependent_parameters(X)
        # update time
        self.last_data_timestamp = self.current_data_timestamp  # TODO can this be removed? Don't think so, it is used to calculate time interval during two point to perform downgrade
        self.current_data_timestamp = data_timestamp

        self.core_clusters = []

        # if time has progressed, decay the weight of old data held in clusters
        if (self.current_data_timestamp - self.last_data_timestamp) != 0:
            print('decaying cluster weights')
            # decay all p-core microclusters c that have not been updated in the last day
            self.decay_cluster_weights()
            # TODO MR does not understand this comment. Eh, cluster.size records number of points added in current timestamp, so when timestamp changes, size should be reset to 0 to record new timestamp,
            for pmc in self.pcore_clusters:
                pmc.size = 0.0
            for mc in self.core_clusters:
                mc.size = 0.0
            for oc in self.outlier_clusters:
                oc.size = 0.0
        
        # store ids of core clusters, now cluster id stored in class cluster
        self.cluster_ids = []  # TODO Tao, can this be deleted?
        # stores cluster points for evaluation
        if self.log_clustering_data:
            self.cluster_data = []
        # creates a list of False values, one per pcore.
        self.is_point_added_to_pcore = (np.full((len(self.pcore_clusters), 1), False, dtype=bool)).tolist()
        sw = Stopwatch()  # for logging execution time

        for i in range(X.shape[0]):  # scan through events in dataset
            x = X[i]  # extract row
            # try to add x to a potential microcluster
            trial1 = self.add_to_pcore(x)
            trial2 = False
            if not trial1:
                # Try to add x to an outlier microcluster
                trial2 = self.add_to_outliers(x)
            if not trial1 and not trial2:
                # Create new outlier microcluster for x
                self.create_new_outlier_cluster(x)
                if self.log_clustering_data:
                    self.outlier_data.append([x])
        print("clustering took %s seconds" % sw.split())
        print('--------------------------')
        print('performing offline clustering with PreDeCon with ' + str(len(self.pcore_clusters)) + ' pcores')
        self.perform_offline_clustering()

    def add_to_pcore(self, p):
        """ Attempts to add data point p to a pcore microcluster. Returns boolean value representing success. """
        distances = []
        # calculate distances to all pcore mc from point p
        for index, pmc in enumerate(self.pcore_clusters):
            # Update pcore_pref_dimen of pmc temporarily
            pcore_pref_dimen = self.update_pcore_pref_dimen(pmc, p)  # returns list of dimensions, !1 = preferred
            # threshold on number of preferred dimensions, where 1 = non-preferred.
            if (pcore_pref_dimen != 1).sum() <= self.max_subspace_dimensionality:
                # use tuple to associate: projected distance, pcore index number, pcore preferred dimensionality
                association = (self.get_projected_dist(pmc, p),
                               index,
                               pcore_pref_dimen)
                distances.append(association)

        if len(distances) > 0:
            # Get the closest pcore mc and compute its new properties with p included
            pmc_closest_index = self.get_closest_cluster_index(distances)

            # gonna check it
            # TODO check that distances[2] gives the same result as new_cluster_dim,
            # new_cluster_dim = self.update_pcore_pref_dimen(self.pcore_clusters[pmc_closest_index], p)
            # print('\ncheck these projected dimensionalities are the same:')
            # print(distances[pmc_closest_index][2])
            # print(new_cluster_dim)
            new_cluster_dim = distances[pmc_closest_index][2]  # was calculated above.
            new_cluster = self.get_projected_pmc(self.pcore_clusters[pmc_closest_index], p)
            radius_squared = self.get_radius_squared(new_cluster, new_cluster_dim)
            # if radius condition for pcore mc is still fulfilled for pmc_closest_index add p to it
            if radius_squared <= self.radius_threshold_squared:
                self.add_point_to_cluster(self.pcore_clusters, pmc_closest_index, p)
                self.pcore_clusters[pmc_closest_index].calculate_preferred_dimension(
                    self.pref_dim_variance_threshold_squared, self.p_k)
                if self.log_clustering_data:
                    self.pcore_data[pmc_closest_index].append(p)
                self.is_point_added_to_pcore[pmc_closest_index] = True
                return True
        return False

    def add_to_outliers(self, p):
        distances = []
        # maximal preferred dimensionality is not limited for outlier microclusters.
        # calculate distances to all o mc from p
        for index, o_mc in enumerate(self.outlier_clusters):
            # use tuple to associate: projected distance, pcore index number, pcore preferred dimensionality
            association = (self.get_projected_dist(o_mc, p),
                           index)
            distances.append(association)

        if len(distances) > 0:
            # Get the closest o mc and compute its new properties with p included
            o_mc_closest_index = self.get_closest_cluster_index(distances)
            o_mc = self.outlier_clusters[o_mc_closest_index].get_copy()
            new_cluster_dim = self.update_pcore_pref_dimen(o_mc, p)
            new_cluster = self.get_projected_pmc(o_mc, p)
            radius_squared = self.get_radius_squared(new_cluster, new_cluster_dim)
            if radius_squared <= self.radius_threshold_squared:
                o_mc.CF1 += p
                o_mc.CF2 += p ** 2
                o_mc.cumulative_size += 1
                current_pref_dim = self.calculate_pref_dimen([o_mc.CF1, o_mc.CF2,o_mc.cumulative_size])
                # check if adding p to outlier cluster makes it a pcore cluster
                if self.check_if_pcore(o_mc, current_pref_dim):
                    # make the o mc into a pcore mc
                    '''should be removed from outlier list'''
                    self.pcore_clusters.append(o_mc)
               #     del self.outlier_clusters[o_mc_closest_index]
                    if self.log_clustering_data:
                        self.pcore_data.append(self.outlier_data[o_mc_closest_index])
                    self.is_point_added_to_pcore.append(True)
                else:
                    # Add p into current o mc
                    self.outlier_clusters[o_mc_closest_index] = o_mc  # replace the previous cluster with the new one.
                    if self.log_clustering_data:
                        self.outlier_data[o_mc_closest_index].append(p)
                return True
        return False

    def get_projected_pmc(self, pmc, p):
        """ Creates a new PMC based on the supplied pmc, but with point p added. """
        return Cluster(CF1=pmc.CF1 + p, CF2=pmc.CF2 + p ** 2, cumulative_size=pmc.cumulative_size + 1)

    # from HDDStream definition 6
    def check_if_pcore(self, cluster, pref_dim):
        radius_squared = self.get_radius_squared(cluster, pref_dim)
        cumulative_size = cluster.cumulative_size
        pdim = (np.array(pref_dim) > 1).sum()
        return radius_squared <= self.radius_threshold_squared \
               and cumulative_size >= self.p_beta * self.density_threshold \
               and pdim <= self.max_subspace_dimensionality

    # from HDDStream definition 5
    def check_if_core(self, cluster, pref_dim=[]):
        pref_dim = cluster.preferred_dimension
        radius_squared = self.get_radius_squared(cluster, pref_dim)
        cumulative_size = cluster.cumulative_size
        pdim = (np.array(pref_dim) > 1).sum()
        return radius_squared <= self.radius_threshold_squared \
               and cumulative_size >= self.density_threshold \
               and pdim <= self.max_subspace_dimensionality

    def get_closest_cluster_index(self, distances):
        """ distances is a tuple (distance, id). Returns the id of the lowest distance"""
        dists = np.array([d[0] for d in distances])  # build numpy array of distances
        min_dist_index = np.nanargmin(dists)  # find the index of the smallest value in the array
        min_dist_id = int(distances[min_dist_index][1])  # get the corresponding cluster id
        return min_dist_id

    def get_radius_squared(self, pmc, pref_dimen):
        """
        From HDDStream definition 5. Note that this is squared radius. Radius is often contratsed to a threshold. It is
        more computationally efficient to square the threshold (that doesn't change) and compare to the squared radius
        than it is to repeated calculate square roots.
        """
        CF1 = pmc.CF1
        CF2 = pmc.CF2
        cumulative_size = pmc.cumulative_size
        radius_squared = 0.0
        for i in range(self.dataset_dimensionality):
            dimension = 1. / pref_dimen[i]  # value 1 or k
            value = (CF2[i] / cumulative_size) - ((CF1[i] / cumulative_size) ** 2)
            radius_squared += dimension * value
        self.radius_squared_hddstream[0] += 1
        self.radius_squared_hddstream[0] += radius_squared
        return radius_squared

    def get_projected_dist(self,pmc, p):
        """
        Get weighted Euclidean distance from cluster to point, weighted vector is the pcore_pref_dimen.
        Corresponds with Bohm paper definition 3.
        """
        # TODO, we can store the cluster centroid with the cluster, updating it only when a point is added.
        # TODO currently this re-computes every time a point is considered, but it won't necessarily have been changed.
        # pref_dimesion stored with other information of cluster together now, not sure what the second means
        # cluster_centroid = [(x / pmc.cumulative_size) for x in pmc.CF1]
        cluster_centroid = pmc.CF1 / pmc.cumulative_size
        dist = 0.0
        for i in range(self.dataset_dimensionality):  # this calculates a weighted Euclidean distance.
            value = (p[i] - cluster_centroid[i]) ** 2
            # TODO this relates to definition 3 in Bohm, and definition 8 in Ntoutsi, but these two definitions are
            # inconsistent. One uses 1/weight, the other one doesn't.
            # That is true, however, Predecon one was used to compare with radius threshold, this one is used to pick up the closet point to cluster centroid, not sure is it necessary to modify it.
            distance_weighting = 1. / pmc.preferred_dimension[i]  # Ntoutsi definition 8.
            dist += value * distance_weighting  # pcore_pref_dimen is the cluster's weighting for each dimension
        return np.sqrt(dist)  # square root

    def update_pcore_pref_dimen(self, pmc, p):
        """ Calculate the new preferred dimensionality for cluster pmc after datapoint p is added to it. """
        new_CF1 = pmc.CF1 + p
        new_CF2 = pmc.CF2 + (p ** 2)
        new_cumulative_size = pmc.cumulative_size + 1
        return self.calculate_pref_dimen(np.asarray([new_CF1, new_CF2, new_cumulative_size]))

    def calculate_pref_dimen(self, cluster_data):
        CF1 = cluster_data[0]
        CF2 = cluster_data[1]
        cumulative_size = cluster_data[2]
        pref_dimen = np.ones(self.dataset_dimensionality)
        for index in range(self.dataset_dimensionality):
            variance_squared = (CF2[index] / cumulative_size) - ((CF1[index] / cumulative_size) ** 2)
            if variance_squared <= self.pref_dim_variance_threshold_squared:
                pref_dimen[index] = self.p_k
        return pref_dimen

    # use a variant of Predecon, consider centroid of each pmc as 'seed',clustering pontential core clusters to core clusters
    def perform_offline_clustering(self):
        """
        PreDeCon algorithm entry point, used for clustering pcores at select snapshots in time (between input data time
        points). This process assembles pcore clusters (from HDDStream) into core clusters, which are output to the
        user.
        """
        # Predecon on pcore mc
        sw = Stopwatch()
        
        print('pcore status:',[self.check_if_core(pmc) for pmc in self.pcore_clusters])
        
        #create datapoints based on current pcore clusters
        datapoints = [
            Datapoint_Cluster(point=pmc.get_centre(pmc), cluster_id=pmc.cluster_id, is_core=self.check_if_core(pmc), \
                              size=pmc.size, cumulative_size=pmc.cumulative_size, CF1=pmc.CF1, CF2=pmc.CF2) \
            for pmc in self.pcore_clusters]



        #create a new Pewdecon to cluster pcore to core
        
        predecon_offline_clustering = PreDeCon(datapoints=datapoints,
                                               dataset_dimensionality=self.dataset_dimensionality, \
                                               radius_threshold=self.radius_threshold,
                                               pref_dim_variance_threshold=self.pref_dim_variance_threshold, \
                                               max_subspace_dimensionality=self.max_subspace_dimensionality,
                                               density_threshold=self.density_threshold, p_k=self.p_k ,radius_threshold_decay_factor = 1)

        predecon_offline_clustering.run()
        self.core_clusters = predecon_offline_clustering.get_result()

        print("offline clustering %s seconds" % sw.split())
        print('--------------------------')

    def decay_cluster_weights(self):
        """ Decays the weight of a cluster based on the ageing of the data it captures. """
        for cluster in self.pcore_clusters:
            # change of time during two data, assume time expressed in hours
            interval = (self.current_data_timestamp - self.last_data_timestamp) / 24
            multiplicity = 2 ** (-self.decay_rate * interval)
            cluster.CF1 *= multiplicity
            cluster.CF2 *= multiplicity
            cluster.cumulative_size *= multiplicity

    # called by start(X),here X was the first 0.1% of original data set
    def _initialise(self, X):
        """
        PreDeCon algorithm entry point. This is used to seed an initial collection of clusters, based on X, which is
        a portion of the supplied dataset. This is covered in the Bohm paper.
        :param X:
        :return:
        """
        sw = Stopwatch()

        # PreDeCon core clusters must have a certain density, calculated here as proportion of input data size.
        self.predecon_cluster_density_threshold = X.shape[0] * self.density_threshold_proportion_events
        print('beginning initialisation with data size ' + str(X.shape[0]) +
              '. Minimum cluster density is ' + str(self.predecon_cluster_density_threshold))


        self.init = True

        datapoints = [Datapoint(point=x) for x in X]

        # create a Predecon class used for initialzation
        predecon_init = PreDeCon(datapoints=datapoints, dataset_dimensionality=self.dataset_dimensionality, \
                                 radius_threshold=self.radius_threshold,
                                 pref_dim_variance_threshold=self.pref_dim_variance_threshold, \
                                 max_subspace_dimensionality=self.max_subspace_dimensionality,
                                 density_threshold=self.predecon_cluster_density_threshold, \
                                 p_k=self.p_k, count_time=True)
        predecon_init.run()
        self.pcore_clusters = predecon_init.get_result()

        self.cluster_seeding_complete = True
        print("initialisation %s seconds" % (sw.split()))
        print('--------------------------')

    def add_point_to_cluster(self, cluster_group, cluster_index, point):
        """ Updates the cluster with a new data point. """
        cluster = cluster_group[cluster_index]
        cluster.CF1 += point
        cluster.CF2 += point ** 2
        cluster.size += 1
        cluster.cumulative_size += 1

    # create a new core or pcore cluster,depends on the tyoe of cluster group
    def create_new_core_cluster(self, cluster_group, cluster_id, preferred_dimensions=[], neighbourhood_clusters=[]):
        CF1 = np.zeros(self.dataset_dimensionality)
        CF2 = np.zeros(self.dataset_dimensionality)
        cumulative_size = 0.0
        size = 0.0
        preferred_dimensions = np.ones(self.dataset_dimensionality)
        cluster_group.append(
            Cluster(cluster_id, CF1, CF2, cumulative_size, size, preferred_dimensions, neighbourhood_clusters))

    def create_new_outlier_cluster(self, point):
        CF1 = point  # point is a np.array, representing location in multi-dimensional space.
        CF2 = np.array(point) ** 2
        cumulative_size = 1.0
        size = 1.0
        new_outlier_cluster = Cluster(CF1=CF1, CF2 = CF2, cumulative_size = cumulative_size, size = size)
        new_outlier_cluster.calculate_preferred_dimension(self.pref_dim_variance_threshold_squared, self.p_k)
        self.outlier_clusters.append(new_outlier_cluster)


class Stopwatch:
    """ Convenient class for timing how long things take. """

    def __init__(self, start_now=True):
        self.start_time = None
        self.split_time = None
        if start_now:
            self.start()

    def start(self):
        self.start_time = time.time()

    def split(self):
        self.split_time = time.time()
        return self.get_last_split()

    def get_last_split(self):
        return self.split_time - self.start_time


def get_euclidean_dist(a, b):
    return np.linalg.norm(np.asarray(a) - np.asarray(b))

