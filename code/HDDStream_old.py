import time
import sys
import numpy as np

# This class applies the HDDStream algorithm to incrementally perform clustering. See the following papers for more
# details:
# [1] Ntoutsi, Irene, et al. "Density-based Projected Clustering over High Dimensional Data Streams." SDM. 2012.
# [2] Bohm, Christian, et al. "Density connected clustering with local subspace preferences." Data Mining, 2004.
# ICDM'04. Fourth IEEE International Conference on. IEEE, 2004.
# Author: Deeksha singh, modified by Tao Tang

debug_fp = 'hddstream_old_debug.txt'


'''represents a set of points, there are three types of clusters in hddstream:outlier,potential,core'''
class cluster(object):
	def __init__(self,cluster_id = [],CF1=0,CF2=0,cumulative_size=0,size=0,preferred_dimension = [],neighbourhood_clusters = []):
		#single int for potential and outlier, multiple ints for core(consists of pontential) 
		self.cluster_id = cluster_id
		#CF1 CF2 and cumulative szie are weighted sum(weight function is f(t-ti), t is current time, ti is the time point i addded)
		#cluster feature 1 , defined as weighted linear sum of all points in this cluster
		self.CF1 = CF1
		#cluster feature 2 , defined as weighted linear sum of square of all points
		self.CF2 = CF2
		#number of clusters added during all time
		self.cumulative_size = cumulative_size
		#number of clusters added in current time point
		self.size = size
		#preferred_dimension of a cluster
		self.neighbourhoods_preferred_dimensions = preferred_dimension
		#set of clusters whose core within a given weighted distance to centroid of this cluster
		self.neighbourhood_clusters = neighbourhood_clusters
		
	def calculate_preferred_dimension(self,pref_dim_variance_threshold_squared,p_k):
		self.preferred_dimension = np.ones(self.CF1.shape[0])
		for index in range(self.CF1.shape[0]):
			variance_squared = (self.CF2[index] / self.cumulative_size) - ((self.CF1[index] / self.cumulative_size) ** 2)
			if variance_squared <= pref_dim_variance_threshold_squared:
				self.preferred_dimension[index] = p_k
	
	def get_copy(self):
		return cluster(self.cluster_id,self.CF1,self.CF2,self.cumulative_size,self.size,self.preferred_dimension.self.neighbourhood_clusters)


		
class HDDStream(object):
    def __init__(self,
                 max_subspace_dimensionality=None,  # if None, will instead use dimensionality of dataset.
                 density_threshold_proportion_events=0.1,
                 radius_threshold=1.5,
                 k=100, beta=0.5,
                 decay=1,
                 initialisation_dataset_proportion=0.005,
                 log_clustering_data=False,
                 pref_dim_variance_threshold_proportion=0.125):
        self.is_point_added_to_pcore = []
        self.init = False
        # max dimensionality of a subspace
        self.max_subspace_dimensionality = max_subspace_dimensionality  # when None, take dimensionality of dataset
        # mu is density threshold for pcore and core microclusters, mu_boundary defines percentage of data to set mu to

        # clusters must capture a certain number of data points (events) to be core or pcore. This is determined
        # as a proportion of the input dataset size, to accommodate different numbers of events in cytometry runs.
        self.density_threshold = None
        self.density_threshold_proportion_events = density_threshold_proportion_events  # see above comment.
        # radius threshold, used in defining the maximum radius (volume) of a cluster
        self.radius_threshold = radius_threshold
        # used to avoid having to square root values for comparison with threshold
        self.radius_threshold_squared = self.radius_threshold * self.radius_threshold

        # delta is variance threshold, set later
        # if a cluster's variance along a dimension is less than this threshold, then that dimension is 'preferred'
        self.pref_dim_variance_threshold = None  # set relative to incoming normalised data, once clustering starts
        # to save computational power (squaring and square rooting), save the squared dimensionality variance threshold
        # for use in comparisons.
        self.pref_dim_variance_threshold_squared = None   # set relative to normalised data, once clustering starts
        # proportion of range of normalised data under which a dim is preferred
        self.pref_dim_variance_threshold_proportion = pref_dim_variance_threshold_proportion

        # k is used to adjust influence of preferred dimensions, should be k >> 1
        self.p_k = k
        # decay is factor to reduce influence of old data with
        self.decay_rate = decay
        # beta is the level of relaxation allowed to potential microclusters, it is within (0,1)
        self.p_beta = beta

        # now initialisation_stage_density_threshold_proportion deleted, both use self.density_threshold_proportion_events
		
        # section is the part of data to use for initialisation using PreDeCon
        self.initialisation_dataset_proportion = initialisation_dataset_proportion

        # list of clusters, see details in class cluster, representation *potential* core microclusters, each cluster has the following values
        # preferred dimension now keep in cluter itself
        self.pcore_clusters = []
        self.outlier_clusters = []
        # collection of all core clusters, each consisting of one or more potential core clusters
        self.core_clusters = []
     
	    # time of last and current data
        self.last_data_timestamp = 0
        self.current_data_timestamp = 0
		
        self.dataset_dimensionality = 0  # number of features in the data set.
        self.dataset_num_events = 0  # number of events (rows) in the data set.Tt149162

        # PreDeCon variables
        ''' list with length equals the number of points in initilazation step,each element maps a point,list the ids of points within a distance from corresponding point '''
        self.neighbourhoods = []
        '''list of preferred_dimension of each point in initilazation step, value equals p_k means this dimension is preferred, 1 means not'''
        self.neighbourhoods_preferred_dimensions = []
        # A list of numpy.array objects, one item per data point (i). Each array contains the data points (points x
        # dimensions) that are in the weighted neighbourhood of i.
        self.datapoint_weighted_neighbourhoods = []
        # list of np.array objects, one per data point (i). Each array contains the data point ids (points x 1) that are
        # in the weighted neighbourhood of point i.
        # TODO, Tao, why do we need "datapoint_weighted_neighbourhoods" above? They are a complete copy of information
        # in the input data file (X), and we are storing their ids here. So why copy all that data?
        '''In theory, it is supposed to store a set of points which weighted euclidean distance whithn a value form one point '''
        self.datapoint_weighted_neighbourhood_ids = []
        self.cores = []  # list of core clusters.
        self.current_ids = []


        # these variables log the progress of the algorithm, and can be output to the user.
        self.cluster_data = []
        self.pcore_data = []
        self.o_data = []
        self.log_clustering_data = log_clustering_data  # records cluster data for downstream evaluation

    def is_started(self):
        return self.init

    def get_ids(self):
        return np.array([cluster.cluster_id for cluster in self.core_clusters])

    def get_core_cluster_ids(self):
        return np.array([cluster.cluster_id for cluster in self.core_clusters])

    def get_cluster_data(self):
        return np.array(self.cluster_data)

    def get_cumulative_size(self):
        return np.array([cluster.cumulative_size for cluster in self.core_clusters])

    def get_size(self):
        return np.array([cluster.size for cluster in self.core_clusters])

    def get_clusters_centres(self):
        return np.array([self.get_centre(cluster) for cluster in self.core_clusters])
		
	#return the centroid of a clusters	
    @staticmethod
    def get_centre(cluster):
        return np.array([(x / cluster.cumulative_size) for x in cluster.CF1])

    def get_core_clusters_pref_dimensions(self):
        return [cluster.preferred_dimension for cluster in self.core_clusters]

    def get_num_dimensions(self):
        return self.dataset_dimensionality

    def get_percentage_boundary(self, base, percentage):
        return (base / 100) * percentage

    def set_variance_threshold(self, data):
        # Sets variance threshold delta. Set anew for each dataset, which is a distinct cytometry run,content currently deleted 
        # However, these data are all normalised in one operation. 
        return

    def print_params(self):
        p_str = 'running HDDStream with params mu=' + str(self.density_threshold) + ' maximal projected dimensionality = ' + str(
            self.max_subspace_dimensionality) + ' radius threshold = ' + str(self.radius_threshold) + ' variance threshold=' + str(self.pref_dim_variance_threshold) + ' decay rate=' + str(self.decay_rate) \
                + ' beta=' + str(self.p_beta) + 'density threshold proportion= ' + str(self.density_threshold_proportion_events)   + ' init percentage ' + str(self.initialisation_dataset_proportion)
        print(p_str)
    
	#not sure should variance set as fixed or according to value range per day,
    def set_dataset_dependent_parameters(self, X):
        # X is the input dataset for a given point in time.
        def determine_variance_threshold(data):
            # Determines variance threshold used for determining if a cluster 'prefers' a given dimension. This is set
            # anew for each input data time point. All data across all time points are normalised to [0,1] based on
            # their collective values prior to clustering. This means data at t=0 can lie in range [0,1], but t=1 might
            # lie in range [0.1,0.88], just because of experimental error in cytometry. We do not want these artifacts
            # to fall through to clustering, so we allow for them by setting the variance threshold anew for every time
            # point.
            maxes = np.nanmax(data, axis=0)  # max values found in each dimension
            mins = np.nanmin(data, axis=0)  # same, but minimum
            ranges = maxes - mins  # find the range, the variance threshold is some proportion of this range.
            average = np.nanmean(ranges)
            threshold = average * self.pref_dim_variance_threshold_proportion
            self.pref_dim_variance_threshold = threshold
            self.pref_dim_variance_threshold_squared = self.pref_dim_variance_threshold ** 2
            return threshold

        self.dataset_dimensionality = X.shape[1]
        self.dataset_num_events = X.shape[0]
        # if set to nonsensical value, set to dimensionality of dataset. This is performed once only, at clustering
        # initialisation; data set dimensionality should not change over time points.
        if self.max_subspace_dimensionality is None:
            self.max_subspace_dimensionality = self.dataset_dimensionality
        # set parameters
        self.density_threshold = self.dataset_num_events * self.density_threshold_proportion_events
        self.pref_dim_variance_threshold = determine_variance_threshold(X)
        # used in comparisons to save sqrt operations.
        self.pref_dim_variance_threshold_squared = self.pref_dim_variance_threshold ** 2
        self.print_params()

    def start(self, X, start_time):
        # X:whole data set
        print('starting HDDStream')
        self.set_dataset_dependent_parameters(X)

        # update time
        self.current_data_timestamp = start_time
        initialisation_data = int(np.floor(self.dataset_num_events * self.initialisation_dataset_proportion))
        # Initialise with section of data
        self._initialise(X[0:initialisation_data])
        # Send rest of data from first day to online clustering
        self.newdata(X[initialisation_data:], start_time, reset_parameters=False)

    def newdata(self, X, data_timestamp, reset_parameters=True):
        """ Load in and cluster a new datafile, representing a different point in time. """
        if reset_parameters:
            self.set_dataset_dependent_parameters(X)
        # update time
        self.last_data_timestamp = self.current_data_timestamp # TODO can this be removed? Don't think so, it is used to calculate time interval during two point to perform downgrade
        self.current_data_timestamp = data_timestamp
        # TODO this needs to be loaded from user, not hard coded. Future applications may not use evenly spaced timepoints. Now laoded time in hour from user
        self.core_clusters = []
		
		#if current time differnet previos,perform downgrade
        if ( self.current_data_timestamp - self.last_data_timestamp ) != 0:
            print('decaying cluster weights')
            # decay all p-core microclusters c that have not been updated in the last day
            self.decay_cluster_weights()
        # TODO MR does not understand this comment. Eh, cluster.size records number of points added in current timestamp, so when timestamp changes, size should be reset to 0 to record new timestamp,
            for pmc in self.pcore_clusters:
                pmc.size = 0.0
            for mc in self.core_clusters:
                mc.size = 0.0
            for oc in self.outlier_clusters:
                oc.size = 0.0
        #store ids of core clusters, now cluster id stored in class cluster
        self.cluster_ids = []
        # stores cluster points for evaluation
        if self.log_clustering_data:
            self.cluster_data = []
        # creates a list of False values, one per pcore.
        self.is_point_added_to_pcore = (np.full((len(self.pcore_clusters), 1), False, dtype=bool)).tolist()
        sw = Stopwatch()  # for logging execution time

        for i in range(X.shape[0]):  # scan through events in dataset
            x = X[i]  # extract row
            # try to add x to a potential microcluster
            trial1 = self.add_to_pcore(x)
            trial2 = False
            if not trial1:
                # Try to add x to an outlier microcluster
                trial2 = self.add_to_outliers(x)
            if not trial1 and not trial2:
                # Create new outlier microcluster for x
                self.create_new_outlier_cluster(x)
                if self.log_clustering_data:
                    self.o_data.append([x])
        print("clustering took %s seconds" % sw.split())
        print('--------------------------')
        print('performing offline clustering with PreDeCon with ' + str(len(self.pcore_clusters)) + ' pcores')
        self.perform_offline_clustering()

    def add_to_pcore(self, p):
        """ Attempts to add data point p to a pcore microcluster. Returns boolean value representing success. """
        distances = []
        # calculate distances to all pcore mc from point p
        for index, pmc in enumerate(self.pcore_clusters):
            # Update pcore_pref_dimen of pmc temporarily
            pcore_pref_dimen = self.update_pcore_pref_dimen(pmc, p)  # returns list of dimensions, !1 = preferred
            # threshold on number of preferred dimensions, where 1 = non-preferred.
            if (pcore_pref_dimen != 1).sum() <= self.max_subspace_dimensionality:
                # use tuple to associate: projected distance, pcore index number, pcore preferred dimensionality
                association = (self.get_projected_dist(self.pcore_clusters[index], pmc, p),
                               index,
                               pcore_pref_dimen)
                distances.append(association)

        if len(distances) > 0:
            # Get the closest pcore mc and compute its new properties with p included
            pmc_closest_index = self.get_closest_cluster_index(distances)

            #gonna check it
            # TODO check that distances[2] gives the same result as new_cluster_dim,
            # new_cluster_dim = self.update_pcore_pref_dimen(self.pcore_clusters[pmc_closest_index], p)
            # print('\ncheck these projected dimensionalities are the same:')
            # print(distances[pmc_closest_index][2])
            # print(new_cluster_dim)
            new_cluster_dim = distances[pmc_closest_index][2]  # was calculated above.
            new_cluster= self.get_projected_pmc(self.pcore_clusters[pmc_closest_index], p)
            radius_squared = self.get_radius_squared(new_cluster, new_cluster_dim)
            # if radius condition for pcore mc is still fulfilled for pmc_closest_index add p to it
            if radius_squared <= self.radius_threshold_squared:
                self.add_point_to_cluster(self.pcore_clusters, pmc_closest_index, p)
                self.pcore_clusters[pmc_closest_index].calculate_preferred_dimension(self.pref_dim_variance_threshold_squared,self.p_k)
                if self.log_clustering_data:
                    self.pcore_data[pmc_closest_index].append(p)
                self.is_point_added_to_pcore[pmc_closest_index] = True
                return True
        return False

    def add_to_outliers(self, p):
        distances = []
        # maximal preferred dimensionality is not limited for outlier microclusters.
        # calculate distances to all o mc from p
        for index, o_mc in enumerate(self.outlier_clusters):
            # use tuple to associate: projected distance, pcore index number, pcore preferred dimensionality
            association = (self.get_projected_dist(self.outlier_clusters[index].preferred_dimension, o_mc, p),
                           index)
            distances.append(association)

        if len(distances) > 0:
            # Get the closest o mc and compute its new properties with p included
            o_mc_closest_index = self.get_closest_cluster_index(distances)
            o_mc = self.outlier_clusters[o_mc_closest_index].get_copy()
            new_cluster_dim = self.update_pcore_pref_dimen(o_mc, p)
            new_cluster = self.get_projected_pmc(o_mc, p)
            radius_squared = self.get_radius_squared(new_cluster, new_cluster_dim)
            if radius_squared <= self.radius_threshold_squared:
                o_mc.CF1 += p
                o_mc.CF2 += p ** 2
                o_mc.cumulative_size += 1
                current_pref_dim = self.calculate_pref_dimen(np.array(o_mc.CF1,o_mc.CF2,o_mc.cumulative_size))  # TODO, why creating new np.array? #seems unnecessary before, but o_mc is not array now 
                # check if adding p to outlier cluster makes it a pcore cluster
                if self.check_if_pcore(o_mc, current_pref_dim):
                    # make the o mc into a pcore mc               
                    '''should be removed from outlier list''' 
                    self.pcore_clusters.append(o_mc)            
                    if self.log_clustering_data:
                        self.pcore_data.append(self.o_data[o_mc_closest_index])
                    self.is_point_added_to_pcore.append(True)
                else:
                    # Add p into current o mc
                    self.outlier_clusters[o_mc_closest_index] = o_mc  # replace the previous cluster with the new one.
                    if self.log_clustering_data:
                        self.o_data[o_mc_closest_index].append(p)
                return True
        return False

    def get_projected_pmc(self, pmc, p):
        """ Creates a new PMC based on the supplied pmc, but with point p added. """
        return cluster(CF1 = pmc.CF1 + p , CF2 = pmc.CF2 + p **2, cumulative_size = pmc.cumulative_size + 1)

    # from HDDStream definition 6
    def check_if_pcore(self, cluster, pref_dim):
        radius_squared = self.get_radius_squared(cluster, pref_dim)
        cumulative_size = cluster.cumulative_size
        pdim = (np.array(pref_dim) > 1).sum()
        return radius_squared <= self.radius_threshold_squared \
               and cumulative_size >= self.p_beta * self.density_threshold \
               and pdim <= self.max_subspace_dimensionality

    # from HDDStream definition 5
    def check_if_core(self, cluster, pref_dim = []):
        pref_dim = cluster.preferred_dimension
        radius_squared = self.get_radius_squared(cluster, pref_dim)
        cumulative_size = cluster.cumulative_size
        pdim = (np.array(pref_dim) > 1).sum()
        return radius_squared <= self.radius_threshold_squared \
               and cumulative_size >= self.density_threshold \
               and pdim <= self.max_subspace_dimensionality

    def get_closest_cluster_index(self, distances):
        """ distances is a tuple (distance, id). Returns the id of the lowest distance"""
        dists = np.array([d[0] for d in distances])  # build numpy array of distances
        min_dist_index = np.nanargmin(dists)  # find the index of the smallest value in the array
        min_dist_id = int(distances[min_dist_index][1])  # get the corresponding cluster id
        return min_dist_id


    def get_radius_squared(self, pmc, pref_dimen):
        """
        From HDDStream definition 5. Note that this is squared radius. Radius is often contratsed to a threshold. It is
        more computationally efficient to square the threshold (that doesn't change) and compare to the squared radius
        than it is to repeated calculate square roots.
        """
        CF1 = pmc.CF1
        CF2 = pmc.CF2
        cumulative_size = pmc.cumulative_size
        radius_squared = 0.0
        for i in range(self.dataset_dimensionality):
            dimension = 1. / pref_dimen[i]  # value 1 or k
            value = (CF2[i] / cumulative_size) - ((CF1[i] / cumulative_size) ** 2)
            radius_squared += dimension * value
        return radius_squared

    def get_projected_dist(self, pcore_cluster, pmc, p):
        """
        Get weighted Euclidean distance from cluster to point, weighted vector is the pcore_pref_dimen.
        Corresponds with Bohm paper definition 3.
        """
        # TODO, we can store the cluster centroid with the cluster, updating it only when a point is added.
        # TODO currently this re-computes every time a point is considered, but it won't necessarily have been changed.
        #pref_dimesion stored with other information of cluster together now, not sure what the second means 
		#cluster_centroid = [(x / pmc.cumulative_size) for x in pmc.CF1]
        cluster_centroid = pmc.CF1/pmc.cumulative_size
        dist = 0.0
        for i in range(self.dataset_dimensionality):  # this calculates a weighted Euclidean distance.
            value = (p[i] - cluster_centroid[i]) ** 2
            # TODO this relates to definition 3 in Bohm, and definition 8 in Ntoutsi, but these two definitions are
            # inconsistent. One uses 1/weight, the other one doesn't.
			# That is true, however, Predecon one was used to compare with radius threshold, this one is used to pick up the closet point to cluster centroid, not sure is it necessary to modify it.			
            distance_weighting = 1. / pcore_cluster.preferred_dimension[i]  # Ntoutsi definition 8.
            dist += value * distance_weighting  # pcore_pref_dimen is the cluster's weighting for each dimension
        return np.sqrt(dist)  # square root

    def update_pcore_pref_dimen(self, pmc, p):
        """ Calculate the new preferred dimensionality for cluster pmc after datapoint p is added to it. """
        new_CF1 = pmc.CF1 + p
        new_CF2 = pmc.CF2 + (p ** 2)
        new_cumulative_size = pmc.cumulative_size + 1
        return self.calculate_pref_dimen(np.asarray([new_CF1, new_CF2, new_cumulative_size]))
		
    def calculate_pref_dimen(self,cluster_data):
        CF1 = cluster_data[0]
        CF2 = cluster_data[1]
        cumulative_size = cluster_data[2]
        pref_dimen = np.ones(self.dataset_dimensionality)
        for index in range(self.dataset_dimensionality):
            variance_squared = (CF2[index] / cumulative_size) - ((CF1[index] / cumulative_size) ** 2)
            if variance_squared <= self.pref_dim_variance_threshold_squared:
                pref_dimen[index] = self.p_k
        return pref_dimen
	
#use a variant of Predecon, consider centroid of each pmc as 'seed',clustering pontential core clusters to core clusters
    def perform_offline_clustering(self):
        """
        PreDeCon algorithm entry point, used for clustering pcores at select snapshots in time (between input data time
        points). This process assembles pcore clusters (from HDDStream) into core clusters, which are output to the
        user.
        """
        # Predecon on pcore mc
        sw = Stopwatch()
        # all pcore clusters are unclassified in the beginning. Creates numpy array of specified dim, filled with value.
        classified = np.full((len(self.pcore_clusters), 1), 'f', dtype='str')
        all = [self.get_centre(cluster) for cluster in self.pcore_clusters]
        self.determine_datapoint_preference_neighbourhoods(all)
        self.calculate_preference_weighted_neighbourhoods(np.array(all))
        
        for d in self.neighbourhoods_preferred_dimensions:
            print('perferred_dimension,',d)
            
        for pmc in self.pcore_clusters:
            print(pmc.CF1,pmc.CF2,pmc.cumulative_size,pmc.size) 
            
        for index, pmc in enumerate(self.pcore_clusters):
            pref_dimen = pmc.preferred_dimension
            if self.check_if_core(pmc, pref_dimen):
                count = len(self.core_clusters)
                #create an empty core_cluster for current pmc, pmc will be added to this corecluster in expand as it includes in the neighbourhood by itself 
                self.create_new_core_cluster(self.core_clusters,[])
                if self.log_clustering_data:
                    self.cluster_data.append([])
                print(self.get_ids())
                # number of core clusters, core clusters list, core cluster preferred dimension , index of pcore cluster supposed to expand
                self.expand_core_cluster(classified, count, index)
                # If nothing has been added to this core mc delete it
                if self.core_clusters[count].cumulative_size < 1.0:
                    del self.core_clusters[count]
                    if self.log_clustering_data:
                        del self.cluster_data[count]
                else:
                    self.cluster_ids.append(np.array(self.current_ids))
                    print('new core',index,self.current_ids,'classification',classified.T)
                    self.current_ids = []
                    
            else:
                classified[index] = 'n'
        # TODO is this resetting actually necessary? They are recalculated from scratch at the top of the method. Seems can be deleted
        self.datapoint_weighted_neighbourhoods = []  # reset for next use.
        self.datapoint_weighted_neighbourhood_ids = []
        self.neighbourhoods = []
        print("offline clustering %s seconds" % sw.split())
        print('--------------------------')
		
    def decay_cluster_weights(self):
        """ Decays the weight of a cluster based on the ageing of the data it captures. """
        for i in range(len(self.is_point_added_to_pcore)):
            cluster = self.pcore_clusters[i]
            # change of time during two data, assume time expressed in hours
            interval = (self.current_data_timestamp - self.last_data_timestamp)/24
            multiplicity = 2 ** (-self.decay_rate * interval)
            cluster.CF1 *= multiplicity
            cluster.CF2 *= multiplicity
            cluster.cumulative_size *= multiplicity
		
    # called by start(X),here X was the first 0.1% of original data set
    def _initialise(self, X):
        """
        PreDeCon algorithm entry point. This is used to seed an initial collection of clusters, based on X, which is
        a portion of the supplied dataset. This is covered in the Bohm paper.
        :param X:
        :return:
        """
        sw = Stopwatch()
        """
        event_classifications is a list, maintaining a status for each event in X. 'f' refers to un-classified, 'n' means noise,
        and otherwise it refers to the cluster ID.
        """
        event_classifications = np.full((X.shape[0], 1), 'f', dtype='str')
        # PreDeCon core clusters must have a certain density, calculated here as proportion of input data size.
        self.predecon_cluster_density_threshold = X.shape[0] * self.density_threshold_proportion_events
        print('beginning initialisation with data size ' + str(X.shape[0]) +
              '. Minimum cluster density is ' + str(self.predecon_cluster_density_threshold))
        self.determine_datapoint_preference_neighbourhoods(X)
        self.calculate_preference_weighted_neighbourhoods(X)
        """
        Run through each datapoint. If they are core points, we examine their neighbourhoods recursively, building up
        the cluster as we go.
        """
        record = []
        
        self.init = True
        for i in range(X.shape[0]):
            x = X[i]
            if self.is_core(i, x):
                new_cluster_id = len(self.pcore_clusters)
                # create a new potential core cluster in pcore_clusters with a point as its core, add neighbourhoods of theis point to cluster.neighbouthood_points
                self.create_new_core_cluster(cluster_group = self.pcore_clusters,cluster_id = [new_cluster_id])
                if self.log_clustering_data:  # purely for logging purposes
                     self.pcore_data.append([])
                # this examines the point's neighbourhood to establish the cluster, 'classifying' data points as it goes
                self.expand_pcore_cluster(event_classifications, new_cluster_id, i)
                # if nothing has been added to this pcore mc, delete it
                if self.pcore_clusters[new_cluster_id].cumulative_size < 1.0:
                    del self.pcore_clusters[new_cluster_id]
                    if self.log_clustering_data:
                        del self.pcore_data[new_cluster_id]  # TODO, what is this for??,seems for evaluation of data if type -evaluate as input when run firstimp.py, but I did not really use it
                else:
                    print('new cluster,',i,self.get_centre(self.pcore_clusters[new_cluster_id]))
            
            #if not core, mark this point as noise
            else:
                event_classifications[i] = 'n'
            if i % 5 == 0 :
                print('initialisation complete: ' + str((i / X.shape[0]) * 100) + ' %')
            if sw.split() > 3600:
                print('warning: initialisation takes more than 1 hour')
                sys.exit()
        #mark    print(str([cluster.cluster_id for cluster in self.pcore_clusters]))
            
        f = open(debug_fp,'w')    
        for r in record:
            f.write(str(r))
            
        for pmc in self.pcore_clusters:
            print('pmc core is:', pmc.CF1/pmc.cumulative_size)
            
        self.cluster_seeding_complete = True
        self.cores = []
        self.datapoint_weighted_neighbourhoods = []
        self.datapoint_weighted_neighbourhood_ids = []
        self.neighbourhoods_preferred_dimensions = []
        print("initialisation %s seconds" % (sw.split()))
        print('--------------------------')

# for a pontential cluster,add all neighbourhood points of its centroid to it, notice: that is not the neighbourhood clusters in cluster class		
    def expand_pcore_cluster(self,datapoint_classifications,newest_cluster_id,datapoint_id):
        '''
        Part of the PreDeCon algorithm, see Bohm paper, particularly figure 4 and section 4.1 or Ntoutsi's paper 4.1 intialisation
		:param event_classifications: list of string, one per data point, record if a point already have been added to cluster
		:cluster_id: id of this pcore cluster in list pcore_clusters
		:datapoint_id: id of datapoint
		'''
        neighbourhood = np.copy(self.datapoint_weighted_neighbourhoods[datapoint_id])
        ids = np.copy(self.datapoint_weighted_neighbourhood_ids[datapoint_id])
        if datapoint_id == 1:
            print(ids)
        count = 0
      #  print('after each loop ids',ids)
		#add all neighbourhood which is unclassfied in datapoint_classifications to this core cluster 
        while len(neighbourhood) > 0:
            point = neighbourhood[0]  # process the top item in the queue
            # buffer directly reachable points and index for each point.
            R = []  # the set of directly-reachable points from q.
            indices = []
            direct_reach_function = self.is_direct_reach
 
            for j, other in enumerate(neighbourhood):
 #  marked             if newest_cluster_id == 1 and count == 1:
 #                   print( direct_reach_function(point, ids[0], ids[j], other,True) )
 #                   print(point,neighbourhood[j],ids[0],ids[j])
                if direct_reach_function(point, ids[0], ids[j], other):
                    R.append(other)
                    indices.append(ids[j])
            
            count +=1
            R = np.array(R)  # convert to numpy.array
       #     if newest_cluster_id == 1:    
       #         print('r indices ',indices)
            for r_index in range(R.shape[0]):
                classified_index = indices[r_index]
                r = datapoint_classifications[classified_index]
                # if r is unclassified, add the pmc corresponds to this r to neighbourhood of 'this pmc' as input
                if r == 'f':
                    # r is unclassified, but it is reachable in the cluster being assembled. Hence, add it to the queue.
                    neighbourhood = np.vstack([neighbourhood, R[r_index]])
                    ids = np.append(ids, [indices[r_index]])
                # if r is unclassified or noise do:
                if r == 'f' or r == 'n':
                    # classify the data point against the cluster being assembled.
                    datapoint_classifications[classified_index] = str(newest_cluster_id)
                    # this clustering exercise is intialisation of clusters before execution of HDDStream
                    self.add_point_to_cluster(self.pcore_clusters, newest_cluster_id, R[r_index])
                    self.pcore_clusters[newest_cluster_id].calculate_preferred_dimension(self.pref_dim_variance_threshold_squared,self.p_k)
                    if self.log_clustering_data:
                        self.pcore_data[newest_cluster_id].append(R[r_index])
            # pop item from top of these two queues.
            neighbourhood = np.delete(neighbourhood, 0, axis=0)
            ids = np.delete(ids, 0, axis=0)
    #mark        print('ids',ids)

    def expand_core_cluster(self,datapoint_classifications,newest_cluster_id,datapoint_index):
        """
		See Ntoutsi's paper page993 section 4.3:offline clustering
        """
        neighbourhood = np.copy(self.datapoint_weighted_neighbourhoods[datapoint_index])
        ids = np.copy(self.datapoint_weighted_neighbourhood_ids[datapoint_index])
        print('neighbourhood is,',ids)
        
		#add all neighbourhood which is unclassfied in datapoint_classifications to this core cluster 
        while len(neighbourhood) > 0:
            point = neighbourhood[0]  # process the top item in the queue
            # buffer directly reachable points and index for each point.
            R = []  # the set of directly-reachable points from q.
            indices = []
            direct_reach_function = self.is_offline_direct_reach
 
            for j, other in enumerate(neighbourhood):
                if direct_reach_function(point, ids[0], ids[j], other):
                    R.append(other)
                    indices.append(ids[j])
            R = np.array(R)  # convert to numpy.array
            for r_index in range(R.shape[0]):
                classified_index = indices[r_index]
                r = datapoint_classifications[classified_index]

                # if r is unclassified, add the pmc corresponds to this r to neighbourhood of 'this pmc' as input
                if r == 'f':
                    # r is unclassified, but it is reachable in the cluster being assembled. Hence, add it to the queue.
                    neighbourhood = np.vstack([neighbourhood, R[r_index]])
                    ids = np.append(ids, [indices[r_index]])
                # if r is unclassified or noise do:
                if r == 'f' or r == 'n':
                    # classify the data point against the cluster being assembled.
                    datapoint_classifications[classified_index] = str(newest_cluster_id)
                    # this clustering exercise is between cytometry datasets, ie, between HDDStream runs.
                    # merge pcore cluster to the core cluster contain 'this pmc'
                    self.merge_cores(self.core_clusters[newest_cluster_id], self.pcore_clusters[ids[r_index]])
                    self.current_ids.append(ids[r_index])  # pcore microcluster ids, that form part of this cluster
                    if self.log_clustering_data:
                        self.cluster_data[newest_cluster_id].extend(self.pcore_data[ids[r_index]])
            # pop item from top of these two queues.
            neighbourhood = np.delete(neighbourhood, 0, axis=0)
            ids = np.delete(ids, 0, axis=0)
			
    def merge_cores(self, base, new):
        base.CF1 += new.CF1
        base.CF2 += new.CF2
        base.size += new.size
        base.cumulative_size += new.cumulative_size
        base.calculate_preferred_dimension(self.pref_dim_variance_threshold_squared,self.p_k)	
        for i in new.cluster_id:
            base.cluster_id.append(i)

    def add_point_to_cluster(self, cluster_group, cluster_index, point):
        """ Updates the cluster with a new data point. """
        cluster = cluster_group[cluster_index]
        cluster.CF1 += point
        cluster.CF2 += point ** 2
        cluster.size += 1
        cluster.cumulative_size += 1		

    ''' 
    Agreed I gonna allocate its functionality to two function expand_pcore_cluster and expand_core_cluster first
    def expand(self, datapoint_classifications, newest_cluster_id,
               cluster_group, clusters_pref_dimen, datapoint_index):
       
		For a potential core cluster pmc, add all weighted neighbothoods of ites centroid to it
        Part of the PreDeCon algorithm, see Bohm paper, particularly figure 4 and section 4.1.
        :param datapoint_classifications: list of strings, one per data point (event) to be clustered.
        :param newest_cluster_id:
        :param cluster_group: a list of clusters; this algorithm is applied to datapoints in initialisation, but pcores
            between cytometry data time points.
        :param clusters_pref_dimen: a list of cluster preferred dimensionalities, see :cluster_group comment.
        :param datapoint_index: index of the datapoint being expanded in (various) lists.
        :return:
        
        # this is a queue, which is expanded as we go. It contains datapoints that form part of the cluster being
        # assembled.
		#weighted neighbourhood of the pcore supposed to be expanded
        neighbourhood = np.copy(self.datapoint_weighted_neighbourhoods[datapoint_index])
        # ids of weighted neighbourhood of 'this pmc'
        ids = np.copy(self.datapoint_weighted_neighbourhood_ids[datapoint_index])
        # Expand the neighbourhood of current point and so on to compute an area of high density
        while len(neighbourhood) > 0:
            point = neighbourhood[0]  # process the top item in the queue
            # buffer directly reachable points and index for each point.
            R = []  # the set of directly-reachable points from q.
            indices = []
            # TODO This is ugly, we should not have two algorithm use cases hard coded into the guts of the algorithm.
            # TODO if we separate out PreDeCon into its own class, I would like the data structures it uses to be generic,
            # TODO... that one implementation should be applicable to all use cases.
            direct_reach_function = self.is_offline_direct_reach
            if not self.cluster_seeding_complete:
                direct_reach_function = self.is_direct_reach

            for j, other in enumerate(neighbourhood):
                if direct_reach_function(point, ids[0], ids[j], other):
                    R.append(other)
                    indices.append(ids[j])
            R = np.array(R)  # convert to numpy.array
            for r_index in range(R.shape[0]):
                classified_index = indices[r_index]
                r = datapoint_classifications[classified_index]

                # if r is unclassified, add the pmc corresponds to this r to neighbourhood of 'this pmc' as input
                if r == 'f':
                    # r is unclassified, but it is reachable in the cluster being assembled. Hence, add it to the queue.
                    neighbourhood = np.vstack([neighbourhood, R[r_index]])
                    ids = np.append(ids, [indices[r_index]])
                # if r is unclassified or noise do:
                if r == 'f' or r == 'n':
                    # classify the data point against the cluster being assembled.
                    datapoint_classifications[classified_index] = str(newest_cluster_id)
                    if not self.cluster_seeding_complete:
                        # this clustering exercise is intialisation of clusters before execution of HDDStream
                        self.add_point_to_cluster(cluster_group, newest_cluster_id, R[r_index])
                        clusters_pref_dimen[newest_cluster_id] = self.calculate_pref_dimen(cluster_group[newest_cluster_id])
                        if self.log_clustering_data:
                            self.pcore_data[newest_cluster_id].append(R[r_index])
                    else:
                        # this clustering exercise is between cytometry datasets, ie, between HDDStream runs.
                        # merge pcore cluster to the core cluster contain 'this pmc'
                        self.merge_cores(cluster_group[newest_cluster_id], newest_cluster_id, self.pcore_clusters[ids[r_index]])
                        self.current_ids.append(ids[r_index])  # pcore microcluster ids, that form part of this cluster
                        if self.log_clustering_data:
                            self.cluster_data[newest_cluster_id].extend(self.pcore_data[ids[r_index]])
            # pop item from top of these two queues.
            neighbourhood = np.delete(neighbourhood, 0, axis=0)
            ids = np.delete(ids, 0, axis=0)
    '''

		

#create a new core or pcore cluster,depends on the tyoe of cluster group
    def create_new_core_cluster(self, cluster_group, cluster_id,preferred_dimensions = [], neighbourhood_clusters = []):
        CF1 = np.zeros(self.dataset_dimensionality)
        CF2 = np.zeros(self.dataset_dimensionality)
        cumulative_size = 0.0
        size = 0.0
        preferred_dimensions = np.ones(self.dataset_dimensionality)
        cluster_group.append(cluster(cluster_id,CF1, CF2, cumulative_size,size, preferred_dimensions,neighbourhood_clusters))

    def create_new_outlier_cluster(self, point):
        CF1 = point  # point is a np.array, representing location in multi-dimensional space.
        CF2 = np.array(point) ** 2
        cumulative_size = 1.0
        size = 1.0
        new_outlier_cluster = cluster(CF1,CF2,cumulative_size,size)
        new_outlier_cluster.calculate_preferred_dimension(self.pref_dim_variance_threshold_squared,self.p_k)
        self.outlier_clusters.append(new_outlier_cluster)
		
    def get_preference_neighbourhood(self, index):
        return self.neighbourhoods[index]		
		
    def determine_datapoint_preference_neighbourhoods(self, X):
        """
        Determines the unweighted neighbourhood of each data point in X. The neighbourhood is all points lying
        within a threshold Euclidean distance of the given point, including the given point itself.
        """
        if self.current_data_timestamp == 0 and len(X)< 20:
            f = open('hddstream_record_old.txt','w')   
            f.write(str(X) + '\n')
            f.write(str(self.radius_threshold_squared) + '\n')
        
        self.neighbourhoods = []
        self.neighbourhoods_preferred_dimensions = []
        for point in X:
            neighbourhood = np.array(point)
            neighbourhood_preferred_dimension = np.array(point)
            for other_point in X:
                if self.get_euclidean_dist(other_point, point) <= self.radius_threshold \
                        and self.get_euclidean_dist(other_point, point) != 0:
                    neighbourhood = np.vstack([neighbourhood, other_point])
            self.neighbourhoods.append(neighbourhood)
			#compute the preferred dimension for this neighbourhood set
            sum = 0.0
            for i in range(self.dataset_dimensionality):
                variance = self.get_variance(neighbourhood,point,i)
                if variance <= self.pref_dim_variance_threshold:
                    neighbourhood_preferred_dimension[i] = self.p_k
                else:
                    neighbourhood_preferred_dimension[i] = 1
            self.neighbourhoods_preferred_dimensions.append(neighbourhood_preferred_dimension)
			
			
    def calculate_preference_weighted_neighbourhoods(self, X):
        """ From PreDeCon Definition 5. """
        """
        TODO can we not set self.datapoint_weighted_neighbourhoods and datapoint_weighted_neighbourhood_ids to []
        here, rather than at the end of _initialise() and perform_offline_clustering()?
        """
        sw = Stopwatch()
        for index, point in enumerate(X):
            neighbourhood = []  # assembled iteratively
            ids = []
            for i in range(len(X)):
                # i and index are used to extract preferred dimension of X[i] and point
                dist = self.get_general_preference_dist_squared(i, X[i], index, point)  # squared to avoid sqrt ops
                if dist <= self.radius_threshold_squared:
                    neighbourhood.append(X[i])
                    ids.append(i)
            self.datapoint_weighted_neighbourhoods.append(np.array(neighbourhood))
            self.datapoint_weighted_neighbourhood_ids.append(np.array(ids))
            if self.current_data_timestamp == 0 and index ==1 and i == 7:
                f = open('hddstream_record_old.txt','w')   
                f.write(str(dist) + '\n')
                f.write(str(self.radius_threshold_squared) + '\n')
                f.write(str(self.neighbourhoods_preferred_dimensions[i]) + '\n')
       
         
        print("Calculating pref weighted neighbourhood took %s seconds " % (sw.split()))
        print('--------------------------')

    def get_point_preference_dimensionality(self, neighbourhood, point):
        """ From PreDeCon Definition 2. How many preferred dimensions does point P have, given it's neighbourhood? """
        count = 0
        for dimen in point:
            if self.get_variance(neighbourhood, point, dimen) <= self.pref_dim_variance_threshold:
                count += 1
        return count		

		
		#in that way  a point will be itselves neighbouthood			
    def get_general_preference_dist_squared(self, p_index, p, q_index, q):
        """ From PreDeCon Definition 4. Note this is squared distance. """
        if p_index == q_index:
            return 0.
        return max(self.get_dist_p_squared(p_index, p, q), self.get_dist_p_squared(q_index, q, p))

    def get_dist_p_squared(self,base_index,base,other):
        weight = self.neighbourhoods_preferred_dimensions[base_index]
        sum = 0.0
        for i in range(self.dataset_dimensionality):
            sum += weight[i] * (base[i] - other[i])**2
        return sum

    def get_variance(self,neighbourhood, point, dimen):
        """ From PreDeCon Definition 1. Retrieves the variance of a point's neighbourhood in the given dimension.
         The neighbourhood is defined through Euclidean distance thresholding. 
        """
        return np.sum(
            [self.get_euclidean_dist(other_point[dimen], point[dimen]) ** 2 for other_point in neighbourhood]
            ) / len(neighbourhood)

    def is_core(self, index, x):
      #  print(len(self.datapoint_weighted_neighbourhoods[index]))
        """ From PreDeCon Definition 6. """
        neighbourhood = self.get_preference_neighbourhood(index)
        value = self.get_point_preference_dimensionality(neighbourhood, x) <= self.max_subspace_dimensionality \
                and len(self.datapoint_weighted_neighbourhoods[index]) >= self.predecon_cluster_density_threshold
        return value			
			
    def is_direct_reach(self, p, p_index, q_index, q,):
        """ From PreDeCon Definition 7. """
        # TODO, this is ugly, we should have 1 implementation of PreDeCon, not butchering methods to make it apply in two contexts.
        neighbourhood_p = self.get_preference_neighbourhood(p_index)
        value = self.is_core(q_index, q) \
                and (self.get_point_preference_dimensionality(neighbourhood_p, p) <= self.max_subspace_dimensionality) \
                and (p in self.datapoint_weighted_neighbourhoods[q_index])  # p in q's preference weighted neighbourhood
        return value

    def is_offline_direct_reach(self, p, p_index, q_index, q):
        """
        PreDeCon definition 7, but applied to pcores (between input cytometry datasets) rather than initialisation.
        """
        # TODO, this is ugly, we should have 1 implementation of PreDeCon, not butchering methods to make it apply in two contexts.
        pdim = (np.array(self.pcore_clusters[p_index].preferred_dimension) > 1).sum()
        value = self.check_if_core(self.pcore_clusters[q_index]) and (
            pdim <= self.max_subspace_dimensionality) and (
                    p in self.datapoint_weighted_neighbourhoods[q_index])
        return value
	
    def get_euclidean_dist(self,a, b):
        return np.linalg.norm(np.asarray(a) - np.asarray(b))
		
class Stopwatch:
    """ Convenient class for timing how long things take. """
    def __init__(self, start_now=True):
        self.start_time = None
        self.split_time = None
        if start_now:
            self.start()

    def start(self):
        self.start_time = time.time()

    def split(self):
        self.split_time = time.time()
        return self.get_last_split()

    def get_last_split(self):
        return self.split_time - self.start_time		
		
		

		
