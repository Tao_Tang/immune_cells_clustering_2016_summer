import time

import numpy as np

CF1_POS = 0
CF2_POS = 1
WEIGHT_POS = 2


# This class applies the HDDStream algorithm to incrementally perform clustering. See the following papers for more
# details:
# [1] Ntoutsi, Irene, et al. "Density-based Projected Clustering over High Dimensional Data Streams." SDM. 2012.
# [2] Bohm, Christian, et al. "Density connected clustering with local subspace preferences." Data Mining, 2004.
# ICDM'04. Fourth IEEE International Conference on. IEEE, 2004.
# Author: Deeksha singh
class HDDStream(object):
    def __init__(self, mu_percentage=5, eps=0.2, delta=1, k=100, beta=0.5, decay=1, minPoints_percentage=5, section=0.1,
                 store_cluster_data=False):
        self.history = []
        self.init = False
        # max dimensionality of a subspace
        self.p_lambda = 0.0
        # mu is density threshold for pcore and core microclusters, mu_boundary defines percentage of data to set mu to
        self.p_mu_boundary = mu_percentage
        # eps is the radius threshold
        self.p_eps = eps
        # delta is variance threshold, set later
        self.p_delta = delta
        # k is used to adjust influence of preferred dimensions
        self.p_k = k
        # decay is factor to reduce influence of old data with
        self.p_decay = decay
        # beta is the level of relaxation allowed to potential microclusters, it is within (0,1)
        self.p_beta = beta
        # minPoiint_boundary is the same as mu_boundary but for the initialisation stage using PreDeCon
        self.p_minPoint_boundary = minPoints_percentage
        # section is the part of data to use for initialisation using PreDeCon
        self.p_section = section
        # collection of core microclusters, each cluster has the following values
        # 0: CF1 [], 1:CF2 [], 2: Weight d
        self.pcore_clusters = []
        # the preferred dimensions vector for each potential mucrocluster microclusters
        self.pcore_pref_dim = []
        # collection of outlier microclusters, each cluster has the following values
        # 0: CF1 [], 1:CF2 [], 2: Weight d
        self.outlier_clusters = []
        # the preferred dimensions vector for each outlier microclusters
        self.outlier_pref_dim = []
        self.core_clusters = []
        self.core_pref_dim = []
        self.cluster_ids = []
        self.day = -1
        self.num_dimen = 0
        self.neighbourhoods = []
        self.weighted_neighbourhoods = []
        self.weighted_ids = []
        self.cores = []
        self.current_ids = []
        self.cluster_data = []
        self.pcore_data = []
        self.o_data = []
        self.store_cluster_data = store_cluster_data

    def is_started(self):
        return self.init

    def get_ids(self):
        return np.array([i for i in range(len(self.core_clusters))])

    def get_cluster_data(self):
        return np.array(self.cluster_data)

    def get_pcore_ids(self):
        return self.cluster_ids

    def get_power(self):
        return np.array([pmc[WEIGHT_POS] for pmc in self.core_clusters])

    def get_clusters(self):
        return np.array([self.get_center(pmc) for pmc in self.core_clusters])

    def get_pref_dimen(self):
        return self.core_pref_dim

    def get_num_dimensions(self):
        return self.num_dimen

    def get_percentage_boundary(self, base, percentage):
        return (base / 100) * percentage

    # Sets variance threshold delta
    def set_variance_threshold(self, data):
        maxes = np.nanmax(data, axis=0)
        average = np.nanmean(maxes)
        self.p_delta = average / 5

    def print_params(self):
        p_str = 'running HDDStream with params mu=' + str(self.p_mu) + ' lambda(pi)=' + str(
            self.p_lambda) + ' eps=' + str(self.p_eps) + ' delta=' + str(self.p_delta) + ' decay=' + str(self.p_decay) \
                + ' beta=' + str(self.p_beta) + ' mu percentage ' + str(self.p_mu_boundary) + ' minPoints percentage ' \
                + str(self.p_minPoint_boundary) + ' init percentage ' + str(self.p_section)
        print(p_str)

    def start(self, X):
        print('starting HDDStream')
        # Compute and set some params
        self.p_lambda = X.shape[1]
        self.num_dimen = X.shape[1]
        section_end = self.get_percentage_boundary(X.shape[0], self.p_section)
        self.p_mu = self.get_percentage_boundary(X.shape[0], self.p_mu_boundary)
        self.set_variance_threshold(X[0:section_end])
        self.print_params()
        # Initialise with section of data
        self.initialise(X[0:section_end])
        # Send rest of data from first day to online clustering
        self.newdata(X[section_end:])

    def newdata(self, X):
        self.set_variance_threshold(X)
        self.print_params()
        self.day += 1
        self.p_lambda = X.shape[1]
        self.p_mu = self.get_percentage_boundary(X.shape[0], self.p_mu_boundary)
        self.core_clusters = []
        self.cluster_ids = []
        # Stores cluster points for evaluation
        if self.store_cluster_data: self.cluster_data = []
        self.history = (np.full((len(self.pcore_clusters), 1), False, dtype=bool)).tolist()
        start_time = time.time()
        for i in range(X.shape[0]):
            x = X[i]
            # Try to add x to a potential microcluster
            trial1 = self.add_to_pcore(x)
            trial2 = False
            if not trial1:
                # Try to add x to an outlier microcluster
                trial2 = self.add_to_outliers(x)
            if not trial1 and not trial2:
                # Create new outlier microcluster for x
                self.create_new_outlier_cluster(x)
                if self.store_cluster_data: self.o_data.append([x])
        print("clustering took %s seconds" % (time.time() - start_time))
        print('--------------------------')
        # Decay all p-core mc that have not been updated in the last day by factor p_decay
        print('performing downgrade')
        self.perform_downgrade()
        # Perform offline clustering on p-core mc using PreDeCon to extract final core mc
        print('performing offline clustering with PreDeCon with ' + str(len(self.pcore_clusters)) + ' pcores')
        self.perform_offline_clustering()

    def add_to_pcore(self, p):
        distances = []
        # Calculate distances to all pcore mc from p
        for index, pmc in enumerate(self.pcore_clusters):
            # Update pcore_pref_dimen of pmc temporarily
            pcore_pref_dimen = self.update_pcore_pref_dimen(pmc, p)
            if (pcore_pref_dimen != 1).sum() <= self.p_lambda:
                distances.append([self.get_projected_dist(self.pcore_pref_dim[index], pmc, p), index])

        if len(distances) > 0:
            # Get the closest pcore mc and compute its new properties with p included
            pmc_closest = self.get_closest_cluster(distances)
            new_cluster_dim = self.update_pcore_pref_dimen(self.pcore_clusters[pmc_closest], p)
            new_cluster = self.get_projected_pmc(self.pcore_clusters[pmc_closest], p)
            radius = self.get_radius(new_cluster, new_cluster_dim)
            # If radius condition for pcore mc is still fulfilled for pmc_closest add p to it
            if radius <= self.p_eps ** 2:
                self.add_to_cluster(self.pcore_clusters, pmc_closest, p)
                self.pcore_pref_dim[pmc_closest] = self.calculate_pref_dimen(self.pcore_clusters[pmc_closest])
                if self.store_cluster_data: self.pcore_data[pmc_closest].append(p)
                self.history[pmc_closest] = True
                return True
        return False

    def add_to_outliers(self, p):
        distances = []
        # Calculate distances to all o mc from p
        for index, pmc in enumerate(self.outlier_clusters):
            distances.append([self.get_projected_dist(self.outlier_pref_dim[index], pmc, p), index])

        if len(distances) > 0:
            # Get the closest o mc and compute its new properties with p included
            pmc_closest = self.get_closest_cluster(distances)
            new_cluster_dim = self.update_pcore_pref_dimen(self.outlier_clusters[pmc_closest], p)
            new_cluster = self.get_projected_pmc(self.outlier_clusters[pmc_closest], p)
            radius = self.get_radius(new_cluster, new_cluster_dim)
            if radius <= self.p_eps ** 2:
                current = self.outlier_clusters[pmc_closest]
                current = [current[CF1_POS] + p, current[CF2_POS] + p ** 2, current[WEIGHT_POS] + 1]
                current_pref_dim = self.calculate_pref_dimen(np.array(current))
                # check if adding p to outlier cluster makes it a pcore cluster
                if self.check_if_pcore(current, current_pref_dim):
                    # Make the o mc into a pcore mc
                    self.pcore_clusters.append(current)
                    self.pcore_pref_dim.append(current_pref_dim)
                    if self.store_cluster_data: self.pcore_data.append(self.o_data[pmc_closest])
                    self.history.append(True)
                else:
                    # Add p into current o mc
                    self.outlier_clusters[pmc_closest] = current
                    self.outlier_pref_dim[pmc_closest] = current_pref_dim
                    if self.store_cluster_data: self.o_data[pmc_closest].append(p)
                return True
        return False

    def get_projected_pmc(self, pmc, p):
        return np.array([pmc[CF1_POS] + p, pmc[CF2_POS] + p ** 2, pmc[WEIGHT_POS] + 1])

    # from HDDStream definition 6
    def check_if_pcore(self, cluster, pref_dim):
        radius = self.get_radius(cluster, pref_dim)
        weight = cluster[WEIGHT_POS]
        pdim = (np.array(pref_dim) > 1).sum()
        return radius <= self.p_eps ** 2 and weight >= self.p_beta * self.p_mu and pdim <= self.p_lambda

    # from HDDStream definition 5
    def check_if_core(self, cluster, pref_dim):
        radius = self.get_radius(cluster, pref_dim)
        weight = cluster[WEIGHT_POS]
        pdim = (np.array(pref_dim) > 1).sum()
        return radius <= self.p_eps ** 2 and weight >= self.p_mu and pdim <= self.p_lambda

    def get_closest_cluster(self, distances):
        dist = np.array(distances)
        ids = dist[:, 1]
        dists = dist[:, 0]
        return int(ids[np.nanargmin(dists)])

    # from HDDStream definition 5
    def get_radius(self, pmc, pref_dimen):
        CF1 = pmc[CF1_POS]
        CF2 = pmc[CF2_POS]
        weight = pmc[WEIGHT_POS]
        radius = 0.0
        for i in range(self.num_dimen):
            dimension = 1 / pref_dimen[i]
            value = (CF2[i] / weight) - (CF1[i] / weight) ** 2
            radius += dimension * value
        return radius

    def get_projected_dist(self, pcore_pref_dimen, pmc, p):
        center = [(x / pmc[WEIGHT_POS]) for x in pmc[CF1_POS]]
        dist = 0.0
        for i in range(self.num_dimen):
            dimension = 1 / pcore_pref_dimen[i]
            value = (p[i] - center[i]) ** 2
            dist += dimension * value
        return dist ** (1 / 2)

    def update_pcore_pref_dimen(self, pmc, p):
        new_CF1 = pmc[CF1_POS] + p
        new_CF2 = pmc[CF2_POS] + p ** 2
        new_weight = pmc[WEIGHT_POS] + 1
        return self.calculate_pref_dimen(np.asarray([new_CF1, new_CF2, new_weight]))

    # from HDDStream definition 2 and 3
    def calculate_pref_dimen(self, pmc):
        pref_dimen = np.ones(self.num_dimen)
        CF1 = pmc[CF1_POS]
        CF2 = pmc[CF2_POS]
        weight = pmc[WEIGHT_POS]
        for index in range(self.num_dimen):
            variance = (CF2[index] / weight) - (CF1[index] / weight) ** 2
            if variance <= self.p_delta ** 2:
                pref_dimen[index] = self.p_k
        return pref_dimen

    def perform_downgrade(self):
        for i in range(len(self.history)):
            if not self.history[i]:
                cluster = self.pcore_clusters[i]
                multiplicity = 2 ** (-self.p_decay * self.day)
                cluster[CF1_POS] *= multiplicity
                cluster[CF2_POS] *= multiplicity
                cluster[WEIGHT_POS] *= multiplicity

    def perform_offline_clustering(self):
        # Predecon on pcore mc
        start_time = time.time()
        classified = np.full((len(self.pcore_clusters), 1), 'f', dtype='str')
        all = [self.get_center(cluster) for cluster in self.pcore_clusters]
        self.update_preference_neighbourhood(all)
        self.calculate_preference_weighted_neighbourhood(np.array(all))
        for index, pmc in enumerate(self.pcore_clusters):
            pref_dimen = self.pcore_pref_dim[index]
            if self.check_if_core(pmc, pref_dimen):
                count = len(self.core_clusters)
                self.create_new_core_cluster(self.core_clusters, self.core_pref_dim)
                if self.store_cluster_data: self.cluster_data.append([])
                self.expand(classified, count, self.core_clusters, self.core_pref_dim, index)
                # If nothing has been added to this core mc delete it
                if self.core_clusters[count][WEIGHT_POS] < 1.0:
                    del self.core_clusters[count]
                    del self.core_pref_dim[count]
                    if self.store_cluster_data: del self.cluster_data[count]
                else:
                    self.cluster_ids.append(np.array(self.current_ids))
                    self.current_ids = []
            else:
                classified[index] = 'n'
        self.weighted_neighbourhoods = []
        self.weighted_ids = []
        self.neighbourhoods = []
        print("offline clustering %s seconds" % (time.time() - start_time))
        print('--------------------------')

    def initialise(self, X):
        start_time = time.time()
        # Mark all data points as unclassified
        classified = np.full((X.shape[0], 1), 'f', dtype='str')
        self.p_minPoints = self.get_percentage_boundary(X.shape[0], self.p_minPoint_boundary)
        print('beginning initialisation with data size ' + str(X.shape[0]) + ' minPts ' + str(self.p_minPoints))
        self.update_preference_neighbourhood(X)
        self.calculate_preference_weighted_neighbourhood(X)

        for i in range(X.shape[0]):
            x = X[i]
            if self.is_core(i, x):
                count = len(self.pcore_clusters)
                self.create_new_core_cluster(self.pcore_clusters, self.pcore_pref_dim)
                if self.store_cluster_data: self.pcore_data.append([])
                self.expand(classified, count, self.pcore_clusters, self.pcore_pref_dim, i)
                # If nothing has been added to this pcore mc delete it
                if self.pcore_clusters[count][WEIGHT_POS] < 1.0:
                    del self.pcore_clusters[count]
                    del self.pcore_pref_dim[count]
                    if self.store_cluster_data: del self.pcore_data[count]
            else:
                classified[i] = 'n'
            print('initialisation complete: ' + str((i / X.shape[0]) * 100) + ' %')
        self.init = True
        self.cores = []
        self.weighted_neighbourhoods = []
        self.weighted_ids = []
        print("initialisation %s seconds" % (time.time() - start_time))
        print('--------------------------')

    def expand(self, classified, count, cluster_group, cluster_pref_dimen, index):
        neighbourhood = np.copy(self.get_preference_weighted_neighbourhood(index))
        ids = np.copy(self.weighted_ids[index])
        # Expand the neighbourhood of current point and so on to compute an area of high density
        while len(neighbourhood) > 0:
            point = neighbourhood[0]
            R = []
            indices = []
            direct_reach_function = self.is_offline_direct_reach
            if not self.init:
                direct_reach_function = self.is_direct_reach

            for j, other in enumerate(neighbourhood):
                if direct_reach_function(point, ids[0], ids[j], other):
                    R.append(other)
                    indices.append(ids[j])
            R = np.array(R)
            for r_index in range(R.shape[0]):
                classified_index = indices[r_index]
                r = classified[classified_index]
                # if r is unclassified do:
                if r == 'f':
                    neighbourhood = np.vstack([neighbourhood, R[r_index]])
                    ids = np.append(ids, [indices[r_index]])
                # if r is unclassified or noise do:
                if r == 'f' or r == 'n':
                    classified[classified_index] = str(count)
                    if not self.init:
                        self.add_to_cluster(cluster_group, count, R[r_index])
                        cluster_pref_dimen[count] = self.calculate_pref_dimen(cluster_group[count])
                        if self.store_cluster_data: self.pcore_data[count].append(R[r_index])
                    else:
                        self.merge_cores(cluster_group[count], count, self.pcore_clusters[ids[r_index]])
                        self.current_ids.append(ids[r_index])
                        if self.store_cluster_data: self.cluster_data[count].extend(self.pcore_data[ids[r_index]])
            neighbourhood = np.delete(neighbourhood, 0, axis=0)
            ids = np.delete(ids, 0, axis=0)

    def merge_cores(self, base, base_index, new):
        base[CF1_POS] += new[CF1_POS]
        base[CF2_POS] += new[CF2_POS]
        base[WEIGHT_POS] += new[WEIGHT_POS]
        self.core_pref_dim[base_index] = self.calculate_pref_dimen(base)

    def get_center(self, pmc):
        return np.array([(x / pmc[WEIGHT_POS]) for x in pmc[CF1_POS]])

    def add_to_cluster(self, cluster_group, cluster_index, point):
        cluster = cluster_group[cluster_index]
        cluster[CF1_POS] += point
        cluster[CF2_POS] += point ** 2
        cluster[WEIGHT_POS] += 1

    def create_new_core_cluster(self, cluster_group, pref_group):
        CF1 = np.zeros(self.num_dimen)
        CF2 = np.zeros(self.num_dimen)
        weight = 0.0
        cluster_group.append([CF1, CF2, weight])
        pref_group.append([] * self.num_dimen)

    def create_new_outlier_cluster(self, point):
        CF1 = point
        CF2 = np.array(point) ** 2
        weight = 1.0
        self.outlier_clusters.append([CF1, CF2, weight])
        self.outlier_pref_dim.append(self.calculate_pref_dimen([CF1, CF2, weight]))

    def get_preference_neighbourhood(self, index):
        return self.neighbourhoods[index]

    def update_preference_neighbourhood(self, X):
        self.neighbourhoods = []
        for point in X:
            neighbourhood = np.array(point)
            for other_point in X:
                if self.get_euclidean_dist(other_point, point) <= self.p_eps:
                    neighbourhood = np.vstack([neighbourhood, other_point])
            self.neighbourhoods.append(neighbourhood)

    # From PreDeCon Definition 2
    def get_preference_dimen(self, neighbourhood, point):
        count = 0
        for dimen in point:
            if self.get_variance(neighbourhood, point, dimen) <= self.p_delta:
                count += 1
        return count

    # From PreDeCon Definition 1
    def get_variance(self, neighbourhood, point, dimen):
        return np.sum(
            [self.get_euclidean_dist(other_point[dimen], point[dimen]) ** 2 for other_point in neighbourhood]) / len(
            neighbourhood)

    # From PreDecon Definition 3
    def get_dist(self, base_index, base, other):
        # print('base ' + str(base))
        weight = np.ones(self.num_dimen)
        pref_neighbour = self.get_preference_neighbourhood(base_index)
        sum = 0.0
        avg = 0.0
        for i in range(self.num_dimen):
            variance = self.get_variance(pref_neighbour, base, i)
            avg += variance
            if variance <= self.p_delta:
                weight[i] = self.p_k
            else:
                weight[i] = 1
            sum += ((base[i] - other[i]) ** 2) * (weight[i])
        return sum

    # From PreDeCon Definition 4
    def get_prefered_dist(self, p_index, p, q_index, q):
        if p_index == q_index:
            return 0
        return max(self.get_dist(p_index, p, q), self.get_dist(q_index, q, p))

    # From PreDeCon Definition 5
    def calculate_preference_weighted_neighbourhood(self, X):
        start_time = time.time()
        for index, point in enumerate(X):
            neighbourhood = []
            ids = []
            for i in range(len(X)):
                dist = self.get_prefered_dist(i, X[i], index, point)
                if dist <= self.p_eps ** 2:
                    neighbourhood.append(X[i])
                    ids.append(i)
            self.weighted_neighbourhoods.append(np.array(neighbourhood))
            self.weighted_ids.append(np.array(ids))
        print("Calculating pref weighted neighbourhood took %s seconds " % (time.time() - start_time))
        print('--------------------------')

    def get_preference_weighted_neighbourhood(self, index):
        return self.weighted_neighbourhoods[index]

    # From PreDeCon Definition 6
    def is_core(self, index, x):
        neighbourhood = self.get_preference_neighbourhood(index)
        weighted_pref_neighbourhood = self.get_preference_weighted_neighbourhood(index)
        value = self.get_preference_dimen(neighbourhood, x) <= self.p_lambda and len(
            weighted_pref_neighbourhood) >= self.p_minPoints
        return value

    # From PreDeCon Definition 7
    def is_direct_reach(self, p, p_index, q_index, q):
        neighbourhood_p = self.get_preference_neighbourhood(p_index)
        weighted_neighbourhood_q = self.get_preference_weighted_neighbourhood(q_index)
        value = self.is_core(q_index, q) and (self.get_preference_dimen(neighbourhood_p, p) <= self.p_lambda) and (
            p in weighted_neighbourhood_q)
        return value

    def is_offline_direct_reach(self, p, p_index, q_index, q):
        weighted_neighbourhood_q = self.get_preference_weighted_neighbourhood(q_index)
        pdim = (np.array(self.pcore_pref_dim[p_index]) > 1).sum()
        value = self.check_if_core(self.pcore_clusters[q_index], self.pcore_pref_dim[q_index]) and (
            pdim <= self.p_lambda) and (
                    p in weighted_neighbourhood_q)
        return value

    def get_euclidean_dist(self, a, b):
        return np.linalg.norm(np.asarray(a) - np.asarray(b))
