import numpy as np
import time

from HDDStream import HDDStream

# factorial experimental design. Samples parameter space, and creates
# xml parameter files for each sample. These are stored in a directroy sturcture
# along with pbs scripts (design for use on Artemis). 
#
# The LHC design is written to a CSV for use in downstream analysis. 
#
# Intended to be executed from the MPN directory.
#
# Mark N. Read, 2016

import numpy as np
import pandas 
import lhsmdu # Latin hypercube design. From https://github.com/sahilm89/lhsmdu
import pyDOE # factorial design in here
from pyDOE import *
import xml.etree.ElementTree as ET
import os
import shutil
import sys
import datetime

outDir = 'factorial_design' # set up dir for experimental data. 
design_fp = outDir + '/src/hddstream_param_spec.csv'
default_params_fp = outDir + '/src/HDDStream_parameter.xml'

divisions = 5 # how many samples per dimension.
replicates = 1 # replicate simulations for each point in parameter space. 

maxQueued = 100
chain = True
cumseed = 0 # cumulative count of seed, ensures experimental diversity by never reusing seed.

date = datetime.date.today().strftime('%B,%d,%Y')

sampleDir = outDir + '/samples_' + date
dataDir = outDir + '/data_' + date
pbsDir = outDir + '/pbs_' + date


if not os.path.exists(sampleDir):
    os.makedirs(sampleDir)

if not os.path.exists(dataDir):
    os.makedirs(dataDir)

if not os.path.exists(pbsDir):
    os.makedirs(pbsDir)

# user-specified. Which parameters, what ranges. 
params = pandas.read_csv(design_fp, sep=',')


def write_pbs_script(fp, expDir, param_fp, cumseed, nextJob=None):
	s = ('#!/bin/bash\n'
		'#PBS -P RDS-FSC-cytoclust-RW\n'
		'#PBS -l select=1:ncpus=1:mem=50GB\n'
		'#PBS -l walltime=120:00:00\n'
		'#PBS -q defaultQ\n'
		'#PBS -e /project/RDS-FSC-cytoclust-RW/tao_tang/{expDir:s}/\n'
		'#PBS -o /project/RDS-FSC-cytoclust-RW/tao_tang/{expDir:s}/\n'
		'\n'
		'cd /project/cytoclust/tao_tang\n'
		'touch $MPN_OUTDIR/pbs.starting\n'
		'/project/cytoclust/anaconda3/bin/python3 /project/cytoclust/tao_tang/main.py -inputfp /project/cytoclust/tao_tang/{param_fp:s}\n'
		'touch $MPN_OUTDIR/pbs.finished\n').format(replicates=replicates-1, 
		expDir=expDir, param_fp=param_fp, cumseed=cumseed)
	if nextJob:
		s += 'qsub ' + '/project/cytoclust/tao_tang/' + nextJob + '\n'
	with open(fp,'w') as script:
		script.write(s)

 
p = params.shape[0] # number of parameter
totalSamples = divisions ** p # how many samples in space, in total.
print('parameters:',p,' totalSamples:',totalSamples) 
# creates a factorial design, with numbers ranging from 0..divisions (integers). 
# argument: list containing p entries of number divisions
k = pyDOE.fullfact([divisions] * p) 
# pyDOE returns array.shape = (divisions, p), but I need (p, divisions). 

k = k.transpose()
k /= (divisions-1) # scales everything onto 0..1. 

for i in range(p):
	# scale the design for each parameter onto the correct range
	param_range = params['max'][i] - params['min'][i]
	k[i] = (k[i] * param_range) + params['min'][i]


# write xml parameter files for each sample of parameter space. 
for d in range(totalSamples): # one per sample of parameter space. 
	sample_fp = sampleDir + '/parameters_samp{sid:d}.xml'.format(sid=d)
	tree = ET.parse(default_params_fp)  # create a fresh copy of the parameters file
	root = tree.getroot()
	# populate each parameter's value for this sample
	for p in range(params.shape[0]):
		value = k[p, d]
		root.find(params['xpath'][p]).text = str(value)
		tree.write(sample_fp)	# write the parameters to the filesystem.
	# create simulation data directories 
	expDir = dataDir + '/{sid:d}'.format(sid=d)
	for rep in range(replicates):
		if(os.path.exists(expDir + '/rep{rep:d}'.format(rep=rep)) is False):
			os.makedirs(expDir + '/rep{rep:d}'.format(rep=rep))
	# create an array job pbs script for each sample. 
	pbs_fp = '{pbs:s}/{sid:d}.pbs'.format(pbs=pbsDir, sid=d)
	nextJob = None
	print(d)
	if chain and (d < totalSamples - maxQueued):
		nj = d + maxQueued
		print ('next job = ',str(nj))
		nextJob = '{pbs:s}/{sid:d}.pbs'.format(pbs=pbsDir, sid=nj)
	write_pbs_script(fp=pbs_fp, expDir=expDir, param_fp=sample_fp, cumseed=cumseed, nextJob=nextJob)
	cumseed += replicates # never use the same seed twice

# convenience script that will submit all pbs files automatically. 
out = None
firstJobs = totalSamples
if chain:
	firstJobs = maxQueued
for d in range(firstJobs):	
	if d % maxQueued == 0:
		if out is not None:
			out.close()
		out = open(pbsDir+'/submitAll_' + str(d // maxQueued) +'.sh', 'w')	
		out.write('#!/bin/bash\n') # makes file executable?		
		out.write('cp -r /project/HSCCH/MPN/src /project/HSCCH/MPN/' + outDir + '/.\n')
	out.write('qsub {d:d}.pbs\n'.format(d=d))
out.close()

# write the LHC experimental design to the FS as a csv
dst = outDir+'/lhc_param_spec.csv'
if not os.path.exists(dst):
	shutil.copyfile(src=design_fp, dst=dst) # start with the parameter design
design = pandas.DataFrame(k.transpose()) # params on columns, samples on rows. 
design.columns = params['xpath'].tolist() # xpath as column names.
design.to_csv(outDir+'/lhc_design.csv')

file = open("factorial_design.txt", "w")
file.write('parameters:{p:d}\n'.format(p=p))
file.write('divisions:{d:d}\n'.format(d=divisions))
file.write('replicates:{r:d}\n'.format(r=replicates))


file.close()

