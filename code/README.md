# README #

## Required packages ##
python 3.4
numpy
pandas
scikit-learn
scipy
matplotlib


## Files ##

### firstimp ###
firstimp.py is the main handler for the clustering algorithms

#### ECM ####
 - run using ./firstimp.py ecm WNV1.txt 
 
  - where ecm is the algorithm and WNV.txt is a next file containing the filenames of the cytometry data in order of appearance e.g.(wnv1_day_1.csv on line 1, wnv1_day_2.csv on line 2)
  
 - specific settings of the algorithm can be found in the init stage
 
   * 1st is initial clusters(no real reason to change this)
  
   * 2nd is how many samples are kept in main memory(more = slower, but better)
  
   * 3rd is how often to include a sample(i.e. if 3, roughly 1/3 samples will be kept)
  
   * 4th defines how to reset the clusters after each iteration. Refer to ECM.py for details
  
   * 5th is the main paramater p which controls the minimum distance required between a cluster and a new point for the new point to define a cluster. 
  Refer to the paper for details on this paramater


ECM.py contains the clustering algorithm Evolving CLuster Models with Split and Merge Using Extended Vector Quantization.
Refer to the paper for full details. 
The first lines of def newdata detail how the reset paramater works.

To run ECM with Satellite Deletion add flag --remove_outliers

To run clustering evaluation add flag --evaluate limit where limit is optional and constraints size of input data


#### THDDStream ####
Run with ./firstimp hddstream WNV1.txt

Parameters can be set optionally with flags -mu -eps -minPoints -beta -decay -init

HDDStream contains the clustering algorithm executed using the works of Ntoutsi et al. and Bohm et al.

To add incremental cluster tracking for THDDStream execute track_history.py afterwards.

To run clustering evaluation add flag --evaluate limit where limit is optional and constraints size of input data

See the honours thesis for more details


### track_history ###
Gives incremental id to clusters produced by HDDStream using containing pcore microclusters. Call after execution of
HDDStream. This function completed THDDStream.

Run with ./track_history.py -i input_file.csv -o output_dir where input_file is the file outputted by HDDStream and
output_dir is the directory to output results.


### secondimp ###
secondimp.py allows for a 3rd argument directing to a csv to be made.

 - This csv should have Columns Paramater, Minimum Value and any others deemed informative.
 
 - Clustering is done for each row where only data is examined if it is above the cutoff for the paramter chosen
 

### vis_ecm ###
vis_ecm.py visualizes the clsuter_ids as the evolve over time, more details in paper.
 
 - run with ./vis_ecm.py cluster_file.csv


### get_interest ###
get_interest.py runs some statistical analysis on clusters.
 
 - run with ./get_interest.py cluster_file.csv


### test_vis ###
test_vis.py visualizes clusters in a one vs all way
 
 - run with ./test_vis.py cluster_file.csv x, where x is 'all' for an iterative display or a column name for a single display 
 
 - may need to adjust code if number of paramaters changed so an optimal image is given
 
assign_data.py assigns all data points to a cluster and outputs similar files to the input files

 - run with ./assign_data  WNV1.txt cluster_file.csv wher both are described above
 

### clustering1  and clustering2 ###
Clustering1.py, clustering2.py are two attempts at using an Artificial Recogniton Ball algorithm. 
These may no longer run with the current code, however the algorithms performed poorly anyway.


### MS ###
MS.py runs Mean Shift as implemented in ski-kit learn. This does not do dynamic clustering, but is a usefull tool to compare the number of clusters obtained.

### get_statistics ###
get_statistics.py produces a spreadsheet of general statistics: min, max, median, mean and std for each dimension

 - run with ./get_statistics csv_data_file x where x is a combination of 's', 'b', 'n' to generate scatter, bar or normal graphs respectively.

 - a data_stats folder will be created and all figures will be saved in the folder ./data_stats/<filename>
 - a statistics csv is created to summarise each dimension in the input file
 - for each dimension a boxplot is generated


### clustering_evaluation ###
clustering_evaluation.py computes a modified Silhouette Coefficient and Davis-Bouldin Indicies. Plots are saved and data is logged.
This class should be called after clustering to calculate quality of clusters.
First call set_save_location(dir) method to set the save location.
Then call evaluate(self, cluster_data, cluster_centroids) to compute quality. cluster_data are points within clusters and the cluster_centroids are the centres.

### concatenate_files ###
Concatenates CSV files of input data. Used to combine the four CSV files representing four mice used in WNV experiments.
Output is saved to /Data_set/Data_set/Concatenated

Run with ./concatenate_files.py files.txt  where files.txt contains list of files to concatenate.


### generate_dummy_data ###
Generates synthetic data to test clustering on. Run with ./generate_dummy_data.py -o output_dir
where output_dir is the directory to save all outputs.


### pick_lambda ###
Generates plots to approximate lambda to use for a data set for THDDStream.
Run with ./pick_lambda.py


### remove_outliers ###
This python file removes data points outside of a mean +- standard deviation threshold. This was concluded not to be
the best method of outlier removal for biological data.
Run with ./remove_outliers.py file.csv upper_limit lower_limit
where file.csv is the data file, and the limits are the thresholds: [mean - lower_limit * sd, mean + upper_limit * sd]


### satellite_deletion ###
This uses the work of Lughofer et al. to remove low signficant clusters after clustering with ECM. This was only
executed after day 7 so to not lose important data throughout clustering.

Call inside another class after clustering with ECM by initialising Satellite_deletion and then calling
run_sd(clusters, c_maxes, c_mins, powers, c_ids) where clusters are cluster centroids, c_maxes are maximums at every
dimension and c_mins minimum, powers are the size of clusters, and c_ids are id given to clusters by ECM

To save clusters as a new file call save_clusters()


### vis_cluster_scatter ###
Creates 2D scatter plots of all dimensions against each other and can place cluster centers and labels.

Run with ./vis_cluster_scatter.py -i input_data -o output_dir where input_data is the input file of raw data and
output_dir is the results directory. Add -c flag with the file contianing clusters to place on graphs. The -d flag must
be used with -c to specify day of clustering. -label_clusters will place the cluster id on the centroids.


