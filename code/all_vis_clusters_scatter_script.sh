#!/bin/bash
# Script to generate scatter plots for all days of disease. Will produce scatter graphs of all data and cluster centers
python ./vis_clusters_scatter.py -i ../Simplified-reordered/CSV/export_export_Sample_1_CF200_1_1.csv -o hdd_simp -c ./track_history_results/hddstream_Simplified_WNV_all_.csv -d 0 -label_clusters
python ./vis_clusters_scatter.py -i ../Simplified-reordered/CSV/export_export_Sample_5_CF200_5_5.csv -o hdd_simp -c ./track_history_results/hddstream_Simplified_WNV_all_.csv -d 1 -label_clusters
python ./vis_clusters_scatter.py -i ../Simplified-reordered/CSV/export_export_Sample_9_CF200_9_9.csv -o hdd_simp -c ./track_history_results/hddstream_Simplified_WNV_all_.csv -d 2 -label_clusters
python ./vis_clusters_scatter.py -i ../Simplified-reordered/CSV/export_export_Sample_13_CF200_13_13.csv -o hdd_simp -c ./track_history_results/hddstream_Simplified_WNV_all_.csv -d 3 -label_clusters
python ./vis_clusters_scatter.py -i ../Simplified-reordered/CSV/export_export_Sample_18_CF200_18_18.csv -o hdd_simp -c ./track_history_results/hddstream_Simplified_WNV_all_.csv -d 4 -label_clusters
python ./vis_clusters_scatter.py -i ../Simplified-reordered/CSV/export_export_Sample_21_CF200_21_21.csv -o hdd_simp -c ./track_history_results/hddstream_Simplified_WNV_all_.csv -d 5 -label_clusters
python ./vis_clusters_scatter.py -i ../Simplified-reordered/CSV/export_export_Sample_25_CF200_25_25.csv -o hdd_simp -c ./track_history_results/hddstream_Simplified_WNV_all_.csv -d 6 -label_clusters
python ./vis_clusters_scatter.py -i ../Simplified-reordered/CSV/export_export_Sample_32_CF200_32_32.csv -o hdd_simp -c ./track_history_results/hddstream_Simplified_WNV_all_.csv -d 7 -label_clusters
