import numpy as np
import matplotlib.pyplot as plt
import os

# Calculates a modified Silhouette Coefficient and Davis-Bouldin Indices for clusters
# For more information see the honours thesis
# Author: Deeksha Singh

class clustering_evaluation(object):
    def __init__(self, eval_limit):
        self.silhouettes = []
        self.silhouettes_per_day = []
        self.cluster_ids = []
        self.dbs = []
        self.save_location = '/results'
        self.eval_limit = eval_limit

    def set_save_location(self, save_dir):
        self.save_location = save_dir
        os.makedirs(self.save_location, exist_ok=True)

    # cluster_data is all points within clusters
    # eg. [[cluster1_point1, cluster1_point2, cluster1_point3],[cluster2_point1, cluster2_point2],[cluster3_point1, cluster3_point2]]
    # cluster_centroids are the matching cluster centers
    # eg. [cluster1, cluster2, cluster3]
    def evaluate(self, cluster_data, cluster_centroids):
        # run evaluation for data given
        print('beginning evaluation and saving to ' + self.save_location)
        self.calculate_silhouettes(cluster_data, cluster_centroids)
        self.calculate_davis_bouldin_criteria(cluster_data, cluster_centroids)

    def calculate_silhouettes(self, cluster_data, cluster_centroids):
        silhouettes_for_current_day = []
        print('silhouette: with data ' + str(len(cluster_data)) + ' centroids ' + str(len(cluster_centroids)))
        for index, cluster in enumerate(cluster_data):
            print('cluster size ' + str(len(cluster)))
            np_cluster = np.asarray(cluster)
            silhouettes_in_cluster = []
            for i, point in enumerate(np_cluster):
                other_points = np.delete(np_cluster, i, axis=0)
                a_average_dist = 0.0
                # Calculate cohesion
                if len(other_points) > 0:
                    a_average_dist = np.mean(
                        [self.get_euclidean_dist(point, other_point) for other_point in other_points])

                # Calculate separation
                distances_to_other_clusters = []
                for j, other_cluster in enumerate(cluster_data):
                    if j != index:
                        distances_to_other_clusters.append(self.get_euclidean_dist(point, cluster_centroids[j]))
                b_minimum_dist = np.min(distances_to_other_clusters)

                # Calculate silhouette for i
                silhouette = (b_minimum_dist - a_average_dist) / max(a_average_dist, b_minimum_dist)
                silhouettes_in_cluster.append(silhouette)

            silhouettes_for_current_day.append(np.mean(silhouettes_in_cluster))
            print('sil for day ' + str(np.mean(silhouettes_in_cluster)))
        self.silhouettes.append(silhouettes_for_current_day)
        self.silhouettes_per_day.append(np.mean(silhouettes_for_current_day))
        print('sil for all ' + str(np.mean(silhouettes_for_current_day)))

    def calculate_davis_bouldin_criteria(self, cluster_data, cluster_centroids):
        db = 0
        k = len(cluster_data)
        average_within_distances = []
        # Calculate average distance from each point in a cluster to its centroid
        for index, cluster in enumerate(cluster_data):
            center = cluster_centroids[index]
            di = np.mean([self.get_euclidean_dist(point, center) for point in cluster])
            average_within_distances.append(di)

        # Calculate within_to_cluster ratios
        for i, cluster in enumerate(cluster_data):
            within_to_cluster_ratios = []
            for j, cluster2 in enumerate(cluster_data):
                if i != j:
                    dij = (average_within_distances[i] + average_within_distances[j]) / self.get_euclidean_dist(
                        cluster_centroids[i], cluster_centroids[j])
                    within_to_cluster_ratios.append(dij)
            db += np.max(within_to_cluster_ratios)
        db /= k
        self.dbs.append(db)
        print('db ' + str(db))

    # Compute Euclidean distance between two points
    def get_euclidean_dist(self, a, b):
        return np.linalg.norm(np.asarray(a) - np.asarray(b))

    def save(self):
        self.save_silhouette_plot_all_days()
        self.save_silhouette_plot_per_day()
        self.save_db_indices()

    def save_silhouette_plot_all_days(self):
        # 1 main plot showing all days
        f, ax = plt.subplots()
        ax.scatter(np.arange(0, len(self.silhouettes_per_day)), self.silhouettes_per_day)

        plt.xticks(np.arange(0, len(self.silhouettes_per_day)), np.arange(0, len(self.silhouettes_per_day)))
        ax.set_xlabel('Days')
        ax.axis('tight')
        ax.set_title('Silhouette coefficients over 7 days')
        ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey',
                      alpha=0.5)
        mng = plt.get_current_fig_manager()
        mng.resize(*mng.window.maxsize())
        f.set_size_inches(18, 10)

        plt.savefig(self.save_location + '/overall_silhouette.png', format='png', dpi=200)
        plt.close(f)

    def save_silhouette_plot_per_day(self):
        # Create and save plot for each day of clustering
        day_index = 1
        for i, day in enumerate(self.silhouettes):
            data_to_plot = day
            f, ax = plt.subplots()
            ax.scatter(np.arange(0, len(data_to_plot)), data_to_plot)

            plt.xticks(np.arange(0, len(data_to_plot)))
            ax.set_xlabel('cluster')
            ax.axis('tight')
            ax.set_title('Silhouette Coefficients for day ' + str(day_index))
            ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey',
                          alpha=0.5)
            mng = plt.get_current_fig_manager()
            mng.resize(*mng.window.maxsize())
            f.set_size_inches(18, 10)

            plt.savefig(self.save_location + '/silhouettes_day_' + str(day_index) + '.png', format='png', dpi=200)
            plt.close(f)

            day_index += 1

    def save_db_indices(self):
        # 1 main plot showing all days of db indices
        f, ax = plt.subplots()
        ax.scatter(np.arange(0, len(self.dbs)), self.dbs)

        plt.xticks(np.arange(0, len(self.dbs)), np.arange(0, len(self.dbs)))
        ax.set_xlabel('Days')
        ax.axis('tight')
        ax.set_title('Davis-Bouldin indices over all days')
        ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey',
                      alpha=0.5)
        mng = plt.get_current_fig_manager()
        mng.resize(*mng.window.maxsize())
        f.set_size_inches(18, 10)

        plt.savefig(self.save_location + '/db.png', format='png', dpi=200)
        plt.close(f)
