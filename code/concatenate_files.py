# Concatenates CSV files. Used to combine multiple csv representing multiple rats for each day of WNV
# Author: Deeksha Singh

import pandas as pd
import sys

import os

RUNFILE = sys.argv[1]
os.makedirs('../Data_set/Data_set/Concatenated', exist_ok=True)

class ConcatenateFiles:
    def __init__(self):
        name = RUNFILE[:-4].split('/')
        self.run_file_name = name[len(name) - 1]
        self.save_file = '../Data_set/Data_set/Concatenated/' + self.run_file_name + '.csv'

    def run(self):
        print('running: ' + RUNFILE)
        print('save file: ' + self.save_file)

        if not os.path.isfile(RUNFILE):
            print('missing file: ', RUNFILE)
            return

        data = []
        frame = pd.DataFrame()
        for line in open(RUNFILE):
            line = line[:-1]  # skip end character
            if not os.path.isfile(line):
                print("missing file: ", line)
                continue
            print("starting file: ", line)
            data.append(pd.read_csv(line, index_col=None, header=0))

        frame = pd.concat(data)
        print('saving to ' + self.save_file)
        frame.to_csv(self.save_file, index=False)


if __name__ == "__main__":
    ConcatenateFiles().run()
