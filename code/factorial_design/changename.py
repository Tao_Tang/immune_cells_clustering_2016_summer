import os

dir = 'results_full_dataset'
os.chdir(dir)
for i in range(625):
	fp1 = 'hddstreamFebruary,24,2017_config_parameters_samp{i:d}__normalised.csv'.format(i=i)
	if os.path.isfile(fp1):
		newname = 'hddstreamFebruary,25,2017_config_parameters_samp{i:d}__normalised.csv'.format(i=i)
		os.system('mv '+ fp1 + ' ' + newname)

	fp2 = 'hddstreamFebruary,24,2017_config_parameters_samp{i:d}__.csv'.format(i=i)
	if os.path.isfile(fp2):
		newname = 'hddstreamFebruary,25,2017_config_parameters_samp{i:d}__.csv'.format(i = i)
		os.system('mv '+ fp2 + ' ' + newname)
	
	fp3 = 'hddstreamFebruary,24,2017_config_parameters_samp{i:d}__with_preferred_dimensions.csv'.format(i = i)
	if os.path.isfile(fp3):
		newname = 'hddstreamFebruary,25,2017_config_parameters_samp{i:d}__with_preferred_dimensions.csv'.format(i=i)
		os.system('mv '+ fp3 + ' ' + newname)
