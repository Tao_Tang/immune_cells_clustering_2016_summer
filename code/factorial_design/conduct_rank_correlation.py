
import csv
import numpy as np
import pandas
import scipy.stats as stats

table_fp = 'results_analysis_10,Feb.csv'
save_fp = 'spearman_rank_correlation_10,Feb.csv'

index = np.array(pandas.read_csv(table_fp,header = None,sep = ','))[0,:]
 
#remove the first row in, which is index
table = np.array(pandas.read_csv(table_fp, sep=','))[1:]

#number of parameters
para_num = 4
#number of coefficient used to analyse result, assume last column is index
coefficient_num = table.shape[1] - para_num - 1
#possible value for each dimension(assumed to be equal in facorial design)
divisions = 5

def generate_correlation_coefficient(fp):
	with open(save_fp,'w') as csvfile:
		writer = csv.writer(csvfile,dialect='excel')
		#add 'corr pvalue' as first column
		
		coefficients = []
		for c in index[para_num:para_num+coefficient_num]:
			coefficients.append(c)
		
		writer.writerow(np.hstack((np.array(['correlation,p-value']),coefficients)))
		for i in range(para_num):
			newline = [index[i]]
			x = table[:,i]
			values = np.unique(x)
			x1 = values
			
			for k in range(para_num,para_num+coefficient_num):
				y = table[:,k]
				y1 = []					
				correlation, pvalue=  stats.spearmanr(x,y)
		#		correlation = float('{0:.12f}'.format(correlation))
		#		pvalue = float('{0:.12f}'.format(pvalue))
				newline.append([correlation,pvalue])
				
			writer.writerow(newline)

generate_correlation_coefficient(table_fp)


