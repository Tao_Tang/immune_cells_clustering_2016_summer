import csv
import pandas
import os
import numpy as np
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#set font size 


plt.rcParams.update({'font.size': 18})

figure_size = (10,8)

#table file path,assume the first n line are parameters and rest are measurements
table_fp = 'results_analysis_21,Feb.csv'
outfp = 'diagram_21,Feb'

index = np.array(pandas.read_csv(table_fp,header = None,sep = ','))[0,:]
#remove the first row in, which is index
table = np.array(pandas.read_csv(table_fp, sep=','))[1:]

#number of parameters
para_num = 4
#number of coefficient used to analyse result,remove last two columns,which are result fp index and preferred dimension rate
coefficient_num = table.shape[1] - para_num - 2
#possible value for each dimension(assumed to be equal in facorial design)
divisions = 5
colour = ['red','green','blue','yellow','orange']

print('remeber to change para_num if xml file change')

#return position of each pair for (parameter1,parameter2)
def value_positions(c1,c2):
	results = []
	
	v1 = np.unique(c1)
	v2 = np.unique(c2)
	
	for x in v1:
		ar1 = np.where(c1==x)
		for y in v2:
			
			ar2 = np.where(c2==y)
			results.append(np.intersect1d(ar1,ar2))
			
	return results
 		
# return all pairs for (parameter1 ,parameter2)
def value_pairs(c1,c2):
	results = []
	v1 = np.unique(c1)
	v2 = np.unique(c2)
	
	for x in v1:
		for y in v2:
			results.append([x,y])
	return results

a = np.array([1,2,3,4,1,2,3,4,1])
b = np.array([1,1,2,2,3,3,4,4,1])

def generate_two_dimesnion():
	for i in range(para_num):
		for j in range(i+1,para_num):
			# column of paramters for each graph
			x = table[:,i]
			y = table[:,j]

			positions = value_positions(x,y)
			
			x1 = []
			y1 = []
			pairs = value_pairs(x,y)
			for p in pairs:
				x1.append(p[0])
				y1.append(p[1])
				

			for k in range(para_num,para_num+coefficient_num):
				fig = plt.figure()
				ax = fig.gca(projection = '3d')
				#column of coefficient for each graph
				z = table[:,k]
				
				#average of coefficient of different pairs
				z1 = []
				for p in positions:
					z1.append(np.average(z[p]))

				for l in range(divisions):
					start = l * divisions
					end = (l+1) * divisions
					ax.plot(x1[start:end],y1[start:end],z1[start:end],color = colour[k-para_num])
	#			patch = mpatches.Patch(color= colour[k-para_num], label= index[k])
	#			plt.legend(handles=[patch])

				ax.set_xlabel(index[i])
				ax.set_ylabel(index[j])
				ax.set_zlabel(index[k])	
			
				fig = plt.gcf()
	#			plt.show()
				fig.savefig(outfp + '/plot({i:d},{j:d},{k:d}).png'.format(i=i,j=j,k=k))

def generate_heatmap():
	
	for i in range(para_num):
		for j in range(i+1,para_num):
			# column of paramters for each graph
			x = table[:,i]
			y = table[:,j]

			positions = value_positions(x,y)
			
			x1 = []
			y1 = []
			pairs = value_pairs(x,y)
			for p in pairs:
				x1.append(p[0])
				y1.append(p[1])
				
			for k in range(para_num,para_num+coefficient_num):
				z = table[:,k]
				z1 = []
				for p in positions:
					z1.append(np.average(z[p]))
				
				fig = plt.figure(figsize = figure_size)
				ax = fig.add_subplot(111)
				
				
				x1 = np.reshape(x1,(5,5))
				y1 = np.reshape(y1,(5,5))
				z1 = np.reshape(z1,(5,5))
					
				xlabel = index[i].replace('./HDDStream/','')
				ylabel = index[j].replace('./HDDStream/','')
				ax.set_xlabel(xlabel)
				ax.set_ylabel(ylabel)

				m = index[k]
				
				ax.set_title(m)
				plt.contourf(x1,y1,z1,5)
				plt.colorbar()				
				

				
				fig.savefig(outfp+'/heatmap_{x:s}_{y:s}_{z:s}.png'.format(x = xlabel,y = ylabel,z = m))
				plt.close()

def one_dimension():
	for i in range(para_num):
			x = table[:,i]
			values = np.unique(x)
			x1 = values
	
			for k in range(para_num,para_num+coefficient_num):
				fig = plt.figure()
				y = table[:,k]
				y1 = []
				for value in values:
					position = np.where(x==value)		
					y1.append(np.average(y[position]))
		
				plt.boxplot(x1,y1)
				
				plt.xlabel(index[i])
				plt.ylabel(index[k])	
		
				fig = plt.gcf()
				fig.savefig(outfp+ '/1_dimension({i:d},{k:d}).png'.format(i=i,k=k))	
                   
def generate_boxplot():
	for i  in range(para_num):
		x = table[:,i]
		values = np.unique(x)
		x1 = values
		
		for k in range(para_num,para_num+coefficient_num):
			# create figure object
			y = table[:,k]
			y1 = []
			for value in values:
				position = np.where(x==value)
				y1.append(y[position])
			
			fig = plt.figure(figsize = figure_size)
			ax = fig.add_subplot(111)
			bp = ax.boxplot(y1)
			
			xlabel = index[i].replace('./HDDStream/','')
			
			
			ax.set_xlabel(xlabel)
			m = index[k]
			
			ax.set_ylabel(m)
			ax.set_xticklabels(x1)
			
			for box in bp['boxes']:
			# change outline color
				box.set( color='#7570b3', linewidth=2)
				# change fill color
				box.set(mfc='#1b9e77' )
				box.set(mfcalt = '#1b9377')

			## change color and linewidth of the whiskers
			for whisker in bp['whiskers']:
				whisker.set(color='#7570b3', linewidth=2)

			## change color and linewidth of the caps
			for cap in bp['caps']:
				cap.set(color='#7570b3', linewidth=2)

			## change color and linewidth of the medians
			for median in bp['medians']:
				median.set(color='#b2df8a', linewidth=2)

			## change the style of fliers and their fill
			for flier in bp['fliers']:
				flier.set(marker='o', color='#e7298a', alpha=0.5)
			
		#	plt.show()

			fig.savefig(outfp+'/boxplot_{para:s}_{metric:s}.png'.format(para = xlabel,metric = m))
			plt.close()

#only plot preferred dimension rate vs variance threshold rate
def generate_histogram():
	#variance threshold					
	x = table[:,2]
	#preferred dimension rate
	y = table[:,-2]
	values = np.unique(x)
	x1 = values
	y1 = []
	for value in values:
		position = np.where(x==value)
		y1.append(sum(y[position])/float(len(y[position])))

	fig = plt.figure(figsize = figure_size)
	ax = fig.add_subplot(111)
	b = ax.bar(range(len(y1)),y1,1/2,color = 'blue')
	ax.set_xticklabels(x1)
	plt.ylim(0,1)
	plt.xlabel('variance_threshold_proportion')
	plt.ylabel('preferred_dimension_rate')
	plt.show()
	fig.savefig(outfp + '/histogram_variance_threshodl_proportion_preferred_dimension_rate.png')


def generate_barplot():
	y = [0.941,0.473]
	fig = plt.figure(figsize = figure_size)
	ax = fig.add_subplot(111)
	b = ax.bar([0.2,0.6],y,1/12,color = 'red')
	ax.set_xticks([0.2,0.6])
	ax.set_xticklabels(['previous','current'])
	b[1].set_color('black')
	plt.xlim(0,1)
	plt.ylim(0,1)
	plt.ylabel('proportion of preferred dimension')
	plt.show()
	fig.savefig('rate.png')
	
generate_barplot()
#one_dimension()
#generate_histogram()
#generate_boxplot()
#generate_heatmap()

