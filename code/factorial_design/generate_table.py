import csv
import numpy as np
import os
import pandas
import sys
import xml.etree.ElementTree as ET
import re
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import math
save_fp = sys.argv[1] 

expected_clusters_fp ='expected_clusters/noramlized_expected_clusters_full_dataset.csv'
#expected_clusters_fp = 'simplified_clusters_normalized.csv'
results_fp = 'results_full_dataset'
config_fp = 'samples_final_configuration_files'
#number of possible values of a paramter
divisions = 5
#dimesnion of this dataset
dimensions = 14
#return paramters in results

#times when clusters generated and the data of clusters need to be detected
times_value = [0,144,168]
#list of array,each one represents dimensions of clustering in one day 
expected_dimensions = []

def config_format(index,params):
	for column in params['xpath']:
		index.append(column.replace('./HDDStream/p_',''))	
	return index
	

#return total number of clusters in clusterring		
def get_clusternumber(fp):
	fileObject = csv.reader(open(fp))
	row_count = sum(1 for row in fileObject)
	return row_count-2

def get_clustervar(fp):
	fileObject = csv.reader(open(fp))
	cluster = []
	content = []
	#skip the first row,which is considered as index
	next(fileObject)
	
	for row in fileObject:
		content.append(int(row[0]))

	days = max(content) + 1
	
	for i in range(days):
		cluster.append(content.count(i))
	return np.var(cluster)
	

def transform(column):
	result = []
	for x in column:
		x = x.strip('[').strip(']')
		x = re.split(',',x)
		result.append([int(s) for s in x if s.isdigit()])
	return result
	
def get_intersted_populations(fp):
	data = np.array(pandas.read_csv(fp,sep=','))
	#possible value of time when clusters generated
	cluster_dimensions = data[:,-dimensions:]
	times = data[:,0]
#	for t in np.unique(times):
#		times_value.append(t)
	for t in times_value:
		d = cluster_dimensions[np.where(times==t)]
		expected_dimensions.append(d)
		print('expected clusters ',d)

def difference_between_expected_dimensions(fp):
	results = []
	sum_of_difference = 0 
	data  = np.array(pandas.read_csv(fp,sep=','))[1:]
	result_times = data[:,0]
	results_dimensions = data[:,-dimensions-2:-2]
	for i in range(len(times_value)):
		difference = 0
		result_dimension = results_dimensions[np.where(result_times==times_value[i])]
		expected_dimension = expected_dimensions[i]
		
		#find the closet distance between centroid of each predicted cluster and their closet expected cluster
		cluster_number_this_day = 0
		for line in result_dimension:
			cluster_number_this_day += 1
			difference += get_minmum_distance(line,expected_dimensions)
		
		#get average of distance	
		difference = difference/cluster_number_this_day
		results.append(difference)
		sum_of_difference += difference
		
	predicted_number = get_clusternumber(fp) 
	expected_number = get_clusternumber(expected_clusters_fp)
	f =  1 + 0.25 * abs(predicted_number - expected_number)/expected_number

	return sum_of_difference  * f
		
#def get_difference(predicted_dimension,expected_dimensions):
#	results = []
#	for t in times_value:
#		
#		result += get_minmum_distance(d,compared_dimension)
#	return result
		
def get_minmum_distance(point,other_points):
	factor = dimensions
	#normalize distance by number of dimensions
	return min([ get_distance(point,op)/factor for op in other_points])
		
def get_distance(p1,p2):
	return np.linalg.norm(np.asarray(p1)-np.asarray(p2))
	
#calculate split and merge time 
def calculate_split_rate(fp):
	#skip time -1
	split = 0
	
	data = np.array(pandas.read_csv(fp, sep=','))[1:]
	
	#array represents time of data
	time_points = data[:,0]
	#array represent unique value of data
	time_points_value = np.unique(time_points)
	p_id = np.array(transform((data[:,3])))
	l = len(time_points_value)
	for i in range(l-1):
		d1 = p_id[np.where(time_points==time_points_value[i])]
		d2 = p_id[np.where(time_points==time_points_value[i+1])]
		for x in d1:
			split_buffer = split
			for y in d2:
			#	print('check contain, ',x,y,check_contain(x,y))
				split = split+1 if check_contain(x,y) and not check_subset(y,x) else split
			#-1 as two split will happen when x split to y1 y2 in this loop 
			split = split - 1 if split != split_buffer and split != split_buffer+1 else split

	return split/get_clusternumber(fp)

def calculate_merge_rate(fp):
	#skip time -1
	merge = 0
	
	data = np.array(pandas.read_csv(fp, sep=','))[1:]
	
	#array represents time of data
	time_points = data[:,0]
	#array represent unique value of data
	time_points_value = np.unique(time_points)
	p_id = np.array(transform((data[:,3])))
	l = len(time_points_value)
	for i in range(l-1):
		d1 = p_id[np.where(time_points==time_points_value[i])]
		d2 = p_id[np.where(time_points==time_points_value[i+1])]
			
		for y in d2:
			merge_buffer = merge
			for x in d1:
				merge = merge+1 if check_contain(y,x) and not check_subset(x,y) else merge
			
			merge = merge -1 if merge != merge_buffer and merge != merge_buffer+1 else merge

	return merge/get_clusternumber(fp)


#check if array 2 is subset of array1(assume array1!=array2)
def check_subset(array1,array2):
	#if x and y not equal,check if they are parent and chlid
	if np.array_equal(array1,array2):
		return False
	result = True
	for a in array2:
		if not a in array1:
			result = False
			return result
	return result		

#check if any element of array1 is also in array2
def check_contain(array1,array2):
	if np.array_equal(array1,array2):
		return False
	for a in array2:
		if a in array1:
			return True
	return False


def get_preferred_dimension_rate(fp):
	data = pandas.read_csv(fp, sep=',')
	not_preferred = np.array(data.isnull().sum())
	rate = 1 - not_preferred.sum()/(dimensions*get_clusternumber(fp))
	return rate
	
class generator():
	def __init__(self):
		#self.params = pandas.read_csv('src/hddstream_param_spec2.csv', sep=',')
		self.params = pandas.read_csv('src/hddstream_param_spec.csv', sep=',')
		#list of header of each column
		self.index = []
		#list of function used to analyse results with preferred dimensions
		self.f_list = []
		#list of function to alalyse results with normalized data
		self.f_list_2 = []
		self.index = config_format(self.index,self.params)
		
		self.index.append('difference in day 0,6,7')
		self.f_list_2.append(difference_between_expected_dimensions)
		
		self.index.append('cluster_number')
		self.f_list.append(get_clusternumber)
		self.index.append('clsuter_variance')
		self.f_list.append(get_clustervar)
		self.index.append('split_rate')
		self.f_list.append(calculate_split_rate)
		self.index.append('merge_rate')
		self.f_list.append(calculate_merge_rate)
		self.f_list.append(get_preferred_dimension_rate)
		self.index.append('preferred_dimension_rate')
		
		
		self.index.append('result index')

			
	def write(self):

		if os.path.isfile(expected_clusters_fp):
			print('extract intersted population')
			get_intersted_populations(expected_clusters_fp)		
		
		#compute file path
		savefile = save_fp
		with open(savefile,'w') as csvfile:
			writer = csv.writer(csvfile,dialect='excel')
			writer.writerow(self.index)
			i = 0
			for i in range(625):
				print('result ',i)
				newline = []
				config = config_fp + '/parameters_samp{i:d}.xml'.format(i=i)
				tree = ET.parse(config) 
				root = tree.getroot()
				#get parameter value from config file
				for para in self.params['xpath']:
					newline.append(root.find(para).text)	
				
				result = results_fp + '/hddstreamFebruary,25,2017_config_parameters_samp{i:d}__normalised.csv'.format(i=i)	
				if os.path.isfile(result):
					for f in self.f_list_2:
						newline.append(f(result))					
				
					result = results_fp + '/hddstreamFebruary,25,2017_config_parameters_samp{i:d}__with_preferred_dimensions.csv'.format(i=i)
					#if result.is_file():
					#get metrics from different functions
					for f in self.f_list:
						newline.append(f(result))
					newline.append(i)
					writer.writerow(newline)
				else:
					print('missing file with index:',i) 

if __name__ == "__main__":
   generator().write()
print('complte generating table')
#calculate_pdimension_rate()
#fp = results_fp +  'hddstreamFebruary,16,2017_config_parameters_samp0_d_with_preferred_dimensions.csv'
#print(calculate_split_rate(fp),calculate_merge_rate(fp))


