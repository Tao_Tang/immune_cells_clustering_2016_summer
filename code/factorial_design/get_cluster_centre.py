import pandas
import os
import numpy as np
import csv


data_fp = 'simplified_cluster_data.csv'
savefile = 'simplified_clusters.csv'
data = np.array(pandas.read_csv(data_fp,sep=','))[:,1:]

#print(data[0:30,9])
times = (np.unique(data[:,0]))
cluster_name = data[:,9]
cluster_name = list(set(cluster_name))
print(cluster_name)
for index,c in enumerate(cluster_name):
	if c == '    ':
		del cluster_name[index]

print('cluster_name',cluster_name)


with open(savefile,'w') as csvfile:
	writer = csv.writer(csvfile,dialect='excel')
	writer.writerow(['time','cluster_name','size','SCA-1','CD11b','Ly6C'])
	
	for time in times:
		size = np.zeros(len(cluster_name))
		#each element represents centroid of a cluster
		dimensions = [np.zeros(3) for c in cluster_name]
	
		for line in data:
			#time equals current time and cluster_name is defined
			if line[0] == time and line[9] in cluster_name:
				i = cluster_name.index(line[9])
				size[i] += 1
				dimensions[i] = dimensions[i] + line[1:4]
		
		for i in range(len(dimensions)):
			dimensions[i] = dimensions[i]/size[i]
			writer.writerow([time*7,cluster_name[i],size[i],dimensions[i][0],dimensions[i][1],dimensions[i][2]])
		 #time is incremental in source file,so remove previos data to speed up
		data = data[sum(size):,:]
