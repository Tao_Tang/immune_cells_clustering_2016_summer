import numpy as np
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

a = np.array([[1,2,3],[4,6,7]])
b = np.array([4,5,6])

def one_dimension():
	fig = plt.figure(1,figsize = (9,6))
	ax = fig.add_subplot(111)
	bp = ax.boxplot(a)
	ax.set_xlabel('parameter a')
	plt.show()
	
one_dimension()
	
