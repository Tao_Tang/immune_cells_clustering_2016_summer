import csv
import pandas as pd
import os
import numpy as np

RUNFILE = '../WNV.csv'

# load all files in the RUNFILE
runfile = pd.read_csv(RUNFILE,sep = ',')
#list of data files and corresponding time
files = []
for i in range(runfile.shape[0]):
	files.append(runfile['file name'][i])
         
data = []
df = 0
for line in files:
#line = line[:-1]  # skip end character
	print('read file:',line)
	df = pd.read_csv(line)
	data.append(df.values[:,:-2])

# get max and mins of each column of each dataset(interval)
maxes = [np.nanmax(i, axis=0) for i in data]
mins = [np.nanmin(i, axis=0) for i in data]

# get max-mins and mins from total dataset
# Note max-mins makes it easier to scale to range [0,1]
mins = np.array(mins).min(axis=0)
maxes = np.array(maxes).max(axis=0)
df_columns = df.columns.values  # dimensions
num_dim = len(df_columns)

begin_column = 3

result_fp = 'expected_clusters/expected_clusters_full_dataset.csv'
index = np.array(pd.read_csv(result_fp,header = None,sep = ','))[0,:]
result = pd.read_csv(result_fp).values 

print(result[1,begin_column:])

result[:,begin_column:] = (result[:,begin_column:] - mins)/(maxes - mins)
#result = np.vstack([index,result])


save_fp = 'expected_clusters/noramlized_expected_clusters_full_dataset.csv'

df = pd.DataFrame(result, columns=index)
df.to_csv(save_fp, index=False)


