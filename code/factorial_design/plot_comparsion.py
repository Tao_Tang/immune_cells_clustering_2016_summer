import os
import sys
import csv
import numpy as np
import matplotlib.pyplot as plt
import pandas


factor = 20
standard_size = 20000
d1 = int(sys.argv[1])
d2 = int(sys.argv[2])

expected_clusters_fp = 'expected_clusters/noramlized_expected_clusters_full_dataset.csv'
#expected_clusters_fp ='hddstream_WNV_all_notcleaned_with_history_TAannotation_normalized.csv'
#expected_clusters_fp = 'expected_clusters/simplified_clusters_normalized.csv'

result_id = 171
result_fp = 'results_full_dataset/hddstreamFebruary,25,2017_config_parameters_samp{i:d}__normalised.csv'.format(i = result_id)
#result_fp = 'results_simplified_dataset/hddstreamFebruary,21,2017_config_parameters_samp{i:d}_d_normalised.csv'.format(i = result_id)
#result_fp = 'hddstream_WNV1_simplified_normalized_previous.csv'
save = 'result{i:d}'.format(i=result_id)
#save = 'previous'
save_fp = 'comparsions_diagram_full_dataset/' + save
#save_fp = 'comparsions_diagram/' + save

if not os.path.isdir(save_fp):
	os.mkdir(save_fp)

column_name = []
with open(expected_clusters_fp,'r') as f:
	reader = csv.reader(f)
	column_name = next(reader)

d1_name = column_name[-d1]
d2_name = column_name[-d2]


expected_data = np.array(pandas.read_csv(expected_clusters_fp,','))
expected_time = expected_data[:,0]
time_values = np.unique(expected_time)

result_data = np.array(pandas.read_csv(result_fp,','))
result_time = result_data[:,0]

print('a',result_time)
print('b',expected_time)
print('c',time_values)

#cluster_name = ['Immature_myeloid_cells', 'T_B_and_NK_cells',' DCs','Monocytes','Neutrophils']

for t in time_values:
	print('t is',t)
	fig = plt.figure()
	plt.xlim(0, 0.8)
	plt.ylim(0, 0.8)
	ax = fig.add_subplot(1,1,1)
	
	result_d1 = (result_data[np.where(result_time == t)])[:,-d1-2]
	result_d2 = (result_data[np.where(result_time == t)])[:,-d2-2]
	result_size =  [ int(factor*n/standard_size) for n in result_data[np.where(result_time == t)][:,2] ]
	
	expected_d1 = (expected_data[np.where(expected_time == t)])[:,-d1]
	expected_d2 = (expected_data[np.where(expected_time == t)])[:,-d2]
	expected_size =  [ int(factor*n/standard_size) for n in expected_data[np.where(expected_time == t)][:,2] ]
#	cluster_name = (expected_data[np.where(expected_time == t)])[:,1]
	#mark expected clusters
#	for i in range(len(cluster_name)):
#		ax.annotate(cluster_name[i], [expected_d1[i],expected_d2[i]])

	
	red_point = ax.scatter(expected_d1,expected_d2,color = 'r',s = expected_size)
	green_point = ax.scatter(result_d1,result_d2,color='g',s = result_size)
	
	fig.suptitle('time in hours: ' + str(t) , fontsize =18)
	ax.set_xlabel(d1_name,fontsize = 16)
	ax.set_ylabel(d2_name,fontsize = 16)
	
	plt.legend((red_point,green_point),('expected clusters','predicted clusters'),scatterpoints=1)
	plt.show()
	
	fig.savefig(save_fp+'/'+ 'time' + str(t) + '_'+ d1_name+ '_' + d2_name+ '.png')
	
