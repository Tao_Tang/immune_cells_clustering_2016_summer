import csv
import pandas
import os
import numpy as np
import re

#transform list of string to list of list of intger
def transform(column):
	result = []
	for x in column:
		x = x.strip('[').strip(']')
		result.append([int(s) for s in x.split() if s.isdigit()])
	return result

def calculate_split_merge(fp):
	#skip time -1
	split = 0
	merge = 0
	
	split_buffer = split
	merge_buffer = merge
	
	data = np.array(pandas.read_csv(fp, sep=','))[1:]
	
	days = np.array(data[:,0])
	p_id = np.array(transform(data[:,2]))
	
	length = np.max(days)
	
	for i in range(length):
		d1 = p_id[np.where(days==i)]
		d2 = p_id[np.where(days==i+1)]
		for x in d1:
			split_buffer = split
			
			for y in d2:
				split = split+1 if check_contain(x,y) else split
		#		merge = merge+1 if check_contain(y,x) else merge
			#-1 as two split will happen when x split to y1 y2 in this loop 
			split = split-1 if split != split_buffer and split != split_buffer+1 else split
			
		for y in d2:
			merge_buffer = merge
			for x in d1:
				merge = merge+1 if check_contain(y,x) else merge
			
			merge = merge -1 if merge != merge_buffer and merge != merge_buffer+1 else merge

	return [split,merge]

#check if array 2 is subset of array1(assume array1!=array2)
def check_contain(array1,array2):
	#if x and y not equal,check if they are parent and chlid
	if np.array_equal(array1,array2):
		return False
	result = True
	for a in array2:
		if not a in array1:
			result = False
			return result
	return result		
	

print(calculate_split_merge('hddstream_fp_December,16,2016_parameters_samp306_WNV1_simplified_.csv'))


a = np.array([1,2,3])
b = np.array([1,2,3])
#print(np.array_equal(a,b))
