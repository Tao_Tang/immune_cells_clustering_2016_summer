# Generates synthetic data as per the thesis.
# Author: Deeksha Singh

import os
import sys

import numpy as np

outdir = 'temp'
if '-o' in sys.argv:
    outdir = sys.argv[sys.argv.index('-o') + 1]

os.makedirs(outdir, exist_ok=True)

outfile = outdir + '/prefer_x_y.csv'

print('starting dummy data generation')
mu = 3
mu2 = 1.4
sigma = 2
size = 10000
xa = np.random.normal(5, sigma, size)
xb = np.random.normal(-5, sigma, size)
x = np.hstack((xa, xb))
y = np.random.normal(mu2, 0.5, 2 * size)
z = np.random.uniform(0, 5, size * 2)

columns = ['x', 'y', 'z']
data = np.column_stack((x, y, z))
