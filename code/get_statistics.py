
# Creates plots and spreadsheet to analyse large quantities of data
# Author: Deeksha Singh

import numpy as np
import pandas as pd
import sys
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
import matplotlib.mlab as mlab

import os

RUNFILE = sys.argv[1]

output_dir = 'data_stats/'
os.makedirs(output_dir, exist_ok=True)


class DataStatistics:
    def __init__(self):
        # set save file for statistics calculated
        name = RUNFILE[:-4].split('/')
        self.run_file_name = name[len(name) - 1]
        os.makedirs(output_dir + self.run_file_name, exist_ok=True)

        self.boxplot_save_location = output_dir + self.run_file_name + '/boxplots/'
        os.makedirs(self.boxplot_save_location, exist_ok=True)

        self.stats_save_file = output_dir + self.run_file_name + '/statistics.csv'
        self.normal_save_location = output_dir + self.run_file_name + '/normal_graphs/'
        self.bar_save_location = output_dir + self.run_file_name + '/bar_graphs/'
        self.scatter_save_location = output_dir + self.run_file_name + '/scatter_graphs/'

        self.graphtypes = sys.argv[2] if len(sys.argv) > 2 else ''

    def run(self):
        if not os.path.isfile(RUNFILE):
            print('missing file: ', RUNFILE)
            return

        print("starting file: ", RUNFILE)
        df = pd.read_csv(RUNFILE)
        self.data = df
        self.generate_statistics_spreadsheet()

        if 'b' in self.graphtypes:
            self.generate_graph('bar', self.bar_save_location)
            os.makedirs(self.bar_save_location, exist_ok=True)
        if 's' in self.graphtypes:
            os.makedirs(self.scatter_save_location, exist_ok=True)
            self.generate_graph('scatter', self.scatter_save_location)
        if 'n' in self.graphtypes:
            os.makedirs(self.normal_save_location, exist_ok=True)
            self.generate_probability_density_graph()

    # Creates a spreadsheet of min, median, max, mean and standard deviation for each dimension (column in input csv)
    def generate_statistics_spreadsheet(self):
        df = self.data
        dimension_index = 0
        output = []

        for col in df.columns:
            newrow = []
            values = df[col]
            min = np.min(values)
            median = np.median(values)
            max = np.max(values)
            mean = np.mean(values)
            std = np.std(values)
            below3std = mean - std * 3
            above3std = mean + std * 3
            above4std = mean + std * 4

            newrow.append(col)
            newrow.append(min)
            newrow.append(median)
            newrow.append(max)
            newrow.append(mean)
            newrow.append(std)
            newrow.append(below3std)

            count = sum(np.array(values) < below3std)
            newrow.append((count / len(values)) * 100)

            newrow.append(above3std)

            count = sum(np.array(values) > above3std)
            newrow.append((count / len(values)) * 100)

            newrow.append(above4std)

            count = sum(np.array(values) > above4std)
            newrow.append((count / len(values)) * 100)

            output.append(newrow)
            dimension_index += 1

            print('generating boxplot for ' + col)
            self.generate_boxplot(col, values, mean, std, min, median, max)

        print('creating statistics spreadsheet with dimensions ' + str(dimension_index))
        df_columns = ['dimension', 'min', 'median', 'max', 'mean', 'std', 'mean - 3std', '% below mean - 3std',
                      'mean + 3std', '% above mean + 3std', 'mean + 4std', '% above mean + 4std']
        dfn = pd.DataFrame(output, columns=df_columns)
        dfn.to_csv(self.stats_save_file, index=False)

    # Creates a graph given the type for each dimension (column in input csv)
    # input: type is "bar" or "scatter" indicating type of graph to create
    def generate_graph(self, type, save_location):
        if type is None:
            return

        df = self.data
        for col in df.columns:
            data_to_plot = df[col]
            mean = np.mean(data_to_plot)
            std = np.std(data_to_plot) * 3
            f, ax = plt.subplots()

            if type == 'bar':
                print('making bar graph for dimension: ' + col)
                ax.bar(np.arange(0, len(data_to_plot)), data_to_plot)
            elif type == 'scatter':
                print('making scatter graph for dimension: ' + col)
                ax.scatter(np.arange(0, len(data_to_plot)), data_to_plot)
            else:
                return

            ax.plot((0, len(data_to_plot)), (mean + std, mean + std), c='r')

            ax.xaxis.set_major_formatter(NullFormatter())
            ax.set_xlabel(col)
            ax.axis('tight')
            ax.set_title(self.run_file_name)
            ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey',
                          alpha=0.5)
            mng = plt.get_current_fig_manager()
            mng.resize(*mng.window.maxsize())
            f.set_size_inches(18, 10)

            plt.savefig(save_location + col + '.png', format='png', dpi=200)
            print('...............')
            # plt.show();
            plt.close(f)

    # Creates probability density distribution figures for each dimension (column in input csv)
    # Note: Graph window is expanded to show the range of 5 standard deviations from mean in either direction
    def generate_probability_density_graph(self):
        df = self.data

        for col in df.columns:
            data_to_plot = df[col]
            std = np.std(data_to_plot)
            five_std = 5 * std
            mean = np.mean(data_to_plot)
            f, ax = plt.subplots()

            x_axis = np.arange(mean - five_std, mean + five_std, 1)
            ax.xaxis.set_ticks(np.arange(mean - five_std, mean + five_std, std))
            ax.set_xlabel(col + '\nstandard deviation: ' + str(std) + ', mean: ' + str(mean), size='x-small')

            print('making normal graph for dimension: ' + col)
            ax.plot(x_axis, mlab.normpdf(x_axis, mean, std))

            save_location = self.normal_save_location + col + '.png'
            plt.savefig(save_location, format='png', dpi=200)
            plt.close(f)

    # Creates modified boxplots with given data
    # Note: The whisker ends are upper (Q1 - 1.5*IQR) and lower fences (Q3 + 1.5*IQR)
    def generate_boxplot(self, col, data_to_plot, mean, std, min, median, max):
        f, ax = plt.subplots()

        ax.set_axisbelow(True)
        ax.set_title(self.run_file_name + ' ' + col)
        ax.set_xticklabels([col])
        ax.set_xlabel('standard deviation: ' + str(std) + ', mean: ' + str(mean) + ', min: ' + str(
            min) + ', median: ' + str(median) + ', max: ' + str(max), size='x-small')

        ax.boxplot(data_to_plot)

        ax.plot(1, mean, 'rs')
        ax.plot([1, 1], [mean - 3 * std, mean + 3 * std], 'ro')
        ax.annotate('mean', (1.01, mean), size='x-small')
        ax.annotate('mean - 3std', (1.01, mean - 3 * std), size='x-small')
        ax.annotate('mean + 3std', (1.01, mean + 3 * std), size='x-small')

        save_location = self.boxplot_save_location + col + '.png'
        plt.savefig(save_location, format='png', dpi=200)


if __name__ == "__main__":
    DataStatistics().run()
