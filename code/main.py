#run firstimp.py with different input parameters created by HDDStream_para_factorial_design.py
import xml.etree.ElementTree as ET
import numpy as np
import time
import pandas
import os
import sys
from HDDStream import HDDStream

outDir = 'factorial_design' # set up dir for experimental data. 
design_fp = 'factorial_design/src/hddstream_param_spec.csv'

# user-specified. Which parameters, what ranges. 
params = pandas.read_csv(design_fp, sep=',')


data = 'WNV.csv'
#data = 'WNV1_simplified.csv' 
sampleDir = 'factorial_design/samples'

d = 5
p = params.shape[0]

values = []

config_fp = ''
if '-inputfp' in sys.argv:
	config_fp = sys.argv[sys.argv.index('-inputfp')+1]

tree = ET.parse(config_fp) 
root = tree.getroot()
for j in range(p):
	values.append(float(root.find(params['xpath'][j]).text))

os.system('python3 firstimp.py hddstream {data:s}  -parameterfp {config_fp:s} -decay {decay:f} \
          -density_threshold_proportion {mu:f}  -variance_threshold_proportion {delta:f} -radius_threshold \
           {eps:f}'.format(data=data,config_fp=config_fp,decay = values[0],mu = values[1],  delta = values[2],eps = values[3]))
