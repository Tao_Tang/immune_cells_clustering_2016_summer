#run firstimp.py with different input parameters created by HDDStream_para_factorial_design.py
import xml.etree.ElementTree as ET
import numpy as np
import time
import pandas
import os
import sys
from HDDStream import hddstream

outDir = 'factorial_design' # set up dir for experimental data. 
design_fp = 'factorial_design/src/hddstream_param_spec2.csv'

# user-specified. Which parameters, what ranges. 
params = pandas.read_csv(design_fp, sep=',')


#data = 'WNV.txt'
data = 'WNV1_simplified.csv' 
sampleDir = 'factorial_design/samples'

d = 5
p = params.shape[0]

values = []

sample_fp = ''
if '-inputfp' in sys.argv:
	sample_fp = sys.argv[sys.argv.index('-inputfp')+1]

tree = ET.parse(sample_fp) 
root = tree.getroot()
for j in range(p):
	values.append(float(root.find(params['xpath'][j]).text))
	

os.system('python3 firstimp.py hddstream {data:s}  -parameterfp {samplefp:s} -delta {delta:f} -mu {mu:f} -minPoints {minPoints:f} -eps {eps:f}'.format(data=data,samplefp=sample_fp,delta = values[0],mu = values[1], minPoints = values[2],eps = values[3]))
