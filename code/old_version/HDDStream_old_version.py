import time
import sys
import numpy as np

CF1_POS = 0
CF2_POS = 1
WEIGHT_POS = 2
CURRENT_WEIGHT_POS = 3


# This class applies the HDDStream algorithm to incrementally perform clustering. See the following papers for more
# details:
# [1] Ntoutsi, Irene, et al. "Density-based Projected Clustering over High Dimensional Data Streams." SDM. 2012.
# [2] Bohm, Christian, et al. "Density connected clustering with local subspace preferences." Data Mining, 2004.
# ICDM'04. Fourth IEEE International Conference on. IEEE, 2004.
# Author: Deeksha singh
class hddstream(object):
    def __init__(self, mu_percentage=5, eps=0.2, delta=1, k=100, beta=0.5, decay=1, minPoints_percentage=5, section=0.1,
                 store_cluster_data=False,interval = 1):
        self.is_point_added_to_pcore = []
        self.init = False
        # max dimensionality of a subspace
        self.p_lambda = 0.0
        # mu is density threshold for pcore and core microclusters, mu_boundary defines percentage of data to set mu to
        self.p_mu_boundary = mu_percentage
        # eps is the radius threshold
        self.radius_threshold = eps
        # delta is variance threshold, set later
        self.p_delta = delta
        #If value of variance_threshold given in _init_ then set TRUE and use given value, otherwise set FALSE, calculate variance threshold each day
        self.variance_threshold_fixed = delta != 1
		# k is used to adjust influence of preferred dimensions
        self.p_k = k
        # decay is factor to reduce influence of old data with
        self.p_decay = decay
        # beta is the level of relaxation allowed to potential microclusters, it is within (0,1)
        self.p_beta = beta
        # minPoiint_boundary is the same as mu_boundary but for the initialisation stage using PreDeCon
        self.p_minPoint_boundary = minPoints_percentage
        # section is the part of data to use for initialisation using PreDeCon
        self.p_section = section
        #length of interval during two time point(default is one day)
        self.interval = interval
        # collection of *pontential* core microclusters, each cluster has the following values
        # 0: CF1 [], 1:CF2 [], 2: Weight d
        self.pcore_clusters = []
        # the preferred dimensions vector for each potential mucrocluster microclusters
        self.pcore_pref_dim = []
        
        # collection of outlier microclusters, each cluster has the following values
        # 0: CF1 [], 1:CF2 [], 2: Cumulative size, size
        
        self.outlier_clusters = []
        # the preferred dimensions vector for each outlier microclusters
        self.outlier_pref_dim = []
        #collection of all cored clusters, each consisted of one or more pontential cored clsuters
        self.core_clusters = []
        self.core_pref_dim = []
        self.cluster_ids = []
        self.day = - interval
        self.num_dimen = 0
        self.neighbourhoods = []
        self.weighted_neighbourhoods = []
        self.weighted_ids = []
        self.cores = []
        self.current_ids = []
        self.cluster_data = []
        self.pcore_data = []
        self.o_data = []
        self.store_cluster_data = store_cluster_data
        
        #time of last and current data
        self.previous_time = 0
        self.current_time = 0

    def is_started(self):
        return self.init

    def get_ids(self):
        return np.array([i for i in range(len(self.core_clusters))])

    def get_cluster_data(self):
        return np.array(self.cluster_data)

    def get_pcore_ids(self):
        return self.cluster_ids

    def get_cumulative_size(self):
        return np.array([pmc[WEIGHT_POS] for pmc in self.core_clusters])

    def get_size(self):
        return np.array([pmc[CURRENT_WEIGHT_POS] for pmc in self.core_clusters])

    def get_clusters(self):
        return np.array([self.get_center(pmc) for pmc in self.core_clusters])

    def get_pref_dimen(self):
        return self.core_pref_dim

    def get_num_dimensions(self):
        return self.num_dimen

    def get_percentage_boundary(self, base, percentage):
        return (base / 100) * percentage

    def set_variance_threshold(self, data):
        # Sets variance threshold delta. Set anew for each dataset, which is a distinct cytometry run.
        # However, these data are all normalised in one operation. 
        maxes = np.nanmax(data, axis=0)
        average = np.nanmean(maxes)
        if not self.variance_threshold_fixed:
            self.p_delta = average / 5
            print('today variance threshold is:',self.p_delta)

    def print_params(self):
        p_str = 'running HDDStream with params mu=' + str(self.p_mu) + ' lambda(pi)=' + str(
            self.p_lambda) + ' eps=' + str(self.radius_threshold) + ' delta=' + str(self.p_delta) + ' decay=' + str(self.p_decay) \
                + ' beta=' + str(self.p_beta) + ' mu percentage ' + str(self.p_mu_boundary) + ' minPoints percentage ' \
                + str(self.p_minPoint_boundary) + ' init percentage ' + str(self.p_section) + 'variance threshold' + str(self.p_delta) 
        print(p_str)

		#X:whole data set
    def start(self, X, start_time):
        print('starting HDDStream')
        # Compute and set some params
        self.p_lambda = X.shape[1]
        self.num_dimen = X.shape[1]
        section_end = self.get_percentage_boundary(X.shape[0], self.p_section)
        self.p_mu = self.get_percentage_boundary(X.shape[0], self.p_mu_boundary)
        self.set_variance_threshold(X[0:section_end])
        self.print_params()
        #set time
        self.current_time = start_time
        # Initialise with section of data
        self.initialise(X[0:section_end])
        # Send rest of data from first day to online clustering
        self.newdata(X[section_end:],start_time)

    def newdata(self,X,new_time):
		#update time
        self.previous_time = self.current_time
        self.current_time = new_time
		
        # Load in and cluster a new datafile, representing a different point in time.
        self.set_variance_threshold(X)
        self.print_params()
        self.day += self.interval   # TODO this needs to be loaded from user, not hard coded. Future applications may not use evenly spaced timepoints. 
        self.p_lambda = X.shape[1]
        #p_mu = size * p_mu_boundary * 0.01 is the density threshold,compared with size of a cluster
        self.p_mu = self.get_percentage_boundary(X.shape[0], self.p_mu_boundary)
        self.core_clusters = []
        
        if not self.day == 0:
            print('perform downgrade')
            self.perform_downgrade()
               
        #store clusters points for evaluation
        #clear previous current_weight,to include points added in initilasation the first day, not clear if in first day
        if not self.day ==  0:
            for pmc in self.pcore_clusters:
                pmc[CURRENT_WEIGHT_POS] = 0.0
            for mc in self.core_clusters:
                mc[CURRENT_WEIGHT_POS] = 0.0
            for oc in self.outlier_clusters:
                oc[CURRENT_WEIGHT_POS] = 0.0
			        
        
        self.cluster_ids = []
        # Stores cluster points for evaluation
        if self.store_cluster_data: self.cluster_data = []
        self.is_point_added_to_pcore = (np.full((len(self.pcore_clusters), 1), False, dtype=bool)).tolist()
        start_time = time.time() #for logging execution time 
        for i in range(X.shape[0]):  #scan through events in dataset
            x = X[i]
            # Try to add x to a potential microcluster
            trial1 = self.add_to_pcore(x)
            trial2 = False
            if not trial1:
                # Try to add x to an outlier microcluster
                trial2 = self.add_to_outliers(x)
            if not trial1 and not trial2:
                # Create new outlier microcluster for x
                self.create_new_outlier_cluster(x)
                if self.store_cluster_data: self.o_data.append([x])
        print("clustering took %s seconds" % (time.time() - start_time))
        print('--------------------------')
        # Decay all p-core mc that have not been updated in the last day by factor p_decay
#        print('performing downgrade')
#        self.perform_downgrade() #Todo: reanme this # not sure,but downgrade was used in Ntoutsi's essay
        # Perform offline clustering on p-core mc using PreDeCon to extract final core mc
        print('performing offline clustering with PreDeCon with ' + str(len(self.pcore_clusters)) + ' pcores')
        self.perform_offline_clustering()

    def add_to_pcore(self, p):
        distances = []
        # Calculate distances to all pcore mc from p
        for index, pmc in enumerate(self.pcore_clusters):
            # Update pcore_pref_dimen of pmc temporarily
            pcore_pref_dimen = self.update_pcore_pref_dimen(pmc, p)
            if (pcore_pref_dimen != 1).sum() <= self.p_lambda:
                distances.append([self.get_projected_dist(self.pcore_pref_dim[index], pmc, p), index])

        if len(distances) > 0:
            # Get the closest pcore mc and compute its new properties with p included
            pmc_closest = self.get_closest_cluster(distances)
            new_cluster_dim = self.update_pcore_pref_dimen(self.pcore_clusters[pmc_closest], p)
            new_cluster = self.get_projected_pmc(self.pcore_clusters[pmc_closest], p)
            radius = self.get_radius(new_cluster, new_cluster_dim)
            # If radius condition for pcore mc is still fulfilled for pmc_closest add p to it
            if radius <= self.radius_threshold ** 2:
                self.add_to_cluster(self.pcore_clusters, pmc_closest, p)
                self.pcore_pref_dim[pmc_closest] = self.calculate_pref_dimen(self.pcore_clusters[pmc_closest])
                if self.store_cluster_data: self.pcore_data[pmc_closest].append(p)
                self.is_point_added_to_pcore[pmc_closest] = True
                return True
        return False

    def add_to_outliers(self, p):
        distances = []
        # Calculate distances to all o mc from p
        for index, pmc in enumerate(self.outlier_clusters):
            distances.append([self.get_projected_dist(self.outlier_pref_dim[index], pmc, p), index])

        if len(distances) > 0:
            # Get the closest o mc and compute its new properties with p included
            pmc_closest = self.get_closest_cluster(distances)
            new_cluster_dim = self.update_pcore_pref_dimen(self.outlier_clusters[pmc_closest], p)
            new_cluster = self.get_projected_pmc(self.outlier_clusters[pmc_closest], p)
            radius = self.get_radius(new_cluster, new_cluster_dim)
            if radius <= self.radius_threshold ** 2:
                current = self.outlier_clusters[pmc_closest]
                current = [current[CF1_POS] + p, current[CF2_POS] + p ** 2, current[WEIGHT_POS] + 1,current[CURRENT_WEIGHT_POS]+1]
                current_pref_dim = self.calculate_pref_dimen(np.array(current))
                # check if adding p to outlier cluster makes it a pcore cluster
                if self.check_if_pcore(current, current_pref_dim):
                    # Make the o mc into a pcore mc
                    self.pcore_clusters.append(current)
                    self.pcore_pref_dim.append(current_pref_dim)
                    if self.store_cluster_data: self.pcore_data.append(self.o_data[pmc_closest])
                    self.is_point_added_to_pcore.append(True)
                else:
                    # Add p into current o mc
                    self.outlier_clusters[pmc_closest] = current
                    self.outlier_pref_dim[pmc_closest] = current_pref_dim
                    if self.store_cluster_data: self.o_data[pmc_closest].append(p)
                return True
        return False

    def get_projected_pmc(self, pmc, p):
        return np.array([pmc[CF1_POS] + p, pmc[CF2_POS] + p ** 2, pmc[WEIGHT_POS] + 1,pmc[CURRENT_WEIGHT_POS]+1 ]) 

    # from HDDStream definition 6
    def check_if_pcore(self, cluster, pref_dim):
        radius = self.get_radius(cluster, pref_dim)
        weight = cluster[WEIGHT_POS]
        pdim = (np.array(pref_dim) > 1).sum()
        return radius <= self.radius_threshold ** 2 and weight >= self.p_beta * self.p_mu and pdim <= self.p_lambda

    # from HDDStream definition 5
    def check_if_core(self, cluster, pref_dim):
        radius = self.get_radius(cluster, pref_dim)
        weight = cluster[WEIGHT_POS]
        pdim = (np.array(pref_dim) > 1).sum()
        return radius <= self.radius_threshold ** 2 and weight >= self.p_mu and pdim <= self.p_lambda

    def get_closest_cluster(self, distances):
        dist = np.array(distances)
        ids = dist[:, 1]
        dists = dist[:, 0]
        return int(ids[np.nanargmin(dists)])

    # from HDDStream definition 5
    def get_radius(self, pmc, pref_dimen):
        CF1 = pmc[CF1_POS]
        CF2 = pmc[CF2_POS]
        weight = pmc[WEIGHT_POS]
        radius = 0.0
        for i in range(self.num_dimen):
            dimension = 1 / pref_dimen[i] #value 1 or 100
            value = (CF2[i] / weight) - (CF1[i] / weight) ** 2
            radius += dimension * value
        return radius
	
	#get weighted Euclidean distance form cluster and point, weighted vector is the pcore_pref_dimen
    def get_projected_dist(self, pcore_pref_dimen, pmc, p):
        center = [(x / pmc[WEIGHT_POS]) for x in pmc[CF1_POS]]
        dist = 0.0
        for i in range(self.num_dimen):
            dimension = 1 / pcore_pref_dimen[i]
            value = (p[i] - center[i]) ** 2
            dist += dimension * value
        return dist ** (1 / 2)

	#get the new pref_dimen after a point added to pontential cluster
    def update_pcore_pref_dimen(self, pmc, p):
        new_CF1 = pmc[CF1_POS] + p
        new_CF2 = pmc[CF2_POS] + p ** 2
        new_weight = pmc[WEIGHT_POS] + 1
        new_current_weight = pmc[CURRENT_WEIGHT_POS] + 1
        return self.calculate_pref_dimen(np.asarray([new_CF1, new_CF2, new_weight,new_current_weight]))

    # from HDDStream definition 2 and 3
    def calculate_pref_dimen(self, pmc):
        pref_dimen = np.ones(self.num_dimen)
        CF1 = pmc[CF1_POS]
        CF2 = pmc[CF2_POS]
        weight = pmc[WEIGHT_POS]
        for index in range(self.num_dimen):
            variance = (CF2[index] / weight) - (CF1[index] / weight) ** 2
            if variance <= self.p_delta ** 2:
                pref_dimen[index] = self.p_k
        return pref_dimen
#modify to perform downgrade everyday
    def perform_downgrade(self):
        for i in range(len(self.is_point_added_to_pcore)):
            #if not self.is_point_added_to_pcore[i]:
				#f = open('count_decay_used.txt','a')
                #f.write('perform_downgrade called\n')
                #f.close()
            cluster = self.pcore_clusters[i]
            #change of time during two data,assume time expressed in hour
            delta_t = (self.current_time - self.previous_time)/24
            multiplicity = 2 ** (-self.p_decay * delta_t)
            cluster[CF1_POS] *= multiplicity
            cluster[CF2_POS] *= multiplicity
            cluster[WEIGHT_POS] *= multiplicity
            
            
#check which pcore can be core or added to exisiting core
    def perform_offline_clustering(self):
        # Predecon on pcore mc
        start_time = time.time()
        #all pcore clusters if unclassified in the beginnnign
        classified = np.full((len(self.pcore_clusters), 1), 'f', dtype='str')
        all = [self.get_center(cluster) for cluster in self.pcore_clusters]
        self.update_preference_neighbourhood(all)
        self.calculate_preference_weighted_neighbourhood(np.array(all))
        
        for index, pmc in enumerate(self.pcore_clusters):
            pref_dimen = self.pcore_pref_dim[index]
            if self.check_if_core(pmc, pref_dimen):
                count = len(self.core_clusters)
                #create an empty core_cluster for current pmc, pmc will be added to this corecluster in expand as it includes in the neighbourhood by itself 
                self.create_new_core_cluster(self.core_clusters, self.core_pref_dim)
                if self.store_cluster_data: self.cluster_data.append([])
                print(self.get_ids())
                # ,number of coreclusters, core clusters list, core cluster preferred dimension , index of pcore cluster supposed to expand
                self.expand(classified, count, self.core_clusters, self.core_pref_dim, index)
                # If nothing has been added to this core mc delete it
                if self.core_clusters[count][WEIGHT_POS] < 1.0:
                    del self.core_clusters[count]
                    del self.core_pref_dim[count]
                    if self.store_cluster_data: del self.cluster_data[count]
                else:
                    self.cluster_ids.append(np.array(self.current_ids))
                    self.current_ids = []
            else:
                classified[index] = 'n'
        self.weighted_neighbourhoods = []
        self.weighted_ids = []
        self.neighbourhoods = []
        print("offline clustering %s seconds" % (time.time() - start_time))
        print('--------------------------')
	#called by start(X),here X is this first 10% of original data set 
    def initialise(self, X):  # TODO, rename this _initialise. 
        start_time = time.time()  # TODO create stopwatch class. 
        # Mark all data points as unclassified
        classified = np.full((X.shape[0], 1), 'f', dtype='str')
        self.p_minPoints = self.get_percentage_boundary(X.shape[0], self.p_minPoint_boundary)
        print('beginning initialisation with data size ' + str(X.shape[0]) + ' minPts ' + str(self.p_minPoints))
        self.update_preference_neighbourhood(X)
        self.calculate_preference_weighted_neighbourhood(X)

        for i in range(X.shape[0]):
            x = X[i]
            if self.is_core(i, x):
                count = len(self.pcore_clusters)
                # create a new potential cluster in pcore_clusters
                self.create_new_core_cluster(self.pcore_clusters, self.pcore_pref_dim)
                if self.store_cluster_data: self.pcore_data.append([])
                self.expand(classified, count, self.pcore_clusters, self.pcore_pref_dim, i)
                # If nothing has been added to this pcore mc delete it
                if self.pcore_clusters[count][WEIGHT_POS] < 1.0:
                    del self.pcore_clusters[count]
                    del self.pcore_pref_dim[count]
                    if self.store_cluster_data: del self.pcore_data[count]
            else:
                classified[i] = 'n'
            print('initialisation complete: ' + str((i / X.shape[0]) * 100) + ' %')
            if time.time() - start_time > 3600:
                print('warning: initialisation takes more than 1 hour')
                sys.exit()
            
        self.init = True
        self.cores = []
        self.weighted_neighbourhoods = []
        self.weighted_ids = []
        print("initialisation %s seconds" % (time.time() - start_time))
        print('--------------------------')
        #index: index of 'this pmc' in list pcore_clusters
    def expand(self, classified, count, cluster_group, cluster_pref_dimen, index):
        neighbourhood = np.copy(self.get_preference_weighted_neighbourhood(index))
        # ids of weighted neighbourhood of 'this pmc'
        ids = np.copy(self.weighted_ids[index])
        # Expand the neighbourhood of current point and so on to compute an area of high density
        while len(neighbourhood) > 0:
            point = neighbourhood[0]
            #buffer direct reach points and index for each point = neighbourhood
            R = []
            indices = []
            direct_reach_function = self.is_offline_direct_reach
            if not self.init:
                direct_reach_function = self.is_direct_reach

            for j, other in enumerate(neighbourhood):
               # print('index of pmc and two pmc two be compared',index,ids[0],ids[j])
                if direct_reach_function(point, ids[0], ids[j], other):
                    R.append(other)
                    indices.append(ids[j])
            R = np.array(R)
            for r_index in range(R.shape[0]):
                classified_index = indices[r_index]
                r = classified[classified_index]
                # if r is unclassified do, add the pmc corresponds to this r to negighbourhood of 'this pmc' as input 
                if r == 'f':
                    neighbourhood = np.vstack([neighbourhood, R[r_index]])
                    ids = np.append(ids, [indices[r_index]])
                # if r is unclassified or noise do:
                if r == 'f' or r == 'n':
                    classified[classified_index] = str(count)
                    if not self.init:
                        self.add_to_cluster(cluster_group, count, R[r_index])
                        cluster_pref_dimen[count] = self.calculate_pref_dimen(cluster_group[count])
                        if self.store_cluster_data: self.pcore_data[count].append(R[r_index])
                    else:
						#also merge pcore cluster to the core cluster contain 'this pmc'
                        self.merge_cores(cluster_group[count], count, self.pcore_clusters[ids[r_index]])
                        self.current_ids.append(ids[r_index])
                        if self.store_cluster_data: self.cluster_data[count].extend(self.pcore_data[ids[r_index]])
            neighbourhood = np.delete(neighbourhood, 0, axis=0)
            ids = np.delete(ids, 0, axis=0)

    def merge_cores(self, base, base_index, new):
        base[CF1_POS] += new[CF1_POS]
        base[CF2_POS] += new[CF2_POS]
        base[WEIGHT_POS] += new[WEIGHT_POS]
        base[CURRENT_WEIGHT_POS] += new[CURRENT_WEIGHT_POS]
        self.core_pref_dim[base_index] = self.calculate_pref_dimen(base)

    def get_center(self, pmc):
        return np.array([(x / pmc[WEIGHT_POS]) for x in pmc[CF1_POS]])

    def add_to_cluster(self, cluster_group, cluster_index, point):
        cluster = cluster_group[cluster_index]
        cluster[CF1_POS] += point
        cluster[CF2_POS] += point ** 2
        cluster[CURRENT_WEIGHT_POS] += 1
        cluster[WEIGHT_POS] += 1

    def create_new_core_cluster(self, cluster_group, pref_group):
        CF1 = np.zeros(self.num_dimen)
        CF2 = np.zeros(self.num_dimen)
        weight = 0.0
        current_weight = 0.0
        cluster_group.append([CF1, CF2, weight,current_weight])
        pref_group.append([] * self.num_dimen)

    def create_new_outlier_cluster(self, point):
        CF1 = point
        CF2 = np.array(point) ** 2
        weight = 1.0
        current_weight = 1.0
        self.outlier_clusters.append([CF1, CF2, weight,current_weight])
        self.outlier_pref_dim.append(self.calculate_pref_dimen([CF1, CF2, weight,current_weight]))

    def get_preference_neighbourhood(self, index):
        return self.neighbourhoods[index]

	#compute neighbourhood relationship betwwen cores of clusters
    def update_preference_neighbourhood(self, X):
        self.neighbourhoods = []
        for point in X:
            neighbourhood = np.array(point)
            for other_point in X:
                if self.get_euclidean_dist(other_point, point) <= self.radius_threshold and self.get_euclidean_dist(other_point, point) != 0 :
                    neighbourhood = np.vstack([neighbourhood, other_point])
#            neighbourhood = np.delete(neighbourhood, 0, axis=0)
            self.neighbourhoods.append(neighbourhood)

    # From PreDeCon Definition 2
    def get_preference_dimen(self, neighbourhood, point):
        count = 0
        for dimen in point:
            if self.get_variance(neighbourhood, point, dimen) <= self.p_delta:
                count += 1
        return count

    # From PreDeCon Definition 1
    def get_variance(self, neighbourhood, point, dimen):
        return np.sum(
            [self.get_euclidean_dist(other_point[dimen], point[dimen]) ** 2 for other_point in neighbourhood]) / len(
            neighbourhood)

    # From PreDecon Definition 3
    def get_dist(self, base_index, base, other):
        # print('base ' + str(base))
        weight = np.ones(self.num_dimen)
        pref_neighbour = self.get_preference_neighbourhood(base_index)
        sum = 0.0
        avg = 0.0
        for i in range(self.num_dimen):
            variance = self.get_variance(pref_neighbour, base, i)
            avg += variance
            if variance <= self.p_delta:
                weight[i] = self.p_k
            else:
                weight[i] = 1
            sum += ((base[i] - other[i]) ** 2) * (weight[i])
        return sum

    # From PreDeCon Definition 4
    def get_prefered_dist(self, p_index, p, q_index, q):
        if p_index == q_index:
            return 0
        return max(self.get_dist(p_index, p, q), self.get_dist(q_index, q, p))

    # From PreDeCon Definition 5
    def calculate_preference_weighted_neighbourhood(self, X):
        start_time = time.time()
        for index, point in enumerate(X):
            neighbourhood = []
            ids = []
            for i in range(len(X)):
				#i and index is used to extraxt preferred dimension of X[i] and point
                dist = self.get_prefered_dist(i, X[i], index, point)
                if dist <= self.radius_threshold ** 2:
                    neighbourhood.append(X[i])
                    ids.append(i)
            self.weighted_neighbourhoods.append(np.array(neighbourhood))
            self.weighted_ids.append(np.array(ids))
        print("Calculating pref weighted neighbourhood took %s seconds " % (time.time() - start_time))
        print('--------------------------')

    def get_preference_weighted_neighbourhood(self, index):
        return self.weighted_neighbourhoods[index]

    # From PreDeCon Definition 6
    def is_core(self, index, x):
        neighbourhood = self.get_preference_neighbourhood(index)
        weighted_pref_neighbourhood = self.get_preference_weighted_neighbourhood(index)
        value = self.get_preference_dimen(neighbourhood, x) <= self.p_lambda and len(
            weighted_pref_neighbourhood) >= self.p_minPoints
        return value

    # From PreDeCon Definition 7
    def is_direct_reach(self, p, p_index, q_index, q):
        neighbourhood_p = self.get_preference_neighbourhood(p_index)
        weighted_neighbourhood_q = self.get_preference_weighted_neighbourhood(q_index)
        value = self.is_core(q_index, q) and (self.get_preference_dimen(neighbourhood_p, p) <= self.p_lambda) and (
            p in weighted_neighbourhood_q)
        return value

    def is_offline_direct_reach(self, p, p_index, q_index, q):
        weighted_neighbourhood_q = self.get_preference_weighted_neighbourhood(q_index)
        pdim = (np.array(self.pcore_pref_dim[p_index]) > 1).sum()
        value = self.check_if_core(self.pcore_clusters[q_index], self.pcore_pref_dim[q_index]) and (
            pdim <= self.p_lambda) and (
                    p in weighted_neighbourhood_q)
        return value

    def get_euclidean_dist(self, a, b):
        return np.linalg.norm(np.asarray(a) - np.asarray(b))
