#!/usr/bin/env python3

# Main file that runs both ECM, HDDStream, Satellite Deletion, Clustering Evaluation
# Authors: Cameron Andrews, Deeksha Singh

import os
import sys

import numpy as np
import pandas as pd
from sklearn import datasets

import datetime
# from ECMwosm import ecm
from ECM import ecm
from MS import MS
from satellite_deletion import satellite_deletion
from clustering_evaluation import clustering_evaluation
from HDDStream import hddstream



os.makedirs('results', exist_ok=True)

ALG = sys.argv[1]  # load algoirthm choices arb, arb2, ecm, ms
RUNFILE = sys.argv[2]  # load in txt file containing seperate data files on lines


date = datetime.date.today().strftime('%B,%d,%Y')

class Controller():
    def __init__(self, fname=None, algo=None):

        # simple code to test iris dataset if an eligitimate runfile is sepcified
        iris = datasets.load_iris()
        self.X = iris.data
        self.Y = iris.target
        self.save_clust = None
        self.save_clust_normalised = None
        self.save_clust_with_dimen = None
        self.isiris = False
        self.ishddstream = False
        self.isecm = False
        self.num_dim = 0
        # load algorithm to use
        # paramaters are hard coded here
        self.params = []
        self.sd = None
        self.eval_function = None
        store_cluster_data = False
        store_max_min = False
        self.eval_limit = None
        self.parameterfp = '' # new arguments to distinguish results of different paramters from factorial design
        decay = 0
        if '--evaluate' in sys.argv:
            self.eval_limit = int(sys.argv[sys.argv.index('--evaluate') + 1])
            self.eval_function = clustering_evaluation(eval_limit=self.eval_limit)
            store_cluster_data = True
        if ALG == 'arb':
            self.clust = Cluster(decayrate=0.97, maxres=1000, mortality=1.0, k=0.001, NAT=0)
        elif ALG == 'arb2':
            self.clust = Cluster2(decayrate=0.97, maxres=1000, mortality=1.0, k=0.001, NAT=0)
        elif ALG == 'ecm':
            self.params = [1, 2000, 3, 1, 1]
            if len(sys.argv) > 3:
                if '--remove_outliers' in sys.argv:
                    store_max_min = True
                    self.sd = satellite_deletion(n_clust_init=self.params[0], max_samples=self.params[1],
                                                 sample_rate=self.params[2], reset=self.params[3],
                                                 punish=self.params[4])
            self.clust = ecm(n_clust_init=self.params[0], max_samples=self.params[1],
                             sample_rate=self.params[2], reset=self.params[3], punish=self.params[4],
                             store_cluster_data=store_cluster_data, store_cluster_limit=self.eval_limit,
                             store_max_min=store_max_min)
            self.isecm = True
        elif ALG == 'hddstream':
            mu = 5
            eps = 2.7
            delta = 1
            k = 100
            beta = 0.5
            decay = 1
            minPoints = 5
            section = 0.1
            if '-eps' in sys.argv:
                eps = float(sys.argv[sys.argv.index('-eps') + 1])
            if '-delta' in sys.argv:
                delta = float(sys.argv[sys.argv.index('-delta') + 1])
            if '-mu' in sys.argv:
                mu = int(float(sys.argv[sys.argv.index('-mu') + 1]))
            if '-minPoints' in sys.argv:
                minPoints = int(float(sys.argv[sys.argv.index('-minPoints') + 1]))
            if '-beta' in sys.argv:
                beta = float(sys.argv[sys.argv.index('-beta') + 1])
            if '-decay' in sys.argv:
                decay = float(sys.argv[sys.argv.index('-decay') + 1])
            if '-init' in sys.argv:
                section = float(sys.argv[sys.argv.index('-init') + 1])
            if '-parameterfp' in sys.argv:
                self.parameterfp = sys.argv[sys.argv.index('-parameterfp') + 1]
            if '-interval' in sys.argv:    
                self.parameterfp = sys.argv[sys.argv.index('-interval') + 1]
            
            self.clust = hddstream(mu_percentage=mu, eps=eps, delta=delta, k=k, beta=beta, decay=decay,
                                   minPoints_percentage=minPoints, section=section,
                                   store_cluster_data=store_cluster_data)
            self.ishddstream = True
        elif ALG == 'ms':
            self.clust = MS()

        self.parameterfp = self.parameterfp[42:-4].replace("/","_")
        runfile = RUNFILE[42:-4].replace("/","_")


        # set save file for clusters
        self.save_file = None
        self.save_file_normalised = None
        temp = '_'.join(str(i) for i in self.params)
        self.save_file = 'results/' + ALG + date + '_config' +'_' + self.parameterfp + '_'+ runfile + '_' + temp + '.csv'
        print('output file:',self.save_file,'decay is:',decay)
        self.save_file_with_dimen = None
        if self.ishddstream:
            self.save_file_with_dimen = 'results/' + ALG + date + '_config' + '_' + self.parameterfp + '_' + runfile + '_' + temp + 'with_preferred_dimensions.csv'
            self.save_file_normalised =  'results/' + ALG + date + '_config' +'_' + self.parameterfp + '_'+ runfile + '_' + temp + 'normalised.csv'

        if self.sd != None:
            self.sd.set_save_file(self.save_file[:-4] + '_satellites_deleted.csv')
            self.sd.set_results_file(self.save_fsile)

        if self.eval_function != None:
            self.eval_function.set_save_location(self.save_file[:-4] + '_eval')

    def run(self):
        print(RUNFILE)
        # if not using  a legitimite runfile load run iris data
        if not os.path.isfile(RUNFILE):
            self.save_file = None
            print("Running on IRIS Data")
            self.isiris = True
            self.maxes = np.max(self.X, axis=0)
            self.mins = np.min(self.X, axis=0)
            self.run_clust((self.X - np.min(self.X, axis=0)) / np.max(self.X, axis=0))
            return

        # load all files in the RUNFILE
        runfile = pd.read_csv(RUNFILE,sep = ',')
        #list of data files and corresponding time
        files = []
        times = []
        for i in range(runfile.shape[0]):
            files.append(runfile['file name'][i])
            times.append(runfile['time(in hour)'][i])

        data = []
        df = 0
        #for line in open(RUNFILE):
        for line in files:
            #line = line[:-1]  # skip end character
            if not os.path.isfile(line):
                print("missing file: ", line)
                continue
            print("starting file: ", line)
            # Load data according to limit, this is used to limit input data for eval
            if self.eval_limit is not None:
                df = pd.read_csv(line, nrows=self.eval_limit)
            else:
                df = pd.read_csv(line)
            data.append(df.values)

        # get max and mins of each column of each dataset(interval)
        self.maxes = [np.nanmax(i, axis=0) for i in data]
        self.mins = [np.nanmin(i, axis=0) for i in data]

        # get max-mins and mins from total dataset
        # Note max-mins makes it easier to scale to range [0,1]
        self.mins = np.array(self.mins).min(axis=0)
        self.maxes = np.array(self.maxes).max(axis=0) - self.mins
        self.df_columns = df.columns.values  # dimensions
        self.num_dim = len(self.df_columns)

        # for each interval (day), scale to [0.1] and run clustering
        self.time = 0
        count = 0
        for interval in data:
            # self.run_clust((interval - self.mins) / self.maxes)
            self.time = times[count]
            count += 1 
            print('clustering time(in hour) ' + str(self.time))
            self.run_clust(self.divide_with_zeros(interval - self.mins, self.maxes))


        # if we are saving, load mins, max-mins in first columns and save the clusters
        # Note:Df_columns is intiiated above, but is added to in run_clust
        if self.save_file != None:
            temp = np.hstack([-1, 0, 0, 'mins , (maxes-mins)', self.mins, self.maxes])
            if self.ishddstream:
                temp = np.hstack([-1, 0, 0, '[]', 0, np.zeros(self.num_dim)])
                if self.save_clust_with_dimen != None:
                    self.save_clust_with_dimen = np.vstack([temp, self.save_clust_with_dimen])
                    df = pd.DataFrame(self.save_clust_with_dimen, columns=self.df_columns)
                    df.to_csv(self.save_file_with_dimen, index=False)
                if self.save_clust_normalised != None:
                    self.save_clust_normalised = np.vstack([temp, self.save_clust_normalised])
                    df = pd.DataFrame(self.save_clust_normalised, columns=self.df_columns)
                    df.to_csv(self.save_file_normalised, index=False)
            self.save_clust = np.vstack([temp, self.save_clust])
            df = pd.DataFrame(self.save_clust, columns=self.df_columns)
            df.to_csv(self.save_file, index=False)

        # run satellite deletion
        if self.sd != None:
            self.sd.run_sd(self.clust.get_clusters(), self.clust.get_cluster_maxes(), self.clust.get_cluster_mins(),
                           self.clust.get_power(), self.clust.get_ids())
        if self.eval_function != None:
            self.eval_function.save()

    # Function to check zero denominator and perform division accordingly
    def divide_with_zeros(self, nom, den):
        result = nom
        for i in range(len(den)):
            if den[i] == 0.0:
                result.T[i] = 0.0
            else:
                result.T[i] /= den[i]
        return result

    def run_clust(self, X):
        # start clustering algorihm if not started, otherwise add a new set of data
        if not self.clust.is_started():
            if ALG == 'hddstream':
                self.clust.start(X,self.time)
            else:
                self.clust.start(X)
        else:
            if ALG == 'hddstream':
                self.clust.newdata(X,self.time)
            else:
                self.clust.newdata(X)
        clusters = self.clust.get_clusters() * self.maxes + self.mins
        # incase we are testing on the iris dataset
        # Otherwise widely ignored
        if self.isiris:
            result = np.zeros(X.shape[0])
            for i, data in enumerate(X):
                result[i] = self.clust.test_point(data)
            check = np.zeros((clusters.shape[0], 3))
            print(check)
            for i, data in enumerate(result):
                print(i, data)
                check[data, self.Y[i]] += 1
            print(check)
        # Save cluster data for printing to csv depending on algorithm
        if self.isecm:
            self.save_ecm(clusters)
        elif self.ishddstream:
            self.save_hddstream(clusters)
        else:
            self.save_other(clusters)

    def save_hddstream(self, clusters):
        print("clusters")
        print(clusters)
        if self.save_file != None:
            ids = self.clust.get_pcore_ids()
            times = np.full(clusters.shape[0], self.time, dtype=int)
            clusters = np.array(clusters, dtype=object)
            clusters_normalised = np.array((clusters-self.mins)/self.maxes,dtype=object)
            pref_dimen = self.clust.get_pref_dimen()
            clusters_with_dimen = []
            
            for index, pmc in enumerate(clusters):
                current = pref_dimen[index]
                curr_cluster = np.copy(clusters[index])
                print('pref dimen ' + str(current))
                for i, dim in enumerate(current):
                    if dim == 1:
                        curr_cluster[i] = 'NA'
                clusters_with_dimen.append(curr_cluster)
            
            clusters_with_dimen = np.array(clusters_with_dimen, dtype=object)

			
            num_ids = [len(id) for id in ids]
            ids_str = [''.join(str(i)) for i in ids]

            clusters = np.insert(clusters, 0, num_ids, axis=1)
            clusters = np.insert(clusters, 0, ids_str, axis=1)
            clusters = np.insert(clusters, 0, self.clust.get_size(), axis = 1)
            clusters = np.insert(clusters, 0, self.clust.get_cumulative_size(), axis=1)
            clusters = np.insert(clusters, 0, times, axis=1)
            
            clusters_normalised = np.insert(clusters_normalised, 0, num_ids, axis=1)
            clusters_normalised = np.insert(clusters_normalised, 0, ids_str, axis=1)
            clusters_normalised = np.insert(clusters_normalised, 0, self.clust.get_size(), axis = 1)
            clusters_normalised = np.insert(clusters_normalised, 0, self.clust.get_cumulative_size(), axis=1)
            clusters_normalised = np.insert(clusters_normalised, 0, times, axis=1)

            clusters_with_dimen = np.insert(clusters_with_dimen, 0, num_ids, axis=1)
            clusters_with_dimen = np.insert(clusters_with_dimen, 0, ids_str, axis=1)
            clusters_with_dimen = np.insert(clusters_with_dimen, 0, self.clust.get_size(), axis=1)            
            clusters_with_dimen = np.insert(clusters_with_dimen, 0, self.clust.get_cumulative_size(), axis=1)
            clusters_with_dimen = np.insert(clusters_with_dimen, 0, times, axis=1)

            # if first time through set-up
            if self.save_clust == None:
                self.df_columns = np.insert(self.df_columns, 0, 'pcore_power')
                self.df_columns = np.insert(self.df_columns, 0, 'pcore_ids')
                self.df_columns = np.insert(self.df_columns, 0, 'size')
                self.df_columns = np.insert(self.df_columns, 0, 'cumulative size')
                self.df_columns = np.insert(self.df_columns, 0, 'time(in hour)')
                self.save_clust = clusters
            else:
                self.save_clust = np.vstack([self.save_clust, clusters])

            if self.save_clust_normalised is None:
                self.save_clust_normalised = clusters_normalised
            else:
                self.save_clust_normalised = np.vstack([self.save_clust_normalised,clusters_normalised])
            
            if self.save_clust_with_dimen is None:
                self.save_clust_with_dimen = clusters_with_dimen            
            else:
                self.save_clust_with_dimen = np.vstack([self.save_clust_with_dimen, clusters_with_dimen])
        # Execute clustering quality evaluation
        if self.eval_function: self.eval_function.evaluate(self.clust.get_cluster_data(), self.clust.get_clusters())

    def save_ecm(self, clusters):
        # get clusters and variances and put back to original values
        print("clusters")
        print(clusters)
        print("links")
        print(self.clust.get_links() * self.maxes)
        print("power")
        print(self.clust.get_power())
        print(clusters.shape)
        # below sets up the columns needed for saving and stacks the clusters over intervals
        if self.save_file != None:
            ids = self.clust.get_ids()
            history = self.clust.get_history()
            times = np.full(clusters.shape[0], self.time, dtype=int)
            clusters = np.array(clusters, dtype=object)
            clusters = np.insert(clusters, 0, history, axis=1)
            clusters = np.insert(clusters, 0, self.clust.get_power(), axis=1)
            clusters = np.insert(clusters, 0, ids, axis=1)
            clusters = np.insert(clusters, 0, times, axis=1)
            clusters = np.hstack([clusters, self.clust.get_links() * self.maxes])
            # if first time through set-up
            if self.save_clust == None:
                var_columns = ['var_' + i for i in self.df_columns]
                self.df_columns = np.append(self.df_columns, var_columns)
                self.df_columns = np.insert(self.df_columns, 0, 'history')
                self.df_columns = np.insert(self.df_columns, 0, 'power')
                self.df_columns = np.insert(self.df_columns, 0, 'id')
                self.df_columns = np.insert(self.df_columns, 0, 'time')
                self.save_clust = clusters
            else:
                self.save_clust = np.vstack([self.save_clust, clusters])
        # Execute cluster quality evaluation
        if self.eval_function: self.eval_function.evaluate(self.clust.get_cluster_data(), self.clust.get_clusters())

    def save_other(self, clusters):
        self.df_columns = np.insert(self.df_columns, 0, 'history')
        self.df_columns = np.insert(self.df_columns, 0, 'power')
        self.df_columns = np.insert(self.df_columns, 0, 'id')
        self.df_columns = np.insert(self.df_columns, 0, 'time')
        ids = np.arange(clusters.shape[0])
        times = np.full(clusters.shape[0], self.time, dtype=int)
        clusters = np.insert(clusters, 0, self.clust.get_power(), axis=1)
        clusters = np.insert(clusters, 0, ids, axis=1)
        clusters = np.insert(clusters, 0, times, axis=1)
        if self.save_clust == None:
            self.save_clust = clusters
        else:
            self.save_clust = np.vstack([self.save_clust, clusters])


if __name__ == "__main__":
    Controller().run()


