# Create graphs to approximate lambda param for THDDStream
# Author: Mark Read

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams  # better layout for figures.

rcParams.update({'figure.autolayout': True})

linewidth = 2.
fontsize = 18.


def prob(age, lamb):
    # smaller values of alpha result in slower growth of probability with time.
    tmp = -age * lamb
    return 2 ** tmp


t = np.arange(0., 8., 0.1)  # time

lambda1 = 1.
lambda2 = 2.
decay = prob(age=t, lamb=lambda1)
decay2 = prob(age=t, lamb=lambda2)

plt.clf()
plt.plot(t, decay, color='k', linewidth=linewidth)
plt.plot(t, decay2, color='r', linewidth=linewidth)
plt.legend([str(lambda1), str(lambda2)], loc=4)
plt.ylabel('Weight')
plt.xlabel('Age of sample')
plt.gca().grid(True, linewidth=linewidth)  # turn on grid lines.
font = {'size': fontsize}  # dictionary
plt.rc('font', **font)
plt.savefig('lambda.png', format='png', dpi=200)
