# Removes data points outside of a sd threshold. After discussion with Tom Ashhurst, we concluded this was not a viable
# method of outlier removal.
# Author: Deeksha Singh

import numpy as np
import pandas as pd
import sys
import os


class RemoveOutliers:
    def __init__(self):
        self.run_file = sys.argv[1]
        if len(sys.argv) < 4:
            print('missing argument')
            return

        self.lower_limit = sys.argv[2]
        self.upper_limit = sys.argv[3]

        name = self.run_file[:-4].split('/')
        self.run_file_name = name[len(name) - 1]

        self.save_file_location = self.run_file[:-4] + '_cleaned/'
        os.makedirs(self.save_file_location, exist_ok=True)

    def run(self):
        if not os.path.isfile(self.run_file):
            print('missing file: ', self.run_file)
            return

        print("starting file: ", self.run_file)
        df = pd.read_csv(self.run_file)
        original_len = len(df)

        for col in df.columns:
            mean = np.mean(df[col])
            std = np.std(df[col])
            df = df[(df[col] > mean - int(self.lower_limit) * std) & (df[col] < mean + int(self.upper_limit) * std)]

        final_len = len(df)
        print('final csv has been reduced to ' + str((final_len / original_len) * 100) + ' %')
        dfn = pd.DataFrame(df, columns=df.columns)
        dfn.to_csv(self.save_file_location + self.run_file_name + '_' + self.lower_limit + '_' +
                   self.upper_limit + '.csv', index=False)


if __name__ == "__main__":
    RemoveOutliers().run()
