import numpy as np
import pandas as pd

E = 0.0000001

# Removes insignificant clusters after execution of ECM using work of Lughofer et a.
# Author: Deeksha Singh

class satellite_deletion(object):
    def __init__(self, n_clust_init=1, max_samples=300, sample_rate=3, reset=1, punish=2):
        print('beginning satellite deletion')
        self.low_mass = 2
        self.n_clust = -1
        self.n_orig = n_clust_init
        self.max_samples = max_samples
        self.sample_rate = sample_rate - 1
        self.reset = reset
        self.punish = punish  # punishment factor see self.criteria below for its impact]
        self.save_file = './sd_temp.csv'
        self.results_file = None
        self.clusters_to_remove = []
        self.thr = 0.9

    def run_sd(self, clusters, c_maxes, c_mins, powers, c_ids):
        print('beginning satellite deletion')
        self.saved_original_clusters = clusters
        self.clusters = clusters
        self.c_maxes = c_maxes
        self.c_mins = c_mins
        self.c_counts = powers
        self.variances = np.zeros((self.n_orig, self.clusters.shape[1]))
        self.N = np.sum(self.c_counts)

        for index, cluster in enumerate(self.clusters):
            k = self.c_counts[index]
            # Remove clusters of low mass
            if (k / self.N) * 100 < 1:
                print('1(1%): marking cluster for removal')
                self.clusters_to_remove.append(c_ids[index])
                continue

            # Equation 6
            qminsc2 = (cluster - self.clusters) ** 2
            # Equation 8
            sumqcv = np.sum(qminsc2 / (self.variances ** 2 + E), axis=1)
            inside_clusters = np.where(sumqcv < 1)
            # Equation 7
            t = 1 / (np.sqrt(sumqcv))
            dists = (1 - t) * (np.sqrt(np.sum(qminsc2, axis=1)))
            min_dist = np.nanargmin(dists)
            win = min_dist
            min_dist = dists[min_dist]
            # range of influence of cluster in consideration
            edgeOfCluster = np.sum(self.c_maxes[index] - self.c_mins[index])
            # range of influence of winning cluster
            edgeOfWin = np.sum(self.c_maxes[win] - self.c_mins[win])
            print('vars initialised: edgeOfCluster ' + str(edgeOfCluster) + ' edgeOfWin ' + str(
                edgeOfWin) + ' win dist ' + str(win))
            if (k / self.N) * 100 < self.low_mass:
                if inside_clusters[0].shape[0] > 0 and edgeOfCluster / edgeOfWin < E:
                    print('2(low mass): marking cluster for removal ')
                    self.clusters_to_remove.append(c_ids[index])
            else:
                print('3(small influence): marking cluster for removal')
                if min_dist < self.thr and edgeOfCluster / edgeOfWin < E:
                    self.clusters_to_remove.append(c_ids[index])

        print('satellite deletion complete')
        self.save_clusters()

    def set_save_file(self, save_file):
        self.save_file = save_file

    def set_results_file(self, results_file):
        self.results_file = results_file

    def save_clusters(self):
        print("saving new clusters at " + self.save_file)
        df = pd.read_csv(self.results_file)
        for id in self.clusters_to_remove:
            df = df[df.id != id]
        df.to_csv(self.save_file, index=False)
