#!/usr/bin/env python3

import numpy as np
import pandas as pd
import sys
import os
from scipy.spatial.distance import pdist, squareform
from sklearn import datasets
from clustering1 import Cluster
from clustering2 import Cluster as Cluster2
#from ECMwosm import ecm
from ECM import ecm
from MS import MS
os.makedirs('results', exist_ok=True)

ALG = sys.argv[1] #load algoirthm choices arb, arb2, ecm, ms
RUNFILE = sys.argv[2] # load in txt file containing seperate data files on lines
SUBSETS = sys.argv[3] #csv with cutoff values as specified in README

class Controller():
	def __init__(self, fname=None, algo=None):

		#simple code to test iris dataset if an eligitimate runfile is sepcified
		iris = datasets.load_iris()
		self.X = iris.data
		self.Y = iris.target
		self.save_clust = None
		self.isiris = False
		self.params = []
		self.cuttofs = pd.read_csv(SUBSETS)

		#load algorithm to use
		#paramaters are hard coded here
		if ALG == 'arb':
			self.clust = Cluster(decayrate = 0.97, maxres = 1000, mortality = 1.0, k = 0.001, NAT = 0)
		elif ALG == 'arb2':
			self.clust = Cluster2(decayrate = 0.97, maxres = 1000, mortality = 1.0, k = 0.001, NAT = 0)
		elif ALG == 'ecm':
			self.params = [1,2000,3,1,2]
			self.clust = ecm(n_clust_init = self.params[0], max_samples = self.params[1], 
				sample_rate = self.params[2], reset = self.params[3], punish = self.params[4])
		elif ALG == 'ms':
			self.clust = MS()

		#set save file for clusters
		self.save_file = None
		temp = '_'.join(str(i) for i in self.params)
		self.save_file = ['results/' + i[1] + ALG + '_' + RUNFILE[:-4] + '_' + temp + '.csv' for i in self.cuttofs.values]
		print(self.save_file)
	def run(self):
		print(RUNFILE)
		#if not using  a legitimite runfile load run iris data
		if not os.path.isfile(RUNFILE):
			self.save_file = None
			print("Running on IRIS Data")
			self.isiris = True
			self.maxes = np.max(self.X, axis = 0)
			self.mins = np.min(self.X, axis = 0)
			self.run_clust((self.X - np.min(self.X, axis = 0))/np.max(self.X, axis = 0))
			return


		#load all files in the RUNFILE
		data = []
		df = 0
		for line in open(RUNFILE):
			line = line[:-1] #skip end character
			if not os.path.isfile(line):
				print("missing file: ", line)
				continue
			print("starting file: ", line)
			df = pd.read_csv(line)
			df.drop('Time', axis=1, inplace=True)
			data.append(df.values)

		#get max and mins of each column of each dataset(interval)
		self.maxes = [np.max(i, axis = 0) for i in data]
		self.mins = [np.min(i, axis = 0) for i in data]

		#get max-mins and mins from total dataset
		#Note max-mins makes it easier to scale to range [0,1]
		self.mins =  np.array(self.mins).min(axis = 0)
		self.maxes = np.array(self.maxes).max(axis = 0) - self.mins
		self.df_columns = df.columns.values #dimensions
		

		self.time = 0
		count = 0
		for index, subset in self.cuttofs.iterrows():
			print(subset)
			#set columns we want to save, including a variance value for all columns
			var_columns = ['var_' + i for i in self.df_columns]
			use_columns = np.append(self.df_columns, var_columns)
			use_columns = np.insert(use_columns, 0, 'history')
			use_columns = np.insert(use_columns, 0, 'power')
			use_columns = np.insert(use_columns, 0, 'id')
			use_columns = np.insert(use_columns, 0, 'time')
			#use_columns = np.delete(use_columns, np.where(use_columns ==subset['Paramater']))
			#use_columns = np.delete(use_columns, np.where(use_columns =='var_' + subset['Paramater']))

			#for each interval, scale to [0.1] and run clustering
			for interval in data:
				use_data = (interval-self.mins)/self.maxes
				rem_col = np.where(self.df_columns == subset['Paramater'])
				use_where = np.where(np.squeeze(interval[:,rem_col]) > subset['Minimum value'])
				#use_data = np.delete(use_data, rem_col, axis = 1)
				print(use_data.shape, use_data[use_where].shape)
				self.run_clust(use_data[use_where])
				self.time += 1

			#if we are saving, load mins, max-mins in first columns and save the clusters
			#Note:Df_columns is intiiated above, but is added to in run_clust
			if self.save_file != None:
				temp = np.hstack([-1,0,0,'mins , (maxes-mins)',self.mins, self.maxes])
				self.save_clust = np.vstack([temp, self.save_clust])
				df = pd.DataFrame(self.save_clust, columns = use_columns)
				df.to_csv(self.save_file[count] , index = False)
			count += 1
			#reset clustering algorithm, but keep settings
			self.clust.set_new() 
			self.save_clust = None
			self.time = 0


	def run_clust(self, X):
		#start clustering algorihm if not started, otherwise add a new set of data
		if not self.clust.is_started():
			self.clust.start(X)
		else:
			self.clust.newdata(X)

		#get clusters and variances and put back to original values
		clusters = self.clust.get_clusters()*self.maxes + self.mins
		print("clusters")
		print(clusters)
		print("links")
		print(self.clust.get_links()*self.maxes)
		print("power")
		print(self.clust.get_power())
		print(clusters.shape)

		#below sets up and stacks the clusters for saving
		if self.save_file != None:
			if ALG == 'ecm':
				ids = self.clust.get_ids()
				history = self.clust.get_history()
				times = np.full(clusters.shape[0], self.time, dtype = int)
				clusters = np.array(clusters, dtype = object)
				clusters = np.insert(clusters, 0, history, axis = 1)
				clusters = np.insert(clusters, 0, self.clust.get_power(), axis = 1)
				clusters = np.insert(clusters, 0, ids, axis = 1)
				clusters = np.insert(clusters, 0, times, axis = 1)
				clusters = np.hstack([clusters, self.clust.get_links()*self.maxes])
				#if first time through set-up
				if self.save_clust == None:
					self.save_clust = clusters
				else:
					self.save_clust = np.vstack([self.save_clust, clusters])
			else:
				#not sure if this if/else is actially needed, may be able to remove below code and if statement
				self.df_columns = np.insert(self.df_columns, 0, 'history')
				self.df_columns = np.insert(self.df_columns, 0, 'power')
				self.df_columns = np.insert(self.df_columns, 0, 'id')
				self.df_columns = np.insert(self.df_columns, 0, 'time')
				ids = np.arange(clusters.shape[0])
				times = np.full(clusters.shape[0], self.time, dtype = int)
				clusters = np.insert(clusters, 0, self.clust.get_power(), axis = 1)
				clusters = np.insert(clusters, 0, ids, axis = 1)
				clusters = np.insert(clusters, 0, times, axis = 1)
				if self.save_clust == None:
					self.save_clust = clusters
				else:
					self.save_clust = np.vstack([self.save_clust, clusters])


if __name__ == "__main__":
	Controller().run()