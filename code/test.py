import os
import sys
import numpy as np
import csv
import pandas
import matplotlib.pyplot as plt

class example(object):
    a = 5

class example1(example):
	@staticmethod
	def get_a():
		return example1.a
    
 

print(example.a)
print(example1.a)
example1.a = 6
print(example.a)
print(example1.a)

class e1(object):
	def __init__(self):
		self.p = 1
		
class e2(object):
	def __init__(self):
		self.p = 2
	def change(self,newe):
		newe.p = 3
		
i1 = e1()


def f(x):
	x.p += 1
	
f(i1)
f(i1)
print(i1.p)

figure_size = [10,8]

#table_fp = '/home/ttan6729/HCT2016_02/Data_set/Data_set/CSV_Original_values/Sample_5_OriginalScale.csv'
table_fp ='/home/ttan6729/HCT2016_02_new/Simplified_data_set/CSV/Simplified_Sample_5_CF200.csv'

index = np.array(pandas.read_csv(table_fp,header = None,sep = ','))[0,:]
#remove the first row in, which is index
table = np.array(pandas.read_csv(table_fp, sep=','))[1:]
dimensions = table.shape[1]

for i in range(dimensions):
	d = table[:,i]
	fig = plt.figure(figsize = figure_size)
	ax = fig.add_subplot(111)
	bp = ax.boxplot(d)
	ax.set_ylabel(index[i])
	plt.show()


import os
import sys
import csv
import numpy as np
import matplotlib.pyplot as plt
import pandas


size_location = 2
factor = 20
standard_size = 5000
d1 = int(sys.argv[1])
d2 = int(sys.argv[2])


#expected_clusters_fp ='hddstream_WNV_all_notcleaned_with_history_TAannotation_normalized.csv'
expected_clusters_fp = 'simplified_clusters_normalized.csv'
#result_fp = 'results_19,Feb/hddstreamFebruary,19,2017_config_parameters_samp53__normalised.csv'

result_id =522
#result_fp = 'results_21,Feb/hddstreamFebruary,21,2017_config_parameters_samp{i:d}_d_normalised.csv'.format(i = result_id)
result_fp = 'hddstream_WNV1_simplified_normalized_previous.csv'
#save = 'result{i:d}'.format(i=result_id)
save = 'previous'
save_fp = 'comparsions_diagram/' + save
if not os.path.isdir(save_fp):
	os.mkdir(save_fp)

column_name = []
with open(result_fp,'r') as f:
	reader = csv.reader(f)
	column_name = next(reader)

d1_name = column_name[-d1]
d2_name = column_name[-d2]


expected_data = np.array(pandas.read_csv(expected_clusters_fp,','))
expected_time = expected_data[:,0]
time_values = np.unique(expected_time)

result_data = np.array(pandas.read_csv(result_fp,','))
result_time = result_data[:,0]

print('a',result_time)
print('b',expected_time)
print('c',time_values)

a 

for t in time_values:
	print('t is',t)
	fig = plt.figure()
	plt.xlim(0, 0.8)
	plt.ylim(0, 0.8)
	ax = fig.add_subplot(1,1,1)
	
	result_d1 = (result_data[np.where(result_time == t)])[:,-d1]
	result_d2 = (result_data[np.where(result_time == t)])[:,-d2]
	result_size =  [ int(factor*n/standard_size) for n in result_data[np.where(result_time == t)][:,1] ]
	
	expected_d1 = (expected_data[np.where(expected_time == t)])[:,-d1]
	expected_d2 = (expected_data[np.where(expected_time == t)])[:,-d2]
	expected_size =  [ int(factor*n/standard_size) for n in expected_data[np.where(expected_time == t)][:,2] ]
	
	red_point = ax.scatter(expected_d1,expected_d2,color = 'r',s = expected_size)
	green_point = ax.scatter(result_d1,result_d2,color='g',s = result_size)
	
	fig.suptitle('time in hours: ' + str(t) , fontsize =18)
	ax.set_xlabel(d1_name,fontsize = 16)
	ax.set_ylabel(d2_name,fontsize = 16)
	
	plt.legend((red_point,green_point),('expected clusters','predicted clusters'),scatterpoints = 1,loc = 'upper right')
	plt.show()
	
	fig.savefig(save_fp+'/'+ 'time' + str(t) + '_'+ d1_name+ '_' + d2_name+ '.png')
	
