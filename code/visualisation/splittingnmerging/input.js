/**
 * Created by ruigeng on 21/1/17.
 */

let markers = [];

function add_input_markers() {
    if(!input_data) {
        d3.csv(INPUT_DATA, function (data) {
            input_data = data;

            if (input_data) {
                let dropDown = document.getElementById("dropdown");
                for (let i = 0; i < input_data.length; i++) {
                    let option = document.createElement("option");
                    option.text = option.value = input_data[i].name;
                    dropDown.add(option);
                }
                dropDown.onchange = function () {
                    for (let data of input_data) {
                        if (data["name"] == this.value) {
                            update_input(data);
                            on_run_button_clicked();
                            break;
                        }
                    }
                };

                init_markers();
                generate_input();
                update_input(input_data[0]);
            }
        });
    }
}

function init_markers() {
    let count = countProperties(data[0]);
    let index = 0;
    markers = [];

    for (let key in data[0]) {
        if (data[0].hasOwnProperty(key)) {
            if (count - NUMBER_OF_MARKERS <= index) {
                markers.push(key);
            }
            index++;
        }
    }
}

function generate_input() {
    let container = document.getElementById("container");

    let button = document.createElement("input");
    button.type = "button";
    button.setAttribute("value", "Run");
    button.onclick = on_run_button_clicked;

    container.appendChild(button);
    container.appendChild(document.createElement("br"));

    for (let i = 0; i < markers.length; i++) {
        container.appendChild(add_button(markers[i]));
        container.appendChild(add_field(markers[i]));

        if (i > 0 && i % 3 == 0) {
            container.appendChild(document.createElement("br"));
        }
    }
}

function colour_by_one_dimension(marker_name) {
    let selected_cluster = new Cluster();

    for (let i = 0; i < markers.length; i++) {
        selected_cluster[markers[i]] = marker_name == markers[i] ? 0.00001 : 0;
    }

    update_colour(selected_cluster);
}

function add_button(name) {
    let button = document.createElement("input");
    button.type = "button";
    button.setAttribute("value", name);
    button.addEventListener("click", function () {
       colour_by_one_dimension(name);
    });

    return button;
}

function add_field(name) {
    let input = document.createElement("input");
    input.type = "text";
    input.name = name;
    input.setAttribute("value", 0);
    input.setAttribute("class", "input marker");
    input.onclick = function () {
        if (this.value == 0) this.value = "";
    };

    input.onblur = function () {
        if (this.value == "") this.value = 0;
    };

    return input;
}

function on_run_button_clicked() {
    let selected_cluster = new Cluster();

    for (let i = 0; i < markers.length; i++) {
        let text = document.getElementsByName(markers[i]);
        if (!text[0].value) {
            text[0].value = 0;
        }
        selected_cluster[markers[i]] = +text[0].value;
    }

    update_colour(selected_cluster);
}

function update_input(data) {
    for (let key in data) {
        if (key != "name" && data.hasOwnProperty(key)) {
            document.getElementsByName(key)[0].value = data[key];
        }
    }
}