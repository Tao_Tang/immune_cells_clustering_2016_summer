/**
 * Created by ruigeng on 11/1/17.
 */

function Cluster() {
    this.x = 0;
    this.y = 0;
    this.time = -1;
    this.ids = [];
    this.fill = false;
    this.distance = 0;
}

function Time(number) {
    this.number = number;
    this.clusters = [];
}

function Line(x1, x2, y1, y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;

    function equals(object) {
        return x1 == object.x1 && x2 == object.x2 && y1 == object.y1 && y2 == object.y2;
    }
}