/**
 * Created by rgeng on 2/12/2017.
 */
let maxValue = 0;
let radar_clusters_stack = [];

function add_cluster_into_radar(cluster) {

    let index = radar_clusters_stack.indexOf(cluster);
    if (index != -1) {
        radar_clusters_stack.splice(index, 1);
    } else {
        radar_clusters_stack.push(cluster);
    }

    draw_radar();
}

function draw_radar() {
    if (radar_clusters_stack.length == 0) {
        d3.select("#radar_chart").select("svg").remove();
    } else {
        let radar_data_list = build_radar_data();

        let colors = [];
        radar_data_list.forEach(function (entry, i) {
            colors.push(d3.schemeCategory20[i]);
        });

        let mycfg = {
            w: 500,
            h: 400,
            maxValue: maxValue,
            levels: 7,
            ExtraWidthX: 300,
            ExtraWidthY: 0,
            TranslateX: 50,
        };

        RadarChart.draw("#radar_chart", radar_data_list, mycfg, colors, radar_clusters_stack);
    }
}

function build_radar_data() {
    let radar_data = [];

    for (let cluster of radar_clusters_stack) {
        let data = [];
        let count = countProperties(cluster);

        let index = 0;
        for (let key in cluster) {
            if (cluster.hasOwnProperty(key)) {
                if (count - NUMBER_OF_MARKERS <= index) {
                    let object = {axis: key, value: cluster[key]};
                    data.push(object);
                    if (cluster[key] > maxValue) maxValue = cluster[key];
                }
                index++;
            }
        }

        radar_data.push(data);
    }

    return radar_data;
}

function clear_radar() {
    radar_clusters_stack = [];
    draw_radar();
    update_radar();
}