const NUMBER_OF_MARKERS = 3;

let data;
let g;

const INPUT_DATA = "data/input_drop_down_data.csv";
var input_data;

let color = d3.schemeCategory10;
var m = [50, 50, 50, 50],//margin
    w = 1400 - m[0] - m[2],//width
    h = 800 - m[1] - m[3];//height

var x;
var y = {};

let svg = d3.select("#visualization").append("svg")
    .attr("width", w)
    .attr("height", h)
    .append("g")
    .attr("transform", "translate(" + m[0] + "," + m[1] + ")");

var line = d3.line(),
    axis = d3.axisLeft();

var cluster_numbers = [];
let scaledPower;

let times = [];
let lines = [];
let files = [];
let clusters = [];

let colours;

let selectedFileIndex = 0;
let selected_cluster;

function fileSelected(input) {
    selectedFileIndex = 0;
    files = input.files;

    loadFile();

    document.addEventListener('keydown', onKeyDown, false);
}

function onKeyDown(e) {
    e = e || window.event;

    if (e.keyCode == '37') {
        onPrevFile();
    } else if (e.keyCode == '39') {
        onNextFile();
    }
}

function loadFile() {
    times = [];
    lines = [];
    cluster_numbers = [];

    svg.selectAll("rect").remove();
    svg.selectAll(".models").remove();
    svg.selectAll(".cluster").remove();
    svg.selectAll(".times").remove();
    svg.selectAll("g").remove();
    svg.selectAll("line").remove();

    let reader = new FileReader();
    reader.onload = function (e) {
        imgURL = e.target.result;
        load_data(imgURL);
    };
    reader.readAsDataURL(files[selectedFileIndex]);

    let nestedDiv = document.getElementById("fileName");
    nestedDiv.textContent = files[selectedFileIndex].name;
}

function onPrevFile() {
    if (selectedFileIndex != 0) {
        selectedFileIndex--;
        loadFile();
    }
}

function onNextFile() {
    if (selectedFileIndex != files.length - 1) {
        selectedFileIndex++;
        loadFile();
    }
}

function load_data(url) {
    d3.csv(url, function (d) {
        data = d;

        solve_data();
        draw_lines();
        rendering();

        if(!input_data) {
            add_input_markers();
        }
    });
}

function solve_data() {
    clusters = [];

    // data cleaning
    if (data[0].time_in_hour < 0) {
        data.splice(0, 1);
    }

    let t;
    for (let i = 0; i < data.length; i++) {
        let c = new Cluster();
        c.time = data[i].time;

        for (let key in data[i]) {
            if (data[i].hasOwnProperty(key)) {
                c[key] = data[i][key];
            }
        }

        // remove the "[" and "]"
        let ids = data[i].pcore_ids.substring(1, data[i].pcore_ids.length - 1);
        if(ids[0] == " ") {
            ids = ids.substring(1);
        }
        ids = ids.replace(/\s+/g, " ");
        c.ids = ids.split(",");

        if (i == 0 || data[i].time_in_hour != data[i - 1].time_in_hour) {
            t = new Time(data[i].time_in_hour);
            times.push(t);
        }
        t.clusters.push(c);
        clusters.push(c);
    }

    scaledPower = d3.scaleLinear().domain([d3.min(data, function (d) {
        return +d["size"];
    }),
        d3.max(data, function (d) {
            return +d["size"];
        })])
        .range([5, 20]);

    let timesDomain = [];
    for (let time in times) {
        timesDomain.push(time);
    }
    x = d3.scalePoint().domain(timesDomain).range([0, w]);

    times.forEach(function (d, i) {
        // Coerce values to numbers.
        y[d] = d3.scaleLinear()
            .domain(cluster_numbers, 0)
            .range([h, 0]);
    });

    for (let i = 0; i < times.length; i++) {
        let time = times[i];
        let bandwidth = w / (times.length);
        let bandheight = h / (time.clusters.length + 2);

        for (let j = 0; j < time.clusters.length; j++) {
            let cluster = time.clusters[j];
            cluster.x = bandwidth * i;
            cluster.y = bandheight * (time.clusters.indexOf(cluster) + 1);
        }
    }
}

function rendering() {

    g = svg.selectAll(".times")
        .data(times)
        .enter().append("svg:g")
        .attr("class", "times")
        .attr("transform", function (d, i) {
            return "translate(" + x(i) + ")";
        });

    g.append("svg:g")
        .attr("class", "axis")
        .each(function (d) {
            d3.select(this).call(axis.scale(y[d]));
        })
        .append("svg:text")
        .attr("text-anchor", "middle")
        .attr("y", -9)
        .text(function (d) {
            return "time: " + d.number;
        })
        .attr("fill", "black")
        .style("cursor", "pointer");

    g.append("svg:g")
        .selectAll(".cluster")
        .data(function (time) {
            return time.clusters;
        })
        .enter().append("circle")
        .attr("class", "cluster")
        .attr("r", function (d) {
            return scaledPower(d.size);
        })
        .attr("cx", function (d, i) {
            return 0;
        })
        .attr("cy", function (d, i, circles) {
            return (h / (circles.length + 2)) * (i + 1);
        })
        .style("stroke", "gray")
        .attr("stroke-width", 0.3)
        .on("mouseover", on_mouse_over)
        .on("mouseout", on_mouse_out)
        .on("click", function (d, i) {
            if(window.event.altKey) {
                if(window.event.shiftKey) d.fill = true;
                add_cluster_into_radar(d);
                update_radar(d);
            }
            else update_colour(d);
        });
}

function update_colour(cluster) {
    selected_cluster = cluster;

    updateDistances(cluster);
    update_circles();
}

function update_radar() {
    g.selectAll(".cluster")
        .attr("stroke-width", function (d) {
            return radar_clusters_stack.indexOf(d) >= 0 ? 3 : 1;
        })
        .style("stroke", function (d) {
            let index = radar_clusters_stack.indexOf(d);
            return index >= 0 ? d3.schemeCategory20[index] : "gray";
        });
        // .attr("fill", function (d) {
        //     return "rgb(255," + colours(d.distance) + "," + colours(d.distance) + ")";
        // });
}

function update_circles() {
    colours = d3.scaleLinear().domain([
        0,
        d3.max(times, function (d) {
            return d3.max(d.clusters, function (c) {
                return c.distance;
            })
        })])
        .rangeRound([0, 255]);

    g.selectAll(".cluster")
        .attr("fill", function (d) {
            return "rgb(255," + colours(d.distance) + "," + colours(d.distance) + ")";
        });

    svg.selectAll(".cluster_line")
        .attr("stroke", function (d) {
            return d.nodes1.hasSameValue(selected_cluster.ids) && d.nodes2.hasSameValue(selected_cluster.ids)? "red" : "gray";
        })
        .attr("stroke-width", function (d) {
            return d.nodes1.hasSameValue(selected_cluster.ids) && d.nodes2.hasSameValue(selected_cluster.ids)? d.distance : 0.1;
        });
}

function reset_lines() {
    svg.selectAll(".cluster_line")
        .attr("stroke", "black")
        .attr("stroke-width", function (d) {
            return d.distance;
        });
}

function draw_lines() {
    for (let i = 1; i < times.length; i++) {
        let current_time = times[i];
        let prev_time = times[i - 1];

        for (let p = 0; p < current_time.clusters.length; p++) {
            for (let q = 0; q < prev_time.clusters.length; q++) {
                if (draw_line_condition(current_time.clusters[p], prev_time.clusters[q])) {
                    let new_line = new Line(current_time.clusters[p].x,
                        prev_time.clusters[q].x,
                        current_time.clusters[p].y,
                        prev_time.clusters[q].y);

                    new_line.distance = euclidean(current_time.clusters[p], prev_time.clusters[q]);
                    new_line.nodes1 = current_time.clusters[p].ids;
                    new_line.nodes2 = prev_time.clusters[q].ids;

                    lines.push(new_line);
                }
            }
        }
    }

    let line_set = new Set(lines);

    let scaledDistance = d3.scaleLinear().domain([d3.min(lines, function (d) {
        return d.distance;
    }),
        d3.max(lines, function (d) {
            return d.distance;
        })])
        .range([10,1]);

    for (let line of line_set) {
        line["distance"] = scaledDistance(line.distance);
    }

    lines = Array.from(line_set);
    rendering_lines();
}

function draw_line_condition(current_cluster, prev_cluster) {
    return current_cluster.ids.hasSameValue(prev_cluster.ids);
}

function on_mouse_over(hovered_cluster) {
    d3.select(this).style("cursor", "pointer");
}

function on_mouse_out(d) {
    d3.select(this).style("cursor", "default");

    setTimeout(function () {
        svg.selectAll("rect").remove();
        svg.selectAll(".models").remove();
    }, 50);
}

function rendering_lines() {
    svg.append("svg:g").selectAll(".cluster_line")
        .data(lines)
        .enter()
        .append("line")
        .attr("class", "cluster_line")
        .attr("x1", function (d) {
            return d.x1;
        })
        .attr("x2", function (d) {
            return d.x2;
        })
        .attr("y1", function (d) {
            return d.y1;
        })
        .attr("y2", function (d) {
            return d.y2;
        })
        .attr("stroke", "black")
        .attr("stroke-width", function (d) {
            return d.distance;
        });
}

function save_to_pdf(id) {
    let div = document.getElementById(id);

    let doc = new jsPDF();

    domtoimage.toPng(div)
        .then(function (dataUrl) {
            let link = document.createElement('a');
            link.download = 'image-name.png';
            link.href = dataUrl;
            doc.addImage(link.href, 10, 10, 180, 180 * 800 / 1400);
            doc.save();
        })
        .catch(function (error) {
            console.error('oops, something went wrong!', error);
        });
}

function save_to_image(id) {
    let div = document.getElementById(id);

    domtoimage.toPng(div)
        .then(function (dataUrl) {
            var link = document.createElement('a');
            link.download = 'image-name.png';
            link.href = dataUrl;
            link.click();
        })
        .catch(function (error) {
            console.error('oops, something went wrong!', error);
        });
}