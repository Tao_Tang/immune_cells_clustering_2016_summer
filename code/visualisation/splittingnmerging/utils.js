/**
 * Created by ruigeng on 16/1/17.
 */
Array.prototype.hasSameValue = function(testArr) {
    for(let i = 0; i < testArr.length; i ++) {
        for(let j = 0; j < this.length; j ++) {
            if(testArr[i] == this[j])
                return true;
        }
    }

    return false;
};

function euclidean(a, b) {
    let distance = 0;

    let count = countProperties(a);

    let index = 0;
    for(let key in a) {
        if(a.hasOwnProperty(key)) {
            if((count - NUMBER_OF_MARKERS <= index)
                    && b[key] != 0) {
                distance += Math.pow(a[key] - b[key], 2);
            }
            index ++;
        }
    }

    return Math.sqrt(distance);
}

function countProperties(obj) {
    let count = 0;

    for(let prop in obj) {
        if(obj.hasOwnProperty(prop))
            ++count;
    }

    return count;
}

function updateDistances(selected) {
    for(let t of times) {
        for (let c of t.clusters) {
            c.distance = euclidean(c, selected);
        }
    }
}